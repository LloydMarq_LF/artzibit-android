package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by LloydM on 7/5/17
 * for Livefitter
 */

public class CartModel implements Parcelable {

    /**
     *
     * "id": 7,
     "user_id": 7,
     "status": null,
     "shipping": 60,
     "installation_fees": 20,
     "subtotal_amount": 324.77,
     "items": [ ]
     */

    private int id;
    @SerializedName("user_id")
    private int userId;
    private boolean status;
    private float shipping;
    @SerializedName("installation_fees")
    private float installationFees;
    @SerializedName("subtotal_amount")
    private float subtotalAmount;
    private ArrayList<CartItemModel> items;

    protected CartModel(Parcel in) {
        id = in.readInt();
        userId = in.readInt();
        status = in.readByte() != 0;
        shipping = in.readInt();
        installationFees = in.readFloat();
        subtotalAmount = in.readFloat();
        items = in.createTypedArrayList(CartItemModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(userId);
        dest.writeByte((byte) (status ? 1 : 0));
        dest.writeFloat(shipping);
        dest.writeFloat(installationFees);
        dest.writeFloat(subtotalAmount);
        dest.writeTypedList(items);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CartModel> CREATOR = new Creator<CartModel>() {
        @Override
        public CartModel createFromParcel(Parcel in) {
            return new CartModel(in);
        }

        @Override
        public CartModel[] newArray(int size) {
            return new CartModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int user_id) {
        this.userId = user_id;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public float getShipping() {
        return shipping;
    }

    public void setShipping(float shipping) {
        this.shipping = shipping;
    }

    public float getInstallationFees() {
        return installationFees;
    }

    public void setInstallationFees(float installationFees) {
        this.installationFees = installationFees;
    }

    public float getSubtotalAmount() {
        return subtotalAmount;
    }

    public void setSubtotalAmount(float subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
    }

    public ArrayList<CartItemModel> getItems() {
        return items;
    }

    public void setItems(ArrayList<CartItemModel> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof CartModel){
            CartModel other = (CartModel) obj;

            return this.getId() == other.getId();
        }
        return false;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
