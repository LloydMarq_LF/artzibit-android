package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;


/**
 * Created by LloydM on 6/28/17
 * for Livefitter
 */

public class FaveArtworkModel implements Parcelable {
    /*
    {
            "id": 9,
            "artwork_id": 3,
            "image": "https://image.jpg"
        }
     */

    private int id;
    @SerializedName("artwork_id")
    private int artworkId;
    private String image;

    public FaveArtworkModel(int id, int artworkId, String image) {
        this.id = id;
        this.artworkId = artworkId;
        this.image = image;
    }

    protected FaveArtworkModel(Parcel in) {
        id = in.readInt();
        artworkId = in.readInt();
        image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(artworkId);
        dest.writeString(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FaveArtworkModel> CREATOR = new Creator<FaveArtworkModel>() {
        @Override
        public FaveArtworkModel createFromParcel(Parcel in) {
            return new FaveArtworkModel(in);
        }

        @Override
        public FaveArtworkModel[] newArray(int size) {
            return new FaveArtworkModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArtworkId() {
        return artworkId;
    }

    public void setArtworkId(int artworkId) {
        this.artworkId = artworkId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
