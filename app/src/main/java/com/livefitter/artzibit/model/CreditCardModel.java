package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.stripe.android.model.Card;

import java.util.ArrayList;


/**
 * Created by LloydM on 7/13/17
 * for Livefitter
 */

public class CreditCardModel implements Parcelable {

    public static final String TYPE_EXISTING_CARD = "card";
    public static final String TYPE_NEW_CARD = "stripe";


    /**
     * id : card_1ARdnOAJ3izz1t2s57h9UrXT
     * isDefault : true
     * brand : Visa
     * last4 : 4242
     * expiryMonth : 10
     * expiryYear : 2017
     */

    private String id;
    @SerializedName("isDefault")
    private boolean isDefault;
    private String brand;
    private String last4;
    @SerializedName("expiryMonth")
    private int expiryMonth;
    @SerializedName("expiryYear")
    private int expiryYear;
    private boolean isSelected;
    private transient boolean isNew;
    private String token;

    public CreditCardModel(@NonNull Card card,@NonNull String token,@NonNull boolean isDefault,@NonNull boolean isNew){
        this.last4 = card.getLast4();
        this.expiryMonth = card.getExpMonth();
        this.expiryYear = card.getExpYear();
        this.token = token;
        this.isDefault = isDefault;
        this.isNew = isNew;
    }

    protected CreditCardModel(Parcel in) {
        id = in.readString();
        isDefault = in.readByte() != 0;
        brand = in.readString();
        last4 = in.readString();
        expiryMonth = in.readInt();
        expiryYear = in.readInt();
        isSelected = in.readByte() != 0;
        isNew = in.readByte() != 0;
        token = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeByte((byte) (isDefault ? 1 : 0));
        dest.writeString(brand);
        dest.writeString(last4);
        dest.writeInt(expiryMonth);
        dest.writeInt(expiryYear);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeByte((byte) (isNew ? 1 : 0));
        dest.writeString(token);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CreditCardModel> CREATOR = new Creator<CreditCardModel>() {
        @Override
        public CreditCardModel createFromParcel(Parcel in) {
            return new CreditCardModel(in);
        }

        @Override
        public CreditCardModel[] newArray(int size) {
            return new CreditCardModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        this.isDefault = aDefault;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public int getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(int expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public int getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(int expiryYear) {
        this.expiryYear = expiryYear;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static boolean hasDefaultSelection(ArrayList<CreditCardModel> creditCardModelArrayList){

        boolean hasDefaultItem = false;

        if(creditCardModelArrayList != null && !creditCardModelArrayList.isEmpty()){
            for (CreditCardModel creditCardModel : creditCardModelArrayList){
                if(creditCardModel.isDefault()){
                    hasDefaultItem = true;
                }
            }
        }

        return hasDefaultItem;
    }

    public static CreditCardModel retrieveDefaultSelection(ArrayList<CreditCardModel> creditCardModelArrayList){

        if(creditCardModelArrayList != null && !creditCardModelArrayList.isEmpty()){
            for (CreditCardModel creditCardModel : creditCardModelArrayList){
                if(creditCardModel.isDefault()){
                    return creditCardModel;
                }
            }
        }

        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CreditCardModel) {
            CreditCardModel other = (CreditCardModel) obj;
            if((this.getId() != null && !this.getId().isEmpty())
                    && (other.getId() != null && !other.getId().isEmpty())) {
                return this.getId().equals(other.getId());
            }else if((this.getToken() != null && !this.getToken().isEmpty())
                    && (other.getToken() != null && !other.getToken().isEmpty())){
                return this.getToken().equals(other.getToken());
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
