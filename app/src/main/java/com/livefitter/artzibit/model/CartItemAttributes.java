package com.livefitter.artzibit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LloydM on 7/7/17
 * for Livefitter
 */

public class CartItemAttributes {


    /**
     * id : 5
     * installation : true
     * quantity : 1
     * _destroy : true
     */

    private int id;
    private boolean installation;
    private int quantity;
    @SerializedName("_destroy")
    private boolean toBeRemoved;

    public CartItemAttributes(int id, boolean installation, int quantity, boolean toBeRemoved) {
        this.id = id;
        this.installation = installation;
        this.quantity = quantity;
        this.toBeRemoved = toBeRemoved;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isInstallation() {
        return installation;
    }

    public void setInstallation(boolean installation) {
        this.installation = installation;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isToBeRemoved() {
        return toBeRemoved;
    }

    public void setToBeRemoved(boolean toBeRemoved) {
        this.toBeRemoved = toBeRemoved;
    }
}
