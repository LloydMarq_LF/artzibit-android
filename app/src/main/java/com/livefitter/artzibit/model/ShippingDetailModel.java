package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by LloydM on 7/13/17
 * for Livefitter
 */

public class ShippingDetailModel implements Parcelable {

    /**
     * id : 1
     * name : XXX
     * phoneNumber : 123
     * address : XXX
     * isDefault : false
     *
     * first_name: "Lloyd",
     * last_name: "Marquez",
     * phone_number: "111",
     * detailed_addr: "111",
     * city: "Valenzuela City",
     * country: "Philippines",
     * is_default: true
     */

    private static final String[] COUNTRIES_WITH_NO_SHIPPING_CHARGE = {"Singapore"};

    private int id;
    private String name;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("lastt_name")
    private String lastName;
    @SerializedName("phone_number")
    private String phoneNumber;
    private String address;
    @SerializedName("detailed_addr")
    private String detailedAddress;
    private String city;
    private String country;
    @SerializedName("is_default")
    private boolean isDefault;
    @SerializedName("is_selected")
    private boolean isSelected;
    private transient boolean isNew;

    public ShippingDetailModel(String firstName, String lastName, String phoneNumber, String detailedAddress, String city, String country, boolean isDefault, boolean isNew) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.detailedAddress = detailedAddress;
        this.city = city;
        this.country = country;
        this.isDefault = isDefault;
        this.isNew = isNew;
    }

    protected ShippingDetailModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        phoneNumber = in.readString();
        address = in.readString();
        detailedAddress = in.readString();
        city = in.readString();
        country = in.readString();
        isDefault = in.readByte() != 0;
        isSelected = in.readByte() != 0;
        isNew = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(phoneNumber);
        dest.writeString(address);
        dest.writeString(detailedAddress);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeByte((byte) (isDefault ? 1 : 0));
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeByte((byte) (isNew ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShippingDetailModel> CREATOR = new Creator<ShippingDetailModel>() {
        @Override
        public ShippingDetailModel createFromParcel(Parcel in) {
            return new ShippingDetailModel(in);
        }

        @Override
        public ShippingDetailModel[] newArray(int size) {
            return new ShippingDetailModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDetailedAddress() {
        return detailedAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        this.isDefault = aDefault;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public static boolean hasDefaultSelection(ArrayList<ShippingDetailModel> shippingDetailModelArrayList){

        boolean hasDefaultItem = false;

        if(shippingDetailModelArrayList != null && !shippingDetailModelArrayList.isEmpty()){
            for (ShippingDetailModel shippingDetailModel : shippingDetailModelArrayList){
                if(shippingDetailModel.isDefault()){
                    hasDefaultItem = true;
                }
            }
        }

        return hasDefaultItem;
    }

    public static ShippingDetailModel retrieveDefaultSelection(ArrayList<ShippingDetailModel> shippingDetailModelArrayList){

        if(shippingDetailModelArrayList != null && !shippingDetailModelArrayList.isEmpty()){
            for (ShippingDetailModel shippingDetailModel : shippingDetailModelArrayList){
                if(shippingDetailModel.isDefault()){
                    return shippingDetailModel;
                }
            }
        }

        return null;
    }

    public static ShippingDetailModel retrieveSelection(ArrayList<ShippingDetailModel> shippingDetailModelArrayList){

        if(shippingDetailModelArrayList != null && !shippingDetailModelArrayList.isEmpty()){
            for (ShippingDetailModel shippingDetailModel : shippingDetailModelArrayList){
                if(shippingDetailModel.isSelected()){
                    return shippingDetailModel;
                }
            }
        }

        return null;
    }

    public static boolean shouldShowShippingChargeWarning(ShippingDetailModel shippingDetailModel){

        if(shippingDetailModel.getCountry() != null && !shippingDetailModel.getCountry().isEmpty()){
            return !Arrays.asList(COUNTRIES_WITH_NO_SHIPPING_CHARGE).contains(shippingDetailModel.getCountry());
        }

        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ShippingDetailModel){
            ShippingDetailModel other = (ShippingDetailModel) obj;
            if(this.isNew() && other.isNew()){
                String thisAddress = this.getDetailedAddress() + this.getCity() + this.getCountry();
                String otherAddress = other.getDetailedAddress() + other.getCity() + other.getCountry();

                return thisAddress.equals(otherAddress);
            }else if(this.getId() != 0 && other.getId() != 0) {
                return this.getId() == other.getId();
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
