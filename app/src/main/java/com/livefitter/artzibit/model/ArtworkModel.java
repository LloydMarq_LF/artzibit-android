package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * Created by LloydM on 6/6/17
 * for Livefitter
 */

public class ArtworkModel implements Parcelable {

    private static final String TAG = ArtworkModel.class.getSimpleName();

    /*
    * {
        "id": 10,
        "image": "https://image.jpg",
        "featured_price": "89.0",
        "name": "Artwork 9 - Custom Abstract Scalable",
        "artist_name": "C Artist",
        "orientation": "landscape",
        "description": "description.",
        "available_sizes": [
            {
                "label": "Extra Large: 150.0cm X 223.0cm",
                "fixed_size_id": 8,
                "height": "150.0",
                "width": "223.0",
                "fixed_size_price_id": 8,
                "price": "1108.0"
            },
            {
                "label": "Large: 91.0cm X 137.0cm",
                "fixed_size_id": 7,
                "height": "91.0",
                "width": "137.0",
                "fixed_size_price_id": 7,
                "price": "456.0"
            },
            {
                "label": "Medium: 51.0cm X 76.0cm",
                "fixed_size_id": 6,
                "height": "51.0",
                "width": "76.0",
                "fixed_size_price_id": 6,
                "price": "203.0"
            },
            {
                "label": "Small: 30.0cm X 46.0cm",
                "fixed_size_id": 5,
                "height": "30.0",
                "width": "46.0",
                "fixed_size_price_id": 5,
                "price": "89.0"
            }
        ],
        "favorite_id": 13,
        "favorited": true,
        "cart_id": null
    }
    *
    */


    private int id;
    private String name;
    @SerializedName("artist_name")
    private String artistName;
    private String image;
    private String description;
    @SerializedName("size_type")
    private String sizeType;
    @SerializedName("size_id")
    private String sizeId;
    private String orientation;
    @SerializedName("price_id")
    private int priceId;
    private float price;
    @SerializedName("available_sizes")
    private ArrayList<ArtworkSizeModel> availableSizes;
    @SerializedName("favorite_id")
    private int favoriteId;
    @SerializedName("favorited")
    private boolean isFaved;
    @SerializedName("cart_id")
    private int cartId;
    @SerializedName("cart_item_id")
    private int cartItemId;

    // Non-gson field
    private transient ArtworkSizeModel minimumSize, maximumSize;
    private transient int viewCounter;
    private ArrayList<ArtworkViewDuration> views = new ArrayList<>();


    protected ArtworkModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        artistName = in.readString();
        image = in.readString();
        description = in.readString();
        sizeType = in.readString();
        sizeId = in.readString();
        orientation = in.readString();
        priceId = in.readInt();
        price = in.readFloat();
        availableSizes = in.createTypedArrayList(ArtworkSizeModel.CREATOR);
        favoriteId = in.readInt();
        isFaved = in.readByte() != 0;
        cartId = in.readInt();
        cartItemId = in.readInt();

        minimumSize = in.readParcelable(ArtworkSizeModel.class.getClassLoader());
        maximumSize = in.readParcelable(ArtworkSizeModel.class.getClassLoader());
        viewCounter = in.readInt();
        views = in.createTypedArrayList(ArtworkViewDuration.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(artistName);
        dest.writeString(image);
        dest.writeString(description);
        dest.writeString(sizeType);
        dest.writeString(sizeId);
        dest.writeString(orientation);
        dest.writeInt(priceId);
        dest.writeFloat(price);
        dest.writeTypedList(availableSizes);
        dest.writeInt(favoriteId);
        dest.writeByte((byte) (isFaved ? 1 : 0));
        dest.writeInt(cartId);
        dest.writeInt(cartItemId);

        dest.writeParcelable(minimumSize, flags);
        dest.writeParcelable(maximumSize, flags);
        dest.writeInt(viewCounter);
        dest.writeTypedList(views);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ArtworkModel> CREATOR = new Creator<ArtworkModel>() {
        @Override
        public ArtworkModel createFromParcel(Parcel in) {
            return new ArtworkModel(in);
        }

        @Override
        public ArtworkModel[] newArray(int size) {
            return new ArtworkModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSizeType() {
        return sizeType;
    }

    public void setSizeType(String sizeType) {
        this.sizeType = sizeType;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public ArrayList<ArtworkSizeModel> getAvailableSizes() {
        return availableSizes;
    }

    public void setAvailableSizes(ArrayList<ArtworkSizeModel> availableSizes) {
        this.availableSizes = availableSizes;
    }

    public int getFavoriteId() {
        return favoriteId;
    }

    public void setFavoriteId(int favoriteId) {
        this.favoriteId = favoriteId;
    }

    public boolean isFaved() {
        return isFaved;
    }

    public void setFaved(boolean faved) {
        isFaved = faved;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(int cartItemId) {
        this.cartItemId = cartItemId;
    }

    public int getViewCounter() {
        return viewCounter;
    }

    public void setViewCounter(int viewCounter) {
        this.viewCounter = viewCounter;
    }

    public ArrayList<ArtworkViewDuration> getViews() {
        return views;
    }

    public void setViews(ArrayList<ArtworkViewDuration> views) {
        this.views = views;
    }

    public ArtworkSizeModel getMinimumSize() {
        if(minimumSize == null){
            computeMinMaxSizes();
        }
        return minimumSize;
    }

    public ArtworkSizeModel getMaximumSize() {
        if(maximumSize == null){
            computeMinMaxSizes();
        }
        return maximumSize;
    }


    public void computeMinMaxSizes(){
        if(getAvailableSizes() != null && !getAvailableSizes().isEmpty()){
            ArtworkSizeModel tempMinSize = getAvailableSizes().get(0);
            ArtworkSizeModel tempMaxSize = getAvailableSizes().get(0);

            for (ArtworkSizeModel artworkSizeModel : getAvailableSizes()){
                boolean isBiggerThanMax = tempMaxSize.getWidth() < artworkSizeModel.getWidth()
                        || tempMaxSize.getHeight() < artworkSizeModel.getHeight();

                if(isBiggerThanMax){
                    tempMaxSize = artworkSizeModel;
                }

                boolean isSmallerThanMin = tempMinSize.getWidth() > artworkSizeModel.getWidth()
                        || tempMinSize.getHeight() > artworkSizeModel.getHeight();

                if(isSmallerThanMin){
                    tempMinSize = artworkSizeModel;
                }
            }

            minimumSize = tempMinSize;
            maximumSize = tempMaxSize;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ArtworkModel) {
            ArtworkModel other = (ArtworkModel) obj;
            return this.getId() == other.getId();
        }
        return false;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
