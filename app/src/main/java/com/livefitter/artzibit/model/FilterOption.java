package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;


/**
 * Created by LloydM on 6/13/17
 * for Livefitter
 */

public class FilterOption implements Parcelable {
    private int id;
    private String name;
    private boolean isSelected;

    protected FilterOption(Parcel in) {
        id = in.readInt();
        name = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FilterOption> CREATOR = new Creator<FilterOption>() {
        @Override
        public FilterOption createFromParcel(Parcel in) {
            return new FilterOption(in);
        }

        @Override
        public FilterOption[] newArray(int size) {
            return new FilterOption[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof FilterOption) {
            FilterOption other = (FilterOption) obj;
            return this.id == other.id;
        }
        return false;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
