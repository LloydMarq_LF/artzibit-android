package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;


/**
 * Created by LloydM on 6/6/17
 * for Livefitter
 */

public class ArtworkSizeModel implements Parcelable {
    /*
    * {
                "label": "Large: 91.0cm X 137.0cm",
                "fixed_size_id": 7,
                "height": "91.0",
                "width": "137.0",
                "fixed_size_price_id": 7,
                "price": "456.0"
      }
    */

    public static final String CUSTOM_SIZE = "Custom";

    private String label;
    @SerializedName("fixed_size_id")
    private int sizeId;
    private float height;
    private float width;
    @SerializedName("fixed_size_price_id")
    private int priceId;
    private String price;

    // Non-gson fields
    private transient float aspectRatio;

    public ArtworkSizeModel(String label, int sizeId, float height, float width, int priceId, String price) {
        this.label = label;
        this.sizeId = sizeId;
        this.height = height;
        this.width = width;
        this.priceId = priceId;
        this.price = price;

        aspectRatio = computeAspectRatio();
    }

    protected ArtworkSizeModel(Parcel in) {
        label = in.readString();
        sizeId = in.readInt();
        height = in.readFloat();
        width = in.readFloat();
        priceId = in.readInt();
        price = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeInt(sizeId);
        dest.writeFloat(height);
        dest.writeFloat(width);
        dest.writeInt(priceId);
        dest.writeString(price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ArtworkSizeModel> CREATOR = new Creator<ArtworkSizeModel>() {
        @Override
        public ArtworkSizeModel createFromParcel(Parcel in) {
            return new ArtworkSizeModel(in);
        }

        @Override
        public ArtworkSizeModel[] newArray(int size) {
            return new ArtworkSizeModel[size];
        }
    };

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public float getAspectRatio() {
        if(aspectRatio == 0){
            aspectRatio = computeAspectRatio();
        }
        return aspectRatio;
    }

    private float computeAspectRatio(){
        float ratio = 0;
        if(width > 0 && height > 0){
            ratio = height / width;
        }

        return ratio;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ArtworkSizeModel) {
            ArtworkSizeModel other = (ArtworkSizeModel) obj;

            return this.getPriceId() == other.getPriceId()
                    || this.getLabel().equals(other.getLabel());
        }
        return false;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
