package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


/**
 * Created by LloydM on 5/30/17
 * for Livefitter
 */

public class UserModel implements Parcelable {

    private static final String TAG = UserModel.class.getSimpleName();

    /*authentication_token: "xxxxx",
    country: "country",
    created_at: "2017-05-17T03:41:35Z",
    email: "example@email.com",
    id: 1,
    name: null,
    updated_at: "2017-05-17T03:41:35Z",
    username: "username"*/

    @SerializedName("authentication_token")
    private String authToken;
    @SerializedName("avatar_file_name")
    private String avatarFileName;
    private String country;
    @SerializedName("created_at")
    private String createdAt;
    private String email;
    private int id;
    private String name;
    @SerializedName("updated_at")
    private String updatedAt;
    private String username;

    public UserModel(int id, String authToken, String email) {
        this.id = id;
        this.authToken = authToken;
        this.email = email;
    }

    public UserModel(Parcel in) {
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in){
        this.authToken = in.readString();
        this.avatarFileName = in.readString();
        this.country = in.readString();
        this.createdAt = in.readString();
        this.email = in.readString();
        this.id = in.readInt();
        this.name = in.readString();
        this.updatedAt = in.readString();
        this.username = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(authToken);
        dest.writeString(avatarFileName);
        dest.writeString(country);
        dest.writeString(createdAt);
        dest.writeString(email);
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(updatedAt);
        dest.writeString(username);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getAuthToken() {
        return authToken;
    }

    public String getAvatarFileName() {
        return avatarFileName;
    }

    public String getCountry() {
        return country;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getEmail() {
        return email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getUsername() {
        return username;
    }

    public static String encode(UserModel userModel){
        if((userModel.email != null && !userModel.email.isEmpty())
                && (userModel.authToken != null && !userModel.authToken.isEmpty())){
            String toEncode = userModel.getId() + ":" + userModel.email + ":" + userModel.authToken;
            return Base64.encodeToString(toEncode.getBytes(), Base64.DEFAULT);
        }

        return "";
    }

    public static UserModel decode(String code){
        String decodedString = new String(Base64.decode(code, Base64.DEFAULT));

        if(!decodedString.isEmpty() && decodedString.contains(":")){
            String[] splitString = decodedString.split(":");
            int id = Integer.decode(splitString[0]);
            String email = splitString[1];
            String authToken = splitString[2];

            return new UserModel(id, authToken, email);
        }

        return null;
    }


    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields){
            try {
                if(f.getModifiers() == Modifier.PRIVATE && !f.getName().equals("CREATOR")){
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }
}
