package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;


/**
 * Created by LloydM on 7/5/17
 * for Livefitter
 */

public class CartItemModel implements Parcelable {


    /**
     {
         "id": 15,
         "image": "https://artzibit-dev.s3-ap-southeast-1.amazonaws.com/artwork_images/images/000/000/011/original/shiny-night-city_1127-8.jpg?1497591529",
         "name": "Artwork 9 - Custom Abstract Scalable",
         "base_price": 132.39,
         "price": 264.77,
         "size": "36.0 X 55.0",
         "quantity": 2,
         "installation": true,
         "installation_amount": 20,
         "base_installation_price": 10
     }
     */

    private int id;
    private String image;
    private String name;
    /**
     * Price of a single unit of this artwork
     */
    @SerializedName("base_price")
    private float basePrice;
    /**
     * Total price of the artwork including the installation fee, if applicable.
     * Computed in the following way:
     *
     * ({@link #basePrice} * {@link #quantity}) + {@link #installationAmount}
     */
    private float price;
    private String size;
    /**
     * Quantity of this artwork, should always be greater than 0
     */
    private int quantity;
    private boolean installation;
    /**
     * If this is being installed, this shows the price of the installation only.
     * Computed as ({@link #baseInstallationPrice} * {@link #quantity})
     */
    @SerializedName("installation_amount")
    private float installationAmount;
    /**
     * Price of a single unit of this artwork if being installed.
     * Otherwise, it is 0
     */
    @SerializedName("base_installation_price")
    private float baseInstallationPrice;
    @SerializedName("delivery_amount")
    private float deliveryAmount;
    /**
     * Internal variable to determine if this cartItem is removed from the list of cartItems
     * as we need to take note of removed items as well when updating the backend for the cart
     */
    private transient boolean isRemoved;

    protected CartItemModel(Parcel in) {
        id = in.readInt();
        image = in.readString();
        name = in.readString();
        basePrice = in.readFloat();
        price = in.readFloat();
        size = in.readString();
        quantity = in.readInt();
        installation = in.readByte() != 0;
        installationAmount = in.readFloat();
        baseInstallationPrice = in.readFloat();
        deliveryAmount = in.readFloat();
        isRemoved = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(image);
        dest.writeString(name);
        dest.writeFloat(basePrice);
        dest.writeFloat(price);
        dest.writeString(size);
        dest.writeInt(quantity);
        dest.writeByte((byte) (installation ? 1 : 0));
        dest.writeFloat(installationAmount);
        dest.writeFloat(baseInstallationPrice);
        dest.writeFloat(deliveryAmount);
        dest.writeByte((byte) (isRemoved ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CartItemModel> CREATOR = new Creator<CartItemModel>() {
        @Override
        public CartItemModel createFromParcel(Parcel in) {
            return new CartItemModel(in);
        }

        @Override
        public CartItemModel[] newArray(int size) {
            return new CartItemModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(float basePrice) {
        this.basePrice = basePrice;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isInstallation() {
        return installation;
    }

    public void setInstallation(boolean installation) {
        this.installation = installation;
    }

    public float getInstallationAmount() {
        return installationAmount;
    }

    public void setInstallationAmount(float installationAmount) {
        this.installationAmount = installationAmount;
    }

    public float getBaseInstallationPrice() {
        return baseInstallationPrice;
    }

    public void setBaseInstallationPrice(float baseInstallationPrice) {
        this.baseInstallationPrice = baseInstallationPrice;
    }

    public float getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(float deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof CartItemModel){
            CartItemModel other = (CartItemModel) obj;

            return this.getId() == other.getId();
        }
        return false;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
