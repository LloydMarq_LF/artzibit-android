package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by LloydM on 6/9/17
 * for Livefitter
 */

public class ArtworkFilterModel implements Parcelable {

    /**
     * {
         "name": "styles",
         "key": "artwork_style_id"
         "options": [
             {
             "id": 1,
             "name": "Impressionist"
             },
             {
             "id": 2,
             "name": "Surrealist"
             },
             {
             "id": 3,
             "name": "Abstract"
             }
         ]
     }
     */

    public static final int NO_SELECTED_OPTION = 0;

    private String name;
    private String key;
    @SerializedName("values")
    private ArrayList<FilterOption> options;
    private int selectedOptionId = NO_SELECTED_OPTION;

    public ArtworkFilterModel(Parcel in) {
        name = in.readString();
        key = in.readString();
        options = in.createTypedArrayList(FilterOption.CREATOR);
        selectedOptionId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(key);
        dest.writeTypedList(options);
        dest.writeInt(selectedOptionId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ArtworkFilterModel> CREATOR = new Creator<ArtworkFilterModel>() {
        @Override
        public ArtworkFilterModel createFromParcel(Parcel in) {
            return new ArtworkFilterModel(in);
        }

        @Override
        public ArtworkFilterModel[] newArray(int size) {
            return new ArtworkFilterModel[size];
        }
    };

    public String getName() {
        // Yeah, we need to capitalize the name.
        return name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
    }

    public String getKey() {
        return key;
    }

    public ArrayList<FilterOption> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<FilterOption> options) {
        this.options = options;
    }

    public int getSelectedOptionId() {
        return selectedOptionId;
    }

    public void setSelectedOptionId(int selectedOptionId) {
        this.selectedOptionId = selectedOptionId;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ArtworkFilterModel) {
            ArtworkFilterModel other = (ArtworkFilterModel) obj;
            return this.getName().equals(other.getName()) || this.getKey().equals(other.getKey());
        }

        return false;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
