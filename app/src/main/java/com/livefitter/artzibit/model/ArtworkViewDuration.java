package com.livefitter.artzibit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;


/**
 * Created by LloydM on 7/20/17
 * for Livefitter
 */

public class ArtworkViewDuration implements Parcelable {

    private String startTime;
    private String endTime;

    public ArtworkViewDuration(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    protected ArtworkViewDuration(Parcel in) {
        startTime = in.readString();
        endTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(startTime);
        dest.writeString(endTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ArtworkViewDuration> CREATOR = new Creator<ArtworkViewDuration>() {
        @Override
        public ArtworkViewDuration createFromParcel(Parcel in) {
            return new ArtworkViewDuration(in);
        }

        @Override
        public ArtworkViewDuration[] newArray(int size) {
            return new ArtworkViewDuration[size];
        }
    };

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
