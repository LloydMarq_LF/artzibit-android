package com.livefitter.artzibit.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by LloydM on 7/17/17
 * for Livefitter
 */

public abstract class CheckOutUpdatesReceiver extends BroadcastReceiver {

    private static final String TAG = CheckOutUpdatesReceiver.class.getSimpleName();

    private OnReceiveBroadcastListener mListener;

    public interface OnReceiveBroadcastListener{
        void onReceiveBroadcast(Context context, Intent intent);
    }

    public CheckOutUpdatesReceiver(OnReceiveBroadcastListener mListener) {
        this.mListener = mListener;
    }
}
