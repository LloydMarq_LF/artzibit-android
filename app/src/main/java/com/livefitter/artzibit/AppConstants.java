package com.livefitter.artzibit;

/**
 * Created by LFT-PC-010 on 4/26/2017.
 */

public class AppConstants {

    public static final String PROVIDER_FACEBOOK            ="facebook";

    // API URL
    public static final String URL_BASE                     = "https://artzibit-dev.herokuapp.com/";
    public static final String URL_VERSION                  = "api/v1/";

    public static final String URL_SESSIONS                 = URL_VERSION + "users/sessions";
    public static final String URL_REGISTRATIONS            = URL_VERSION + "users/registrations";
    public static final String URL_FORGOT_PASSWORDS         = URL_VERSION + "users/passwords";
    public static final String URL_ARTWORKS                 = URL_VERSION + "artworks";
    public static final String URL_ARTWORKS_FILTERS         = URL_VERSION + "filters";
    public static final String URL_ARTWORKS_FAVORITES       = URL_VERSION + "user_favorites";
    public static final String URL_CART                     = URL_VERSION + "carts";
    public static final String URL_SHIPPING_DETAILS         = URL_VERSION + "user_shipping_details";
    public static final String URL_BANK_DETAILS             = URL_VERSION + "user_bank_details";
    public static final String URL_USER_VIEWS               = URL_VERSION + "user_views";
    public static final String URL_APP_COUNTERS             = URL_VERSION + "user_launch_app_counters";
    public static final String URL_APP_TRACKING_LOGS        = URL_VERSION + "user_app_tracking_logs";

    // Fragment tags
    public static String FRAG_CURRENT;
    public static final String FRAG_HOME                    = "frag_home";

    // Shared Preferences
    // preference filenames
    public static final String PREFS                        = "shared_preferences";
    public static final String PREFS_FILTERS                = "filters";
    // Keys for each preference
    public static final String PREFS_CHOSEN_CATEGORY        = "chosen_category";
    public static final String PREFS_AUTH                   = "auth";
    public static final String PREFS_CART_ID                = "cart_id";

    // Bundle Keys
    public static final String BUNDLE_FACEBOOK_UID          = "account_facebook_uid";
    public static final String BUNDLE_FACEBOOK_NAME         = "account_facebook_name";
    public static final String BUNDLE_FACEBOOK_EMAIL        = "account_facebook_email";
    public static final String BUNDLE_FACEBOOK_PICTURE      = "account_facebook_picture";
    public static final String BUNDLE_USER                  = "user";
    public static final String BUNDLE_CATEGORY              = "category";
    public static final String BUNDLE_FILTER                = "filter_options";
    public static final String BUNDLE_ARTWORK               = "artwork";
    public static final String BUNDLE_ARTWORK_ID            = "artwork_id";
    public static final String BUNDLE_CART                  = "cart";
    public static final String BUNDLE_SHIPPING_DETAIL       = "shipping_detail";
    public static final String BUNDLE_CREDIT_CARD           = "credit_card";
    public static final String BUNDLE_SHIPPING_DETAIL_LIST  = "shipping_detail_list";
    public static final String BUNDLE_CREDIT_CARD_LIST      = "credit_card_list";
    public static final String BUNDLE_CHECKOUT_NAVIGATION   = "checkout_navigation";
    public static final String BUNDLE_RECEIPT_NUMBER        = "receipt_number";

    // Activity Request code
    public static final int REQUEST_CODE_FILTER_OPTIONS     = 280;
    public static final int REQUEST_CHECKOUT_CART           = 281;
    public static final int REQUEST_CREATE_SHIPPING_DETAIL  = 282;
    public static final int REQUEST_CREATE_PAYMENT_METHOD   = 283;

    // Intent filters
    public static final String INTENT_ACTION_CHECKOUT_PROCESS = "com.livefitter.artzibit.CHECKOUT_PROCESS";

    // Categories
    public static final int CATEGORY_FIXED                  = 1;
    public static final int CATEGORY_CUSTOM                 = 2;
}
