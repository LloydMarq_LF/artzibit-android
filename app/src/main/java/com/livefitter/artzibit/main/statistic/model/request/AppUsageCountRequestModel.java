package com.livefitter.artzibit.main.statistic.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LloydM on 7/19/17
 * for Livefitter
 */

public class AppUsageCountRequestModel {

    /**
     * user_launch : {"user_id":1}
     */
    @SerializedName("user_launch")
    private UserLaunch userLaunch;

    public AppUsageCountRequestModel(int userId) {
        this.userLaunch = new UserLaunch(userId);
    }

    public static class UserLaunch {
        /**
         * user_id : 1
         */

        @SerializedName("user_id")
        private int userId;

        public UserLaunch(int userId) {
            this.userId = userId;
        }
    }
}
