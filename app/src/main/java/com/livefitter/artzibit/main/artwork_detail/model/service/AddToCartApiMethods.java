package com.livefitter.artzibit.main.artwork_detail.model.service;

import android.support.annotation.NonNull;

import com.google.gson.internal.LinkedTreeMap;
import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.artwork_detail.model.request.AddToCartRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AddToCartApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_CART)
    Call<BaseResponseModel<LinkedTreeMap>> addArtworkToCart(@NonNull @Header("X-User-Email") String userEmail,
                                                            @NonNull @Header("X-User-Token") String authToken,
                                                            @Body AddToCartRequestModel body);


    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @PUT(AppConstants.URL_CART + "/{cartId}")
    Call<BaseResponseModel<LinkedTreeMap>> addArtworkToCart(@NonNull @Header("X-User-Email") String userEmail,
                                                            @NonNull @Header("X-User-Token") String authToken,
                                                            @Path("cartId") int cartId,
                                                            @Body AddToCartRequestModel body);
}