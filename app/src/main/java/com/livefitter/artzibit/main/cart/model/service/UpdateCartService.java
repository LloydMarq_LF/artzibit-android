package com.livefitter.artzibit.main.cart.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.main.cart.model.request.CartRequestModel;
import com.livefitter.artzibit.model.CartModel;

/**
 * Created by LloydM on 7/5/17
 * for Livefitter
 */

public interface UpdateCartService {

    void updateCart(@NonNull String userEmail,
                    @NonNull String authToken,
                    @NonNull int cartId,
                    @NonNull CartRequestModel body,
                    Callback callback);

    void cancel();

    public interface Callback {
        void onUpdateCartSuccess();

        void onUpdateCartItemError();
    }
}
