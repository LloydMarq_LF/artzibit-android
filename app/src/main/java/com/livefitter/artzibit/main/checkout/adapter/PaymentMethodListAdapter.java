package com.livefitter.artzibit.main.checkout.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.main.checkout.adapter.viewholder.CheckOutItemFooterViewHolder;
import com.livefitter.artzibit.main.checkout.adapter.viewholder.PaymentMethodItemViewHolder;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.model.ShippingDetailModel;

import java.util.ArrayList;


/**
 * Created by LloydM on 7/13/17
 * for Livefitter
 */

public class PaymentMethodListAdapter extends RecyclerView.Adapter {

    private static final int ITEM_TYPE_NORMAL = 12;
    private static final int ITEM_TYPE_FOOTER = 13;

    private Context mContext;
    private LayoutInflater mInflater;
    private PaymentMethodListCallback mCallback;
    private ArrayList<CreditCardModel> mCreditCards;
    private String mFooterLabel;

    public interface PaymentMethodListCallback {
        void onPaymentMethodSelected(CreditCardModel creditCard);
        void onFooterClick();
    }

    public PaymentMethodListAdapter(Context context, String footerLabel, PaymentMethodListCallback paymentMethodListCallback) {
        this.mContext = context;
        this.mCallback = paymentMethodListCallback;
        this.mFooterLabel = footerLabel;

        this.mInflater = LayoutInflater.from(context);
    }

    public void setCreditCards(ArrayList<CreditCardModel> creditCards) {
        this.mCreditCards = creditCards;

        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM_TYPE_NORMAL){
            return PaymentMethodItemViewHolder.create(mInflater, parent);
        }
        return CheckOutItemFooterViewHolder.create(mInflater, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);

        switch (viewType){
            case ITEM_TYPE_NORMAL:
                final CreditCardModel creditCard = mCreditCards.get(position);

                if(creditCard != null){
                    ((PaymentMethodItemViewHolder)holder).bind(creditCard, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCallback.onPaymentMethodSelected(creditCard);
                            setSelectedItem(creditCard);
                        }
                    });
                }

                break;

            case ITEM_TYPE_FOOTER:
                ((CheckOutItemFooterViewHolder)holder).bind(mFooterLabel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallback.onFooterClick();
                    }
                });

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(mCreditCards != null
                && (position >= 0 && position < mCreditCards.size())){

            return ITEM_TYPE_NORMAL;
        }

        return ITEM_TYPE_FOOTER;
    }

    @Override
    public int getItemCount() {
        if(mCreditCards != null && !mCreditCards.isEmpty()){

            return mCreditCards.size() + 1;
        }

        // For the footer
        return 1;
    }

    private void setSelectedItem(CreditCardModel selectedItem) {
        if(selectedItem != null && mCreditCards != null && !mCreditCards.isEmpty()){
            for(CreditCardModel creditCard : mCreditCards){
                creditCard.setSelected(creditCard.equals(selectedItem));
            }

            notifyDataSetChanged();
        }
    }
}
