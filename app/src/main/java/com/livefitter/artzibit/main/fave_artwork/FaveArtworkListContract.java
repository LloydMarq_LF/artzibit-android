package com.livefitter.artzibit.main.fave_artwork;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.listener.FaveArtworkClickListener;
import com.livefitter.artzibit.model.FaveArtworkModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public interface FaveArtworkListContract {

    public interface Presenter extends FaveArtworkClickListener {
        void retrieveFaveArtworks();
    }

    public interface View extends BaseContractView{
        void updateFaveArtworkList(ArrayList<FaveArtworkModel> faveArtworkList);
        void proceedToArtworkDetails(FaveArtworkModel faveArtworkModel);
        void showProgressBlocker();
        void hideProgressBlocker();
        void showProgressIndicator();
        void hideProgressIndicator();
        void showEmptyFavesError();
        void hideEmptyFavesError();
        void showErrorDialog();
    }
}
