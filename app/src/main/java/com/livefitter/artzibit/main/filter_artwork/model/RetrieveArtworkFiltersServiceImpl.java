package com.livefitter.artzibit.main.filter_artwork.model;

import android.util.Log;

import com.livefitter.artzibit.model.ArtworkFilterModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 6/7/17
 * for Livefitter
 */

public class RetrieveArtworkFiltersServiceImpl implements RetrieveArtworkFiltersService, Callback<BaseResponseModel<ArrayList<ArtworkFilterModel>>> {

    private static final String TAG = RetrieveArtworkFiltersServiceImpl.class.getSimpleName();

    Call<BaseResponseModel<ArrayList<ArtworkFilterModel>>> mCall;
    private Callback mCallback;

    @Override
    public void retrieveArtworkFilters(Callback callback) {
        mCallback = callback;

        /*Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        FaveArtworkApiMethods apiMethods = RetrofitUtil.getGSONRetrofit(gson).create(FaveArtworkApiMethods.class);*/
        RetrieveArtworkApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(RetrieveArtworkApiMethods.class);

        mCall = apiMethods.retrieveArtworkFilters();
        mCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<BaseResponseModel<ArrayList<ArtworkFilterModel>>> call, Response<BaseResponseModel<ArrayList<ArtworkFilterModel>>> response) {
        Log.d(TAG, "onResponse: " + response);
        if(response.isSuccessful()){

            Log.d(TAG, "response success body: " + response.body().toString());

            if(response.body().getStatus()) {

                ArrayList<ArtworkFilterModel> filterModelArrayList = response.body().getData();

                if(filterModelArrayList != null && !filterModelArrayList.isEmpty()){
                    mCallback.onSuccess(filterModelArrayList);
                }else{
                    mCallback.onError();
                }
            }else{
                mCallback.onError();
            }
        }else{
            Log.d(TAG, "response failed body: "+ response.errorBody());
            mCallback.onError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<ArrayList<ArtworkFilterModel>>> call, Throwable t) {
        Log.d(TAG, "onFailure: ");
        t.printStackTrace();
        mCallback.onError();
    }
}
