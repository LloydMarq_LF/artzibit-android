package com.livefitter.artzibit.main.checkout.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.livefitter.artzibit.main.checkout.adapter.viewholder.CartReviewViewHolder;
import com.livefitter.artzibit.model.CartItemModel;

import java.util.ArrayList;


/**
 * Created by LloydM on 7/4/17
 * for Livefitter
 */

public class CartReviewAdapter extends RecyclerView.Adapter<CartReviewViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<CartItemModel> mCartItems;
    private String mCurrencySymbol;

    public CartReviewAdapter(Context context, String currencySymbol) {
        this.mContext = context;
        this.mCurrencySymbol = currencySymbol;

        mInflater = LayoutInflater.from(context);
    }

    public void setCartItems(ArrayList<CartItemModel> cartItems) {
        this.mCartItems = cartItems;
        notifyDataSetChanged();
    }

    @Override
    public CartReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return CartReviewViewHolder.create(mInflater, parent);
    }

    @Override
    public void onBindViewHolder(CartReviewViewHolder holder, int position) {
        CartItemModel cartItem = mCartItems.get(position);
        if (cartItem != null) {
            holder.bind(mCurrencySymbol, cartItem);
        }
    }

    @Override
    public int getItemCount() {
        return mCartItems != null ? mCartItems.size() : 0;
    }
}
