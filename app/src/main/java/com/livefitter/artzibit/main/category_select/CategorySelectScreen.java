package com.livefitter.artzibit.main.category_select;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.browse_artwork.view.HomeActivity;
import com.livefitter.artzibit.model.UserModel;

import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by LloydM on 6/5/17
 * for Livefitter
 */

public class CategorySelectScreen extends BaseActivity {

    private UserModel mUserModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_selection);

        ButterKnife.bind(this);

        if(getIntent().getExtras() != null){
            Bundle extras = getIntent().getExtras();
            mUserModel = extras.getParcelable(AppConstants.BUNDLE_USER);
        }
    }

    @OnClick({R.id.category_selection_fixed, R.id.category_selection_custom})
    public void categorySelected(View view){
        if(view.getId() == R.id.category_selection_fixed){
            proceedToHomeScreen(AppConstants.CATEGORY_FIXED);
        }else{
            proceedToHomeScreen(AppConstants.CATEGORY_CUSTOM);
        }
    }

    public void proceedToHomeScreen( @NonNull int categorySelected){
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(AppConstants.BUNDLE_CATEGORY, categorySelected);
        intent.putExtra(AppConstants.BUNDLE_USER, mUserModel);
        switchActivity(intent, true);
    }
}
