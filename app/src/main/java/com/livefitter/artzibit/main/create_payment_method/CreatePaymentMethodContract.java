package com.livefitter.artzibit.main.create_payment_method;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.model.ShippingDetailModel;

/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public interface CreatePaymentMethodContract {
    public interface Presenter{
        void saveCreditCard(@NonNull String cardNumber,
                            @NonNull int cardExpMonth,
                            @NonNull int cardExpYear,
                            @NonNull String cardCVC,
                            @NonNull boolean isDefault);
    }

    public interface View extends BaseContractView{
        void returnToCheckOutScreen(CreditCardModel creditCard);
        void showValidationProgressIndicator();
        void hideValidationProgressIndicator();
        void showInvalidCardError();
        void showCreateCardError();
    }
}
