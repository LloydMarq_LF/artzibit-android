package com.livefitter.artzibit.main.checkout.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.databinding.ItemCheckoutShippingAddressBinding;
import com.livefitter.artzibit.model.ShippingDetailModel;

import butterknife.ButterKnife;


/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class ShippingAddressItemViewHolder extends RecyclerView.ViewHolder {


    private final ItemCheckoutShippingAddressBinding mBinding;

    public static ShippingAddressItemViewHolder create(LayoutInflater inflater, ViewGroup parent){
        ItemCheckoutShippingAddressBinding binding = ItemCheckoutShippingAddressBinding.inflate(inflater, parent, false);

        return new ShippingAddressItemViewHolder(binding);
    }

    private ShippingAddressItemViewHolder(ItemCheckoutShippingAddressBinding mBinding) {
        super(mBinding.getRoot());
        this.mBinding = mBinding;

        ButterKnife.bind(this, mBinding.getRoot());
    }

    public void bind(ShippingDetailModel shippingAddress, View.OnClickListener clickListener){
        mBinding.getRoot().setOnClickListener(clickListener);
        mBinding.setAddress(shippingAddress);
        mBinding.executePendingBindings();
    }
}
