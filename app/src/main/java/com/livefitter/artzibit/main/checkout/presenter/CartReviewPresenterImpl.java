package com.livefitter.artzibit.main.checkout.presenter;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.broadcastreceiver.CheckOutUpdatesReceiver;
import com.livefitter.artzibit.main.cart.CartComputationManager;
import com.livefitter.artzibit.main.checkout.CartReviewContract;
import com.livefitter.artzibit.main.checkout.CheckOutContract;
import com.livefitter.artzibit.model.CartModel;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.model.UserModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by LloydM on 7/17/17
 * for Livefitter
 */

public class CartReviewPresenterImpl implements CartReviewContract.Presenter, CartComputationManager.CartComputationCallback {
    private static final String TAG = CartReviewPresenterImpl.class.getSimpleName();

    private WeakReference<CartReviewContract.View> mView;

    private CartModel mCart;
    private String mCurrencySymbol;
    private ShippingDetailModel mShippingDetail;
    private CartComputationManager mCartComputationManager;

    public CartReviewPresenterImpl(CartReviewContract.View view, CartModel cart, String currencySymbol, ShippingDetailModel chosenShippingDetail) {
        this.mView = new WeakReference<CartReviewContract.View>(view);
        this.mCurrencySymbol = currencySymbol;
        this.mCart = cart;
        this.mShippingDetail = chosenShippingDetail;

        mCartComputationManager = new CartComputationManager(this);
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private CartReviewContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void navigateToNextStep() {
        Intent intent = new Intent(AppConstants.INTENT_ACTION_CHECKOUT_PROCESS);
        intent.putExtra(AppConstants.BUNDLE_CHECKOUT_NAVIGATION, 2);
        LocalBroadcastManager.getInstance(getView().getViewActivity()).sendBroadcast(intent);
    }

    @Override
    public void onViewReady() {
        if(mCart != null){
            boolean computeForInstallation = !ShippingDetailModel.shouldShowShippingChargeWarning(mShippingDetail);
            mCartComputationManager.compute(mCart, computeForInstallation);
        }
    }

    @Override
    public void onComputationFinished(CartModel cartModel, boolean didComputeForInstallation) {
        this.mCart = cartModel;
        // if the address needs a shipping charge warning,
        // we should not enable the shipping fee
        getView().updateCartItems(mCurrencySymbol, mCart, !ShippingDetailModel.shouldShowShippingChargeWarning(mShippingDetail));
    }
}
