package com.livefitter.artzibit.main.browse_artwork.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.listener.ArtworkClickListener;
import com.livefitter.artzibit.main.artwork_detail.view.ArtworkDetailScreen;
import com.livefitter.artzibit.main.browse_artwork.adapter.BrowserArtworkPagerAdapter;
import com.livefitter.artzibit.main.browse_artwork.BrowseArtworkContract;
import com.livefitter.artzibit.main.browse_artwork.presenter.BrowseArtworkPresenterImpl;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.util.SharedPrefsUtil;
import com.livefitter.artzibit.view.CrossFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.errorlab.widget.CheckableFloatingActionButton;


/**
 * Created by LloydM on 5/25/17
 * for Livefitter
 */

public class BrowseArtworkScreen extends CrossFragment implements BrowseArtworkContract.View, ArtworkClickListener {

    private static final String TAG = BrowseArtworkScreen.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.blocker_layout)
    FrameLayout flBlocker;
    //@BindView(R.id.toolbar_spinner)
    //Spinner spnModeSelection;
    @BindView(R.id.browse_artwork_viewpager)
    ViewPager vpArtwork;
    @BindView(R.id.browse_artwork_name)
    TextView tvName;
    @BindView(R.id.browse_artwork_artist)
    TextView tvArtist;
    @BindView(R.id.browse_artwork_button_cart)
    FloatingActionButton fabCart;
    @BindView(R.id.browse_artwork_button_ar)
    FloatingActionButton fabAr;
    @BindView(R.id.browse_artwork_button_fave)
    CheckableFloatingActionButton fabFave;
    ProgressDialog pdLoading;

    private BrowseArtworkContract.Presenter mPresenter;
    private BrowserArtworkPagerAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        //clear the filter preferences for now
        SharedPrefsUtil.clearFilterPreferences(getContext());

        mPresenter = new BrowseArtworkPresenterImpl(this);

        Bundle extras = getActivity().getIntent().getExtras();

        if (extras != null && extras.containsKey(AppConstants.BUNDLE_USER)) {
            mPresenter.setUserModel((UserModel) extras.getParcelable(AppConstants.BUNDLE_USER));

            /*if (extras.containsKey(AppConstants.BUNDLE_CATEGORY)) {

                mPresenter.setChosenCategory(extras.getInt(AppConstants.BUNDLE_CATEGORY));
            } else {
                mPresenter.setChosenCategory(SharedPrefsUtil.getChosenArtworkCategory(getContext()));
            }*/
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_browse_artwork, container, false);

        ButterKnife.bind(this, rootView);

        mAdapter = new BrowserArtworkPagerAdapter(getActivity(), this);

        vpArtwork.setAdapter(mAdapter);
        vpArtwork.setOffscreenPageLimit(2);
        vpArtwork.setPageTransformer(false, new ShowcasePageTransformer());
        vpArtwork.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            ArtworkModel mCurrentArtwork;
            ArtworkModel mPreviousArtwork;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                mCurrentArtwork = mPresenter.getArtworkItem(position);

                if(mCurrentArtwork != null) {
                    // Only call the onArtworkViewStart/End if the current artwork != previous artwork
                    if(mPreviousArtwork != null && !mCurrentArtwork.equals(mPreviousArtwork)){
                        updateCurrentArtwork(mCurrentArtwork);
                        mPresenter.onArtworkViewEnd(mPreviousArtwork);
                        mPresenter.onArtworkViewStart(mCurrentArtwork);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                switch (state){
                    case ViewPager.SCROLL_STATE_DRAGGING:
                        mPreviousArtwork = mCurrentArtwork;
                        break;
                    case ViewPager.SCROLL_STATE_SETTLING:
                         break;
                    case ViewPager.SCROLL_STATE_IDLE:
                        break;
                }
            }
        });

        /*spnModeSelection.setAdapter(new BrowseArtworkSpinnerAdapter(
                mToolbar.getContext(),
                mPresenter.getCategorySpinnerItems()));

        spnModeSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mPresenter.setChosenSpinnerCategory(position);

                mPresenter.retrieveArtworks(SharedPrefsUtil.getFilterPreferences(getContext()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Set the default
        spnModeSelection.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int initialSelection = mPresenter.getChosenCategory() == AppConstants.CATEGORY_FIXED ? 0 : 1;

                spnModeSelection.setSelection(initialSelection);

                spnModeSelection.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });*/

        // set the mToolbar as the actionbar as its the main fragment
        setOwnToolbar();

        if(mPresenter != null){
            mPresenter.retrieveArtworks(SharedPrefsUtil.getFilterPreferences(getContext()));
        }

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_browse_artwork, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d(TAG, "clicked MAP ");
                return true;
            case R.id.action_refresh:
                if(mPresenter != null){
                    mPresenter.retrieveArtworks(SharedPrefsUtil.getFilterPreferences(getContext()));
                    return true;
                }
                return false;
            case R.id.action_cart:
                Log.d(TAG, "clicked Cart ");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mPresenter != null){
            mPresenter.submitArtworkViewStatistics();
        }
    }

    @OnClick(R.id.browse_artwork_button_cart)
    public void cartButtonClick(View view) {
        ArtworkModel artworkModel = mAdapter.getArtwork(vpArtwork.getCurrentItem());
        if(artworkModel != null && mPresenter != null) {
            mPresenter.addToCart(artworkModel);
        }
    }

    @OnClick(R.id.browse_artwork_button_ar)
    public void arButtonClick(View view) {
        Log.d(TAG, "arButtonClick ");
    }

    @OnClick(R.id.browse_artwork_button_fave)
    public void faveButtonClick(View view) {
        if(mAdapter != null) {
            ArtworkModel artworkModel = mAdapter.getArtwork(vpArtwork.getCurrentItem());
            if(artworkModel != null) {
                mPresenter.favoriteArtwork(artworkModel);
            }
        }
    }

    @Override
    public void setOwnToolbar() {
        getViewActivity().setSupportActionBar(null);
        getViewActivity().setSupportActionBar(mToolbar);
        getViewActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getViewActivity().getSupportActionBar().setTitle(null);
        //mToolbar.setNavigationIcon(R.drawable.ic_star_actionbar);
//        getViewActivity().getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_star_actionbar);
        getViewActivity().invalidateOptionsMenu();
    }

    @Override
    public void onVisible() {
        //
    }

    @Override
    public void onHidden() {
        //
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void saveChosenCategoryPreference(int chosenCategory) {
        SharedPrefsUtil.setChosenArtworkCategory(getContext(), chosenCategory);
    }

    @Override
    public void updateArtworkList(ArrayList<ArtworkModel> artworkList) {
        if(mAdapter != null && mPresenter != null){
            mAdapter.setArtworkList(artworkList);
            vpArtwork.setCurrentItem(0);

            mPresenter.onArtworkViewStart(artworkList.get(0));
        }
    }

    @Override
    public void updateArtworkList(ArtworkModel artworkModel) {
        if(mAdapter != null){
            Log.d(TAG, "updateArtworkList: artwork count for " + artworkModel.getName() + " - view count " + artworkModel.getViewCounter());
            mAdapter.updateArtwork(artworkModel);
        }
    }

    @Override
    public void updateCurrentArtwork(ArtworkModel artworkModel) {

        tvName.animate().alpha(0).start();
        tvName.setText(artworkModel.getName());
        tvName.animate().alpha(100).start();

        tvArtist.animate().alpha(0).start();
        tvArtist.setText(artworkModel.getArtistName());
        tvArtist.animate().alpha(100).start();

        fabFave.setChecked(artworkModel.isFaved());
    }

    @Override
    public void toggleBlockerLayout(boolean shouldShow) {
        int visibility = shouldShow? View.VISIBLE : View.GONE;
        flBlocker.setVisibility(visibility);
    }

    @Override
    public void showArtworkRetrievalProgressDialog() {
        if (pdLoading == null) {
            String progressMessage = getString(R.string.msg_progress_get_artworks);
            pdLoading = new ProgressDialog(getActivity());
            pdLoading.setMessage(progressMessage);
            pdLoading.setCancelable(false);
        }

        pdLoading.show();
    }

    @Override
    public void showArtworkRetrievalErrorDialog() {
        String message = getString(R.string.msg_error_get_artworks);
        DialogUtil.showAlertDialog(getActivity(), message);
    }

    @Override
    public void showArtworkRetrievalEmptyErrorDialog() {
        String message = getString(R.string.msg_error_get_artworks_empty);
        DialogUtil.showAlertDialog(getActivity(), message);
    }

    @Override
    public void showArtworkFaveErrorMessage() {
        Toast.makeText(getActivity(), R.string.msg_error_fave_artwork, Toast.LENGTH_LONG).show();
    }

    @Override
    public void hideProgressDialog() {
        if (pdLoading != null && pdLoading.isShowing()) {
            pdLoading.dismiss();
        }
    }

    @Override
    public void proceedToArtworkDetailScreen(ArtworkModel artworkModel) {

        Log.d(TAG, "proceedToArtworkDetailScreen: " + artworkModel.getName() + " - view count " + artworkModel.getViewCounter());
        Intent intent = new Intent(getActivity(), ArtworkDetailScreen.class);
        intent.putExtra(AppConstants.BUNDLE_ARTWORK, artworkModel);
        startActivity(intent);
    }

    @Override
    public void onArtworkClick(ArtworkModel artworkModel) {
        proceedToArtworkDetailScreen(artworkModel);
    }
}
