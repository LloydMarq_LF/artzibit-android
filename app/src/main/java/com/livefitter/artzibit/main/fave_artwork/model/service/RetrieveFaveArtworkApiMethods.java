package com.livefitter.artzibit.main.fave_artwork.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.FaveArtworkModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

public interface RetrieveFaveArtworkApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @GET(AppConstants.URL_ARTWORKS_FAVORITES)
    Call<BaseResponseModel<ArrayList<FaveArtworkModel>>> retrieveFaveArtwork(@NonNull @Header("X-User-Email") String userEmail,
                                                                             @NonNull @Header("X-User-Token") String authToken);
}