package com.livefitter.artzibit.main.cart.model.service;

import android.support.annotation.NonNull;

import com.google.gson.internal.LinkedTreeMap;
import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.cart.model.request.CartRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.CartModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by LloydM on 7/5/17
 * for Livefitter
 */

public interface UpdateCartApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @PUT(AppConstants.URL_CART + "/{cartId}")
    Call<BaseResponseModel<LinkedTreeMap>> updateCart(@NonNull @Header("X-User-Email") String userEmail,
                                                      @NonNull @Header("X-User-Token") String authToken,
                                                      @NonNull @Path("cartId") int cartId,
                                                      @NonNull @Body CartRequestModel body);
}
