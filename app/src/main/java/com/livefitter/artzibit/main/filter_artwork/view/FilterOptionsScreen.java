package com.livefitter.artzibit.main.filter_artwork.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.filter_artwork.adapter.FilterOptionsAdapter;
import com.livefitter.artzibit.model.ArtworkFilterModel;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LloydM on 6/15/17
 * for Livefitter
 */

public class FilterOptionsScreen extends BaseActivity {

    private static final String TAG  = FilterOptionsScreen.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerview)
    RecyclerView rvFilterOptions;

    ArtworkFilterModel filterModel;
    FilterOptionsAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_options);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();

        if(extras != null && extras.containsKey(AppConstants.BUNDLE_FILTER)){
            filterModel = extras.getParcelable(AppConstants.BUNDLE_FILTER);

            getSupportActionBar().setTitle(filterModel.getName());

            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvFilterOptions.setLayoutManager(layoutManager);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvFilterOptions.getContext(),
                    layoutManager.getOrientation());
            rvFilterOptions.addItemDecoration(dividerItemDecoration);
            mAdapter = new FilterOptionsAdapter(this, filterModel);
            rvFilterOptions.setAdapter(mAdapter);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home){
            retrieveSelectedFilterOption();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void retrieveSelectedFilterOption() {
        filterModel = mAdapter.getFilterModel();

        Intent intent = new Intent();
        intent.putExtra(AppConstants.BUNDLE_FILTER, filterModel);

        setResult(Activity.RESULT_OK, intent);
    }
}
