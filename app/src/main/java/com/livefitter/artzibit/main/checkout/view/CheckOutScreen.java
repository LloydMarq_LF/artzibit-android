package com.livefitter.artzibit.main.checkout.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.browse_artwork.view.HomeActivity;
import com.livefitter.artzibit.main.checkout.CheckOutContract;
import com.livefitter.artzibit.main.checkout.adapter.CheckOutPagerAdapter;
import com.livefitter.artzibit.main.checkout.presenter.CheckOutPresenterImpl;
import com.livefitter.artzibit.model.CartModel;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.view.ConditionalPagerFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckOutScreen extends BaseActivity implements CheckOutPagerAdapter.OnPageConditionChangeListener, CheckOutContract.View {

    private static final String TAG = CheckOutScreen.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.checkout_tab_shipping)
    ConstraintLayout clTabShipping;
    @BindView(R.id.checkout_tab_payment)
    ConstraintLayout clTabPayment;
    @BindView(R.id.checkout_tab_review)
    ConstraintLayout clTabReview;
    @BindView(R.id.viewpager)
    ViewPager vpFragments;

    ProgressDialog dProgress;

    private CheckOutPagerAdapter mAdapter;
    private ArrayList<ConstraintLayout> mTabsList = new ArrayList<>();
    private CheckOutContract.Presenter mPresenter;

    private ShippingDetailsListScreen mShippingFragment;
    private PaymentMethodsListScreen mPaymentFragment;
    private CartReviewScreen mReviewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_screen);

        if (getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(AppConstants.BUNDLE_USER)
                && getIntent().getExtras().containsKey(AppConstants.BUNDLE_CART)) {
            UserModel user = getIntent().getExtras().getParcelable(AppConstants.BUNDLE_USER);
            CartModel cart = getIntent().getExtras().getParcelable(AppConstants.BUNDLE_CART);

            mPresenter = new CheckOutPresenterImpl(this, user, cart);

            ButterKnife.bind(this);

            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.

            ArrayList<ConditionalPagerFragment> fragmentList = new ArrayList<>();
            mShippingFragment = new ShippingDetailsListScreen();
            fragmentList.add(mShippingFragment);

            mPaymentFragment = new PaymentMethodsListScreen();
            fragmentList.add(mPaymentFragment);

            //mReviewFragment = TestConditionalFragment.newInstance(fragmentList.size() + 1);
            mReviewFragment = new CartReviewScreen();
            fragmentList.add(mReviewFragment);

            mAdapter = new CheckOutPagerAdapter(getSupportFragmentManager(), fragmentList, this);
            vpFragments.setAdapter(mAdapter);

            mTabsList.add(clTabShipping);
            mTabsList.add(clTabPayment);
            mTabsList.add(clTabReview);

            if (mTabsList != null) {
                for (int i = 0; i < fragmentList.size(); i++) {
                    ConditionalPagerFragment pagerFragment = fragmentList.get(i);
                    if (i + 1 < fragmentList.size()) {
                        mTabsList.get(i + 1).setEnabled(pagerFragment.canProgress());
                    }
                }

                mTabsList.get(vpFragments.getCurrentItem()).setSelected(true);
            }

            vpFragments.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (mTabsList != null && !mTabsList.isEmpty()) {
                        for (int i = 0; i < mTabsList.size(); i++) {
                            mTabsList.get(i).setSelected(i == position);
                        }
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            vpFragments.setCurrentItem(0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mPresenter != null) {
            mPresenter.registerBroadcastReceiver();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mPresenter != null) {
            mPresenter.unregisterBroadcastReceiver();
        }
    }

    @OnClick({R.id.checkout_tab_shipping, R.id.checkout_tab_payment, R.id.checkout_tab_review})
    public void onTabClick(View view) {
        if (view != null && view instanceof ConstraintLayout
                && mTabsList != null && !mTabsList.isEmpty()) {
            if (mTabsList.contains(view)) {
                for (ConstraintLayout tabLayout : mTabsList) {
                    if (tabLayout.isEnabled()) {
                        tabLayout.setSelected(tabLayout == view);
                    }
                }

                vpFragments.setCurrentItem(mTabsList.indexOf(view));
            }
        }
    }

    @Override
    public void onPageConditionChange(int position, boolean canProgress) {

        int nextPagePosition = position + 1;

        if (canProgress) {
            // Enable the next item
            if (nextPagePosition > 0 && nextPagePosition < mTabsList.size()) {
                mTabsList.get(nextPagePosition).setEnabled(true);
            }
        } else {
            // iterate through the tabs list and disable all items from position + 1 to the end

            for (int i = nextPagePosition; i < mTabsList.size(); i++) {
                mTabsList.get(i).setEnabled(false);
            }
        }
    }

    @Override
    public BaseActivity getViewActivity() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return null;
    }

    @Override
    public void navigateToPage(int childIndex) {
        if(vpFragments != null
                && childIndex >= 0 && childIndex < vpFragments.getChildCount()){

            vpFragments.setCurrentItem(childIndex);
        }
    }

    @Override
    public void proceedToReceiptScreen(String orderNumber) {
        //TODO enable when ready
        /*Intent intent = new Intent(this, ReceiptScreen.class);
        intent.putExtra(AppConstants.BUNDLE_RECEIPT_NUMBER, orderNumber);
        switchActivity(intent, true);*/

        //TODO temporary behavior. should bring them to receipt screen, then this activity should be gone
        Toast.makeText(this, "Checkout successful", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showCheckOutProgress() {
        if(dProgress == null){
            String message = getString(R.string.msg_progress_finish_checkout);
            dProgress = DialogUtil.showIndeterminateProgressDialog(this,
                    "",
                    message,
                    false);
        }else{
            dProgress.show();
        }
    }

    @Override
    public void hideCheckOutProgress() {
        if(dProgress != null && dProgress.isShowing()){
            dProgress.dismiss();
        }
    }

    @Override
    public void showCheckOutCartError() {
        String message = getString(R.string.msg_error_checkout_cart);
        DialogUtil.showAlertDialog(this, message);
    }

    public ShippingDetailModel getSelectedShippingModel(){
        if(mPresenter != null){
            Log.d(TAG, "getSelectedShippingModel: " + mPresenter.getSelectedShippingDetail());
            return mPresenter.getSelectedShippingDetail();
        }

        Log.d(TAG, "getSelectedShippingModel: NULL");
        return null;
    }
}
