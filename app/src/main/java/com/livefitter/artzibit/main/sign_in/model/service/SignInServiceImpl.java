package com.livefitter.artzibit.main.sign_in.model.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.livefitter.artzibit.main.sign_in.model.request.SignInRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 5/12/17
 * for Livefitter
 */

public class SignInServiceImpl implements SignInService, Callback<BaseResponseModel<UserModel>> {

    private static final String TAG = SignInService.class.getSimpleName();

    private SignInCallback mCallback;

    @Override
    public void doSignIn(SignInRequestModel params, final SignInCallback callback) {
        mCallback = callback;

        SignInApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(SignInApiMethods.class);

        apiMethods.signUp(params).enqueue(this);
    }

    @Override
    public void onResponse(Call<BaseResponseModel<UserModel>> call, Response<BaseResponseModel<UserModel>> response) {

        Log.d(TAG, "onResponse: " + response);
        if(response.isSuccessful()){

            Log.d(TAG, "response success body: " + response.body().toString());

            if(response.body().getStatus()){
                UserModel userModel = response.body().getData();

                if(userModel != null){
                    mCallback.onSuccess(userModel);
                }else{
                    mCallback.onInvalidSignIn();
                }
            }else{
                Log.d(TAG, "response unsuccessful: ");
                mCallback.onInvalidSignIn();
            }
        }else{
            Log.d(TAG, "response unsuccessful: "+ response.errorBody());
            mCallback.onInvalidSignIn();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<UserModel>> call, Throwable t) {
        Log.d(TAG, "onFailure: ");
        t.printStackTrace();
        mCallback.onError();
    }
}
