package com.livefitter.artzibit.main.fave_artwork.model.service;

import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkRequestModel;
import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkUpdateRequestModel;

/**
 * Created by LloydM on 5/12/17
 * for Livefitter
 */

public interface FaveArtworkService {
    void faveArtwork(String userEmail, String userAuthToken, FaveArtworkRequestModel body, FaveArtworkCallback callback);
    void updateFaveArtwork(String userEmail, String userAuthToken, int artworkId, int favoriteId, FaveArtworkUpdateRequestModel body, FaveArtworkCallback callback);

    interface FaveArtworkCallback {
        void onFaveArtworkSuccess(int artworkId, boolean isDeleted, int artworkFavedId);
        void onFaveArtworkError(int artworkId);
    }
}
