package com.livefitter.artzibit.main.artwork_detail.model.service;

import android.util.Log;

import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 6/28/17
 * for Livefitter
 */

public class RetrieveArtworkDetailsServiceImpl implements RetrieveArtworkDetailsService, Callback<BaseResponseModel<ArtworkModel>> {

    private static final String TAG = RetrieveArtworkDetailsServiceImpl.class.getSimpleName();

    Call<BaseResponseModel<ArtworkModel>> mCall;
    RetrieveArtworkDetailsCallback mCallback;

    @Override
    public void retrieveArtworkDetails(String userEmail, String authToken, int artworkID, RetrieveArtworkDetailsCallback callback) {
        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }

        mCallback = callback;

        RetrieveArtworkDetailsApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(RetrieveArtworkDetailsApiMethods.class);

        mCall = apiMethods.retrieveArtworks(userEmail, authToken, artworkID);

        mCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<BaseResponseModel<ArtworkModel>> call, Response<BaseResponseModel<ArtworkModel>> response) {
        if(response.isSuccessful()){

            if(response.body().getStatus()) {

                ArtworkModel artwork = response.body().getData();

                if(artwork != null) {
                    mCallback.onRetrieveArtworkSuccess(artwork);
                }else{
                    mCallback.onRetrieveArtworkError();
                }
            }else{
                Log.d(TAG, "response unsuccessful: "+ response.errorBody());
                mCallback.onRetrieveArtworkError();
            }
        }else{
            Log.d(TAG, "response failed body: "+ response.errorBody());
            mCallback.onRetrieveArtworkError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<ArtworkModel>> call, Throwable t) {
        t.printStackTrace();
        mCallback.onRetrieveArtworkError();
    }
}
