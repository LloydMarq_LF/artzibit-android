package com.livefitter.artzibit.main.welcome.model.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.livefitter.artzibit.main.welcome.model.request.FbSignInRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 5/30/17
 * for Livefitter
 */

public class FbSignInServiceImpl implements FbSignInService, Callback<BaseResponseModel<Object>> {

    private static final String TAG = FbSignInServiceImpl.class.getSimpleName();

    private SignInCallback mCallback;

    @Override
    public void doSignInViaFacebook(FbSignInRequestModel params, final SignInCallback callback) {
        mCallback = callback;
        FbSignInApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(FbSignInApiMethods.class);

        apiMethods.signInViaFacebook(params).enqueue(this);
    }

    @Override
    public void onResponse(Call<BaseResponseModel<Object>> call, Response<BaseResponseModel<Object>> response) {
        Log.d(TAG, "onResponse: " + response);

        if(response.isSuccessful()) {
            Log.d(TAG, "response success body: " + response.body().toString());
            BaseResponseModel baseResponseModel = response.body();

            if(baseResponseModel.getStatus()){
                // Convert the received data into a UserModel
                Log.d(TAG, "response success data: " + baseResponseModel.getData());
                LinkedTreeMap map = (LinkedTreeMap) baseResponseModel.getData();
                Gson gson = new Gson();
                JsonObject jsonObject = gson.toJsonTree(map).getAsJsonObject();

                Type type = new TypeToken<UserModel>(){}.getType();
                UserModel userModel = new Gson().fromJson(jsonObject, type);

                Log.d(TAG, "onResponse: userModel " + userModel);

                String encoded = UserModel.encode(userModel);
                Log.d(TAG, "encoding: " + encoded);

                mCallback.onSignInSuccess(userModel);
            }else{
                mCallback.onUnregisteredSignIn();
            }

        }else{
            Log.d(TAG, "response not successful");
            mCallback.onSignInError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<Object>> call, Throwable t) {
        Log.d(TAG, "response failed");
        t.printStackTrace();
        mCallback.onSignInError();
    }
}
