package com.livefitter.artzibit.main.statistic.model.request;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.livefitter.artzibit.model.ArtworkModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/19/17
 * for Livefitter
 */

public class ArtworkViewCountRequestModel {

    @SerializedName("user_view")
    private UserView userView;

    public ArtworkViewCountRequestModel(int userId, ArrayList<ArtworkModel> artworkModelList) {


        ArrayList<ArtworkViewCountRequestModel.UserViewValues> userViewValuesArrayList = new ArrayList<>();

        for(ArtworkModel artworkModel : artworkModelList){
            if(artworkModel.getViewCounter() > 0) {
                ArtworkViewCountRequestModel.UserViewValues userViewValues
                        = new ArtworkViewCountRequestModel.UserViewValues(userId, artworkModel);

                userViewValuesArrayList.add(userViewValues);
            }
        }

        this.userView = new UserView(userViewValuesArrayList);
    }

    public class UserView{

        private ArrayList<UserViewValues> values;

        public UserView(ArrayList<UserViewValues> artworkValues) {
            this.values = artworkValues;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }

    public class UserViewValues {

        /**
         * user_id : 1
         * artwork_id : 9
         * counter : 2
         */

        @SerializedName("user_id")
        private int userId;
        @SerializedName("artwork_id")
        private int artworkId;
        private int counter;

        public UserViewValues(int userId, ArtworkModel artworkModel){
            this.userId = userId;
            this.artworkId = artworkModel.getId();
            this.counter = artworkModel.getViewCounter();
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
