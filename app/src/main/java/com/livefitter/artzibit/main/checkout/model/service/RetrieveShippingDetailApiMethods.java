package com.livefitter.artzibit.main.checkout.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.ShippingDetailModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public interface RetrieveShippingDetailApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @GET(AppConstants.URL_SHIPPING_DETAILS)
    Call<BaseResponseModel<ArrayList<ShippingDetailModel>>> retrieveShippingDetail(@NonNull @Header("X-User-Email") String userEmail,
                                                                                   @NonNull @Header("X-User-Token") String authToken);
}
