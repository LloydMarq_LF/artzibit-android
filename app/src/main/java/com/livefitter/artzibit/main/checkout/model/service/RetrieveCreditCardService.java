package com.livefitter.artzibit.main.checkout.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.model.ShippingDetailModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public interface RetrieveCreditCardService {

    void retrieveCreditCard(@NonNull String email, @NonNull String authToken, Callback callback);
    void cancel();

    public interface Callback{
        void onRetrieveCreditCardSuccess(ArrayList<CreditCardModel> creditCardModels);
        void onRetrieveCreditCardError();
    }
}
