package com.livefitter.artzibit.main.sign_up.model.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.livefitter.artzibit.main.sign_up.model.request.SignUpRequestModel;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 5/15/17
 * for Livefitter
 */

public class SignUpServiceImpl implements SignUpService, Callback<Object> {

    private final String TAG = SignUpServiceImpl.class.getSimpleName();

    private SignUpCallback mCallback;

    @Override
    public void doSignUp(SignUpRequestModel params, final SignUpCallback callback) {
        mCallback = callback;
        SignUpApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(SignUpApiMethods.class);
        apiMethods.signUp(params).enqueue(this);
    }

    @Override
    public void onResponse(Call<Object> call, Response<Object> response) {
        Log.d(TAG, "onResponse: " + response);
        if(response.isSuccessful()){

            Log.d(TAG, "response success body: " + response.body().toString());
            // Convert the received data into a UserModel
            LinkedTreeMap map = (LinkedTreeMap) response.body();
            Gson gson = new Gson();
            JsonObject jsonObject = gson.toJsonTree(map).getAsJsonObject();

            Type type = new TypeToken<UserModel>(){}.getType();
            UserModel userModel = new Gson().fromJson(jsonObject, type);

            mCallback.onSuccess(userModel);
        }else{
            Log.d(TAG, "response failed body: "+ response.errorBody());
            mCallback.onAccountAlreadyTakenError();
        }
    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {
        Log.d(TAG, "onFailure: ");
        t.printStackTrace();
        mCallback.onError();
    }
}
