package com.livefitter.artzibit.main.artwork_detail.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.databinding.ActivityArtworkDetailBinding;
import com.livefitter.artzibit.main.artwork_detail.adapter.ArtworkSizeSpinnerAdapter;
import com.livefitter.artzibit.main.artwork_detail.ArtworkDetailContract;
import com.livefitter.artzibit.main.artwork_detail.presenter.ArtworkDetailPresenterImpl;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.ArtworkSizeModel;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.util.FormValidityHelper;
import com.livefitter.artzibit.util.SharedPrefsUtil;
import com.livefitter.artzibit.view.FormFieldTextWatcher;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.errorlab.widget.CheckableFloatingActionButton;


/**
 * Created by LloydM on 6/21/17
 * for Livefitter
 */

public class ArtworkDetailScreen extends BaseActivity implements ArtworkDetailContract.View {

    private static final String TAG = ArtworkDetailScreen.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout srLayout;
    @BindView(R.id.artwork_detail_button_fave)
    CheckableFloatingActionButton fabFave;
    @BindView(R.id.artwork_detail_button_ar)
    FloatingActionButton fabAr;
    @BindView(R.id.artwork_detail_button_cart)
    FloatingActionButton fabCart;
    @BindView(R.id.artwork_detail_spinner_size)
    Spinner spnArtworkSize;
    @BindView(R.id.artwork_detail_til_height)
    TextInputLayout tilHeight;
    @BindView(R.id.artwork_detail_til_width)
    TextInputLayout tilWidth;
    @BindView(R.id.artwork_detail_et_height)
    TextInputEditText etHeight;
    @BindView(R.id.artwork_detail_et_width)
    TextInputEditText etWidth;
    @BindView(R.id.artwork_detail_installation_checkbox)
    CheckBox cbInstallation;
    @BindView(R.id.quantity_stepper_count)
    TextView tvQuantity;
    ProgressDialog dAddToCartProgress;

    ActivityArtworkDetailBinding mBinding;
    ArtworkDetailContract.Presenter mPresenter;

    ArtworkSizeSpinnerAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_artwork_detail);

        int artworkId = 0;

        Bundle extras = getIntent().getExtras();

        if(extras != null){
            if(extras.containsKey(AppConstants.BUNDLE_ARTWORK_ID)) {
                artworkId = extras.getInt(AppConstants.BUNDLE_ARTWORK_ID, 0);
            }else if(extras.containsKey(AppConstants.BUNDLE_ARTWORK)){
                ArtworkModel artwork = extras.getParcelable(AppConstants.BUNDLE_ARTWORK);
                if(artwork != null){
                    artworkId = artwork.getId();
                }
            }
        }

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        UserModel userModel = SharedPrefsUtil.getUserAuth(this);

        if(userModel != null && artworkId != 0){
            mPresenter = new ArtworkDetailPresenterImpl(this, artworkId, userModel);
            mPresenter.retrieveArtworkDetails();
        }else{
            finish();
        }

        srLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.retrieveArtworkDetails();
            }
        });

        spnArtworkSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(mAdapter != null && mPresenter != null) {
                    mPresenter.onSizeSpinnerItemSelected(mAdapter.getLabelAt(position));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        etHeight.addTextChangedListener(new FormFieldTextWatcher(tilHeight));
        etWidth.addTextChangedListener(new FormFieldTextWatcher(tilWidth));
        etHeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length() == 0){
                    etWidth.getText().clear();
                }else{
                    if(mPresenter != null){
                        try {
                            float height = Float.valueOf(s.toString());
                            mPresenter.onSizeValueChange(0, height);
                        }catch (NumberFormatException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        etWidth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length() == 0){
                    etHeight.getText().clear();
                }else{
                    if(mPresenter != null){
                        try {
                            float width = Float.valueOf(s.toString());
                            mPresenter.onSizeValueChange(width, 0);
                        }catch (NumberFormatException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        cbInstallation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPresenter.onInstallationCheckboxChecked(isChecked);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(mPresenter != null){
            mPresenter.onArtworkViewEnd();
        }
    }

    @Override
    public BaseActivity getViewActivity() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void updateArtwork(ArtworkModel artwork) {
        mBinding.setArtwork(artwork);
        fabFave.setChecked(artwork.isFaved());
    }

    @Override
    public void updateSizeSpinnerItems(ArrayList<String> artworkSizeLabels) {
        if(mAdapter == null) {
            mAdapter = new ArtworkSizeSpinnerAdapter(this,
                    R.layout.item_artwork_size,
                    R.id.artwork_detail_spinner_item_label,
                    artworkSizeLabels);
        }else{
            mAdapter.clear();
            mAdapter.addAll(artworkSizeLabels);
        }


        spnArtworkSize.setAdapter(mAdapter);
    }

    @Override
    public void updateArtworkQuantity(int quantity) {
        mBinding.setQuantity(quantity);
        mBinding.executePendingBindings();
    }

    @Override
    public void toggleCustomSizeLayoutVisibility(boolean isVisible) {
        mBinding.setIsUsingCustomSize(isVisible);
        mBinding.executePendingBindings();
    }

    @Override
    public void resetCustomSizeFields() {

        tilHeight.setError(null);
        tilWidth.setError(null);
        etHeight.getText().clear();
        etWidth.getText().clear();
    }

    @Override
    public void showRetrievalProgress() {
        srLayout.setRefreshing(true);
    }

    @Override
    public void hideRetrievalProgress() {
        srLayout.setRefreshing(false);
    }

    @Override
    public void showAddToCartProgress() {

        String message = getString(R.string.msg_progress_add_to_cart);
        dAddToCartProgress = ProgressDialog.show(this, null, message, true, false, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if(mPresenter != null){
                    mPresenter.onAddToCartCancel();
                }
            }
        });

        dAddToCartProgress.show();
    }

    @Override
    public void hideAddToCartProgress() {
        if(dAddToCartProgress != null && dAddToCartProgress.isShowing()) {
            dAddToCartProgress.dismiss();
        }
    }

    @Override
    public void showAddToCartSuccess() {
        String message = getString(R.string.msg_success_add_to_cart);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void showAddToCartError() {
        String message = getString(R.string.msg_error_add_to_cart);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void showArtworkRetrievalError() {
        String message = getString(R.string.msg_error_get_artwork_details);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void showArtworkFaveErrorMessage() {
        Toast.makeText(this, R.string.msg_error_fave_artwork, Toast.LENGTH_LONG).show();
    }

    @Override
    public void changeWidthField(float width) {
        if(etWidth != null && tilWidth != null){
            etWidth.setText(String.valueOf(width));
        }
    }

    @Override
    public void changeHeightField(float height) {
        if(etHeight != null && tilHeight != null){
            etHeight.setText(String.valueOf(height));
        }
    }

    @OnClick(R.id.artwork_detail_button_fave)
    protected void favoriteArtwork(){
        mPresenter.favoriteArtwork();
    }

    @OnClick(R.id.artwork_detail_button_ar)
    protected void viewArtworkInAR(){
        mPresenter.viewArtworkInAR();
    }

    @OnClick(R.id.artwork_detail_button_cart)
    protected void addToCart(){
        hideSoftKeyboard();
        if(areFieldsValid()){
            if(mBinding.getIsUsingCustomSize()){

                float height = Float.parseFloat(etHeight.getText().toString());
                float width = Float.parseFloat(etWidth.getText().toString());
                mPresenter.addArtworkToCart(height,width);
            }else{
                mPresenter.addArtworkToCart();
            }
        }
    }

    @OnClick({R.id.quantity_stepper_minus, R.id.quantity_stepper_plus})
    protected void changeQuantity(ImageButton button){
        mPresenter.changeArtworkQuantity(button.getId() == R.id.quantity_stepper_plus);
    }

    private boolean areFieldsValid(){
        boolean areFieldsValid = true;

        try {
            int quantity = Integer.parseInt(tvQuantity.getText().toString());
            if (quantity <= 0) {
                String errorMessage = getString(R.string.msg_error_zero_quantity_for_cart);
                DialogUtil.showAlertDialog(this, errorMessage);

                return false;
            }
        }catch (NumberFormatException e){
            e.printStackTrace();
        }

        // Check first if the fields are visible
        if(!mBinding.getIsUsingCustomSize()){
            return true;
        }

        if(mPresenter != null) {

            // Check if the values are within the acceptable sizes for the artwork
            ArtworkSizeModel minSize = mPresenter.getMinimumSize();
            ArtworkSizeModel maxSize = mPresenter.getMaximumSize();

            /*int height = Integer.parseInt(etHeight.getText().toString());
            int width = Integer.parseInt(etWidth.getText().toString());*/


            if (!FormValidityHelper.isFieldComplete(etHeight)) {
                areFieldsValid = false;
                String errorMessage = getString(R.string.msg_error_required_field);
                tilHeight.setError(errorMessage);
            } else if (Float.parseFloat(etHeight.getText().toString()) <= 0) {
                areFieldsValid = false;
                String errorMessage = getString(R.string.msg_error_value_not_positive_number);
                tilHeight.setError(errorMessage);
            }else if(minSize != null && maxSize != null
                    && !FormValidityHelper.isFieldValueInRange(etHeight, minSize.getHeight(), maxSize.getHeight())){
                areFieldsValid = false;
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                decimalFormat.format(minSize.getHeight());

                String fieldName = getString(R.string.label_height);
                String errorMessage = getString(R.string.msg_error_value_not_in_range,
                        fieldName,
                        decimalFormat.format(minSize.getHeight()),
                        decimalFormat.format(maxSize.getHeight()));
                tilHeight.setError(errorMessage);
            }

            if (!FormValidityHelper.isFieldComplete(etWidth)) {
                areFieldsValid = false;
                String errorMessage = getString(R.string.msg_error_required_field);
                tilWidth.setError(errorMessage);
            } else if (Float.parseFloat(etWidth.getText().toString()) <= 0) {
                areFieldsValid = false;
                String errorMessage = getString(R.string.msg_error_value_not_positive_number);
                tilWidth.setError(errorMessage);
            }else if(minSize != null && maxSize != null
                    && !FormValidityHelper.isFieldValueInRange(etWidth, minSize.getWidth(), maxSize.getWidth())){

                areFieldsValid = false;
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                decimalFormat.format(minSize.getHeight());
                String fieldName = getString(R.string.label_width);
                String errorMessage = getString(R.string.msg_error_value_not_in_range,
                        fieldName,
                        decimalFormat.format(minSize.getWidth()),
                        decimalFormat.format(maxSize.getWidth()));
                tilWidth.setError(errorMessage);
            }
        }

        return areFieldsValid;
    }
}
