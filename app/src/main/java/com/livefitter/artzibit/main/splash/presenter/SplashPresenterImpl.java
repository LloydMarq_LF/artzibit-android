package com.livefitter.artzibit.main.splash.presenter;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.main.splash.SplashContract;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.SharedPrefsUtil;

import java.lang.ref.WeakReference;

/**
 * Created by LloydM on 5/11/17
 * for Livefitter
 */

public class SplashPresenterImpl implements SplashContract.Presenter {
    private WeakReference<SplashContract.View> mView;

    public SplashPresenterImpl(@NonNull SplashContract.View view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void checkUserLoggedIn() {
        UserModel userModel = getView().retrieveUserDetails();
        if(userModel == null){
            // User is not logged in
            getView().proceedToWelcomeScreen();
        }else{
            getView().proceedToHomeScreen(userModel);
        }
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private SplashContract.View getView() throws NullPointerException{
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }
}
