package com.livefitter.artzibit.main.filter_artwork.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.filter_artwork.adapter.FilterAdapter;
import com.livefitter.artzibit.main.filter_artwork.FilterContract;
import com.livefitter.artzibit.main.filter_artwork.presenter.FilterPresenterImpl;
import com.livefitter.artzibit.model.ArtworkFilterModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.util.SharedPrefsUtil;
import com.livefitter.artzibit.view.CrossFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LloydM on 6/13/17
 * for Livefitter
 */

public class FilterScreen extends CrossFragment implements FilterContract.View, FilterAdapter.FilterClickListener {

    private static final String TAG = FilterScreen.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.blocker_layout)
    FrameLayout flBlocker;
    @BindView(R.id.recyclerview)
    RecyclerView rvFilters;

    ProgressDialog pdLoading;
    Menu mMenu;

    FilterPresenterImpl mPresenter;
    FilterAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mPresenter = new FilterPresenterImpl(this);

        //todo Find a way to let this child handle vertical scrolls to let CrossContainer play nice with child views
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_artwork_filter, container, false);
        ButterKnife.bind(this, rootView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        rvFilters.setLayoutManager(layoutManager);
        mAdapter = new FilterAdapter(getActivity(), this);
        rvFilters.setAdapter(mAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvFilters.getContext(),
                layoutManager.getOrientation());
        rvFilters.addItemDecoration(dividerItemDecoration);

        return rootView;
    }

    @Override
    public void setOwnToolbar() {
        getViewActivity().setSupportActionBar(null);
        getViewActivity().setSupportActionBar(mToolbar);
        getViewActivity().getSupportActionBar().setTitle(R.string.title_filter);
        //getViewActivity().invalidateOptionsMenu();
    }

    @Override
    public void onVisible() {
        mPresenter.retrieveFilterOptions();
        getActivity().invalidateOptionsMenu();
        getActivity().getMenuInflater().inflate(R.menu.menu_empty, mMenu);
    }

    @Override
    public void onHidden() {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mMenu = menu;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void saveFilterPreferences(ArrayList<ArtworkFilterModel> filterModelList) {
        SharedPrefsUtil.saveArtworkFilters(getContext(), filterModelList);
    }

    @Override
    public void updateFilterList(ArrayList<ArtworkFilterModel> filterModelList) {
        mAdapter.setFilterList(filterModelList);
    }

    @Override
    public void showProgressDialog() {

        if (pdLoading == null) {
            String progressMessage = getString(R.string.msg_progress_get_artwork_filters);
            pdLoading = new ProgressDialog(getActivity());
            pdLoading.setMessage(progressMessage);
            pdLoading.setCancelable(false);
        }

        pdLoading.show();
    }

    @Override
    public void hideProgressDialog() {
        if (pdLoading != null && pdLoading.isShowing()) {
            pdLoading.dismiss();
        }
    }

    @Override
    public void showErrorDialog() {
        String message = getString(R.string.msg_error_get_artwork_filters);
        DialogUtil.showAlertDialog(getActivity(), message);
    }

    @Override
    public void proceedToFilterSubmenu(ArtworkFilterModel filterModel) {
        Intent intent = new Intent(getActivity(), FilterOptionsScreen.class);
        intent.putExtra(AppConstants.BUNDLE_FILTER, filterModel);
        startActivityForResult(intent, AppConstants.REQUEST_CODE_FILTER_OPTIONS);
    }

    @Override
    public void onFilterClick(ArtworkFilterModel filter) {
        mPresenter.onFilterOptionSelected(filter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.onActivityResult(requestCode, resultCode, data);
    }
}
