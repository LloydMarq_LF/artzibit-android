package com.livefitter.artzibit.main.artwork_detail.model.service;

import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.livefitter.artzibit.gson.AddToCartItemAttributesSerializer;
import com.livefitter.artzibit.main.artwork_detail.model.request.AddToCartRequestModel;
import com.livefitter.artzibit.main.artwork_detail.model.request.AddToCartItemAttributes;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 6/30/17
 * for Livefitter
 */

public class AddToCartServiceImpl implements AddToCartService, Callback<BaseResponseModel<LinkedTreeMap>> {

    private static final String TAG = AddToCartServiceImpl.class.getSimpleName();

    Call<BaseResponseModel<LinkedTreeMap>> mCall;
    Callback mCallback;

    @Override
    public void addToCart(String userEmail, String authToken, AddToCartRequestModel params, Callback callback) {
        if (mCall != null && mCall.isExecuted()) {
            mCall.cancel();
        }

        mCallback = callback;

        GsonBuilder gsonBuilder = new GsonBuilder()
                .registerTypeAdapter(AddToCartItemAttributes.class, new AddToCartItemAttributesSerializer());

        AddToCartApiMethods apiMethods = RetrofitUtil.getGSONRetrofit(gsonBuilder.create()).create(AddToCartApiMethods.class);

        mCall = apiMethods.addArtworkToCart(userEmail,
                authToken,
                params);
        mCall.enqueue(this);
    }

    @Override
    public void updateCart(String userEmail, String authToken, int cartId, AddToCartRequestModel params, Callback callback) {
        if (mCall != null && mCall.isExecuted()) {
            mCall.cancel();
        }

        mCallback = callback;

        GsonBuilder gsonBuilder = new GsonBuilder()
                .registerTypeAdapter(AddToCartItemAttributes.class, new AddToCartItemAttributesSerializer());

        AddToCartApiMethods apiMethods = RetrofitUtil.getGSONRetrofit(gsonBuilder.create()).create(AddToCartApiMethods.class);

        mCall = apiMethods.addArtworkToCart(userEmail,
                authToken,
                cartId,
                params);

        mCall.enqueue(this);
    }

    @Override
    public void cancel() {
        if (mCall != null && mCall.isExecuted()) {
            mCall.cancel();
        }
    }

    @Override
    public void onResponse(Call<BaseResponseModel<LinkedTreeMap>> call, Response<BaseResponseModel<LinkedTreeMap>> response) {
        Log.d(TAG, "onResponse: " + response);

        if(response.isSuccessful()){
            Log.d(TAG, "response success body: " + response.body().toString());
            if(response.body().getStatus()){
                mCallback.onAddToCartSuccess();
            }else{
                Log.d(TAG, "response unsuccessful");
                mCallback.onAddToCartError();
            }
        }else{
            Log.d(TAG, "response failed body: "+ response.errorBody());
            mCallback.onAddToCartError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<LinkedTreeMap>> call, Throwable t) {
        t.printStackTrace();
        mCallback.onAddToCartError();
    }
}
