package com.livefitter.artzibit.main.checkout;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.CartItemModel;
import com.livefitter.artzibit.model.CartModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/17/17
 * for Livefitter
 */

public interface CartReviewContract {
    public interface Presenter{
        void navigateToNextStep();
        void onViewReady();
    }

    public interface View extends BaseContractView{
        void updateCartItems(String currencySymbol, CartModel cart, boolean installationFeeEnabled);
    }
}
