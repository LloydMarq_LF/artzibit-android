package com.livefitter.artzibit.main.statistic.model.service;

import android.util.Log;

import com.livefitter.artzibit.main.statistic.model.request.ArtworkViewCountRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.ArtworkViewDurationRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by LloydM on 7/20/17
 * for Livefitter
 */

public class StatsArtworkViewDurationServiceImpl implements StatsArtworkViewDurationService {

    private static final String TAG = StatsArtworkViewDurationServiceImpl.class.getSimpleName();


    private Callback mCallback;
    private Call<BaseResponseModel<Object>> mCall;

    @Override
    public void logArtworkViewDuration(String userEmail, String authToken, ArtworkViewDurationRequestModel body, Callback callback) {
        this.mCallback = callback;

        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }

        Log.d(TAG, "logArtworkViewDuration: body: " + body);

        StatsArtworkViewDurationApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(StatsArtworkViewDurationApiMethods.class);

        this.mCall = apiMethods.logArtworkViewDuration(userEmail, authToken, body);

        mCall.enqueue(new retrofit2.Callback<BaseResponseModel<Object>>() {
            @Override
            public void onResponse(Call<BaseResponseModel<Object>> call, Response<BaseResponseModel<Object>> response) {

                Log.d(TAG, "onViewDuration: response: " + response);

                if(response.isSuccessful()){
                    Log.d(TAG, "onViewDuration: response body: " + response.body());
                    if(response.body().getStatus()){
                        mCallback.onArtworkViewDurationSuccess();
                    }else{
                        Log.d(TAG, "onViewDuration: failed: ");
                        mCallback.onArtworkViewDurationError();
                    }
                }else{
                    Log.d(TAG, "onViewDuration: unsuccessful: " + response.errorBody());
                    mCallback.onArtworkViewDurationError();
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel<Object>> call, Throwable t) {
                t.printStackTrace();
                mCallback.onArtworkViewDurationError();
            }
        });
    }
}
