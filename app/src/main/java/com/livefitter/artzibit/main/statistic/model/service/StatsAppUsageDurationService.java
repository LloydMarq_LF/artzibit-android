package com.livefitter.artzibit.main.statistic.model.service;

import com.livefitter.artzibit.main.statistic.model.request.AppUsageCountRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.AppUsageDurationRequestModel;

/**
 * Created by LloydM on 5/15/17
 * for Livefitter
 */

public interface StatsAppUsageDurationService {
    void logAppUsageDuration(String userEmail, String authToken, AppUsageDurationRequestModel body, Callback callback);

    public interface Callback {
        void onAppUsageDurationSuccess();
        void onAppUsageDurationError();
    }
}
