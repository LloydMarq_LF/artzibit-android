package com.livefitter.artzibit.main.browse_artwork.presenter;

import android.os.CountDownTimer;
import android.util.Log;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.browse_artwork.BrowseArtworkContract;
import com.livefitter.artzibit.main.browse_artwork.model.RetrieveArtworkService;
import com.livefitter.artzibit.main.browse_artwork.model.RetrieveArtworkServiceImpl;
import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkRequestModel;
import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkUpdateRequestModel;
import com.livefitter.artzibit.main.fave_artwork.model.service.FaveArtworkService;
import com.livefitter.artzibit.main.fave_artwork.model.service.FaveArtworkServiceImpl;
import com.livefitter.artzibit.main.statistic.ArtzibitLogger;
import com.livefitter.artzibit.main.statistic.model.service.StatsArtworkViewCountService;
import com.livefitter.artzibit.main.statistic.model.service.StatsArtworkViewDurationService;
import com.livefitter.artzibit.main.welcome.presenter.WelcomePresenterImpl;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.ArtworkViewDuration;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.CalendarHelper;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by LloydM on 6/7/17
 * for Livefitter
 */

public class BrowseArtworkPresenterImpl implements BrowseArtworkContract.Presenter, RetrieveArtworkService.RetrieveArtworkCallback, FaveArtworkService.FaveArtworkCallback {

    private static final String TAG = BrowseArtworkPresenterImpl.class.getSimpleName();
    private static final long ARTWORK_VIEW_MINIMUM_DURATION = 800;

    private String[] mCategorySpinnerItems = {"Fixed", "Custom"};
    private HashMap<String, Integer> mCategoryMap = new HashMap<>();

    private WeakReference<BrowseArtworkContract.View> mView;
    private RetrieveArtworkServiceImpl mRetrieveArtworkService;
    private FaveArtworkServiceImpl mFaveArtworkService;

    private UserModel mUserModel;
    //private int mChosenCategory;
    private ArrayList<ArtworkModel> mArtworkList;
    private CountDownTimer mArtworkViewTimer;
    /**
     * Internal flag that is triggered when the currently viewed artwork in {@link BrowseArtworkPresenterImpl#onArtworkViewStart(ArtworkModel) onArtworkViewStart() }
     * has fulfilled the minimum required display time to be considered it being a "view"
     */
    private boolean isCurrentArtworkBeingViewed;
    private String mCurrentArtworkViewStartTime;

    public BrowseArtworkPresenterImpl(BrowseArtworkContract.View view) {
        this.mView = new WeakReference<>(view);
        mRetrieveArtworkService = new RetrieveArtworkServiceImpl();
        mFaveArtworkService = new FaveArtworkServiceImpl();

        mCategoryMap.put(mCategorySpinnerItems[0], AppConstants.CATEGORY_FIXED);
        mCategoryMap.put(mCategorySpinnerItems[1], AppConstants.CATEGORY_CUSTOM);
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private BrowseArtworkContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public int getChosenCategory() {
        //return mChosenCategory;
        return 0;
    }

    @Override
    public void setChosenCategory(int chosenCategory) {
        //mChosenCategory = chosenCategory;
        getView().saveChosenCategoryPreference(chosenCategory);
    }

    @Override
    public void setChosenSpinnerCategory(int index) {
        if (index >= 0 && index < mCategorySpinnerItems.length) {
            int selectedCategory = mCategoryMap.get(mCategorySpinnerItems[index]);

            setChosenCategory(selectedCategory);
        }
    }

    @Override
    public void setUserModel(UserModel userModel) {
        mUserModel = userModel;
    }

    @Override
    public String[] getCategorySpinnerItems() {
        return mCategorySpinnerItems;
    }

    @Override
    public void retrieveArtworks(Map<String, String> queries) {

        getView().toggleBlockerLayout(true);
        getView().showArtworkRetrievalProgressDialog();

        // Safety check to currently outgoing requests
        mRetrieveArtworkService.cancel();

        mRetrieveArtworkService.retrieveArtwork(mUserModel.getEmail(),
                mUserModel.getAuthToken(),
                //mChosenCategory,
                queries,
                this);
    }

    @Override
    public void favoriteArtwork(ArtworkModel artworkModel) {
        Log.d(TAG, "retrieveFaveArtwork: artwork: " + artworkModel);
        if (artworkModel != null) {
            if (artworkModel.getFavoriteId() == 0 && !artworkModel.isFaved()) {
                Log.d(TAG, "favoriteArtwork: creating new favorite instance");
                // Artwork faved value is not yet created in the backend
                FaveArtworkRequestModel body = new FaveArtworkRequestModel(artworkModel.getId());
                mFaveArtworkService.faveArtwork(mUserModel.getEmail(),
                        mUserModel.getAuthToken(),
                        body,
                        this);
            } else {
                Log.d(TAG, "favoriteArtwork: updating old favorite instance");
                // Artwork faved value is already created in the backend, update it
                FaveArtworkUpdateRequestModel body = new FaveArtworkUpdateRequestModel(artworkModel.isFaved());
                mFaveArtworkService.updateFaveArtwork(mUserModel.getEmail(),
                        mUserModel.getAuthToken(),
                        artworkModel.getId(),
                        artworkModel.getFavoriteId(),
                        body,
                        this);
            }
        }
    }

    @Override
    public void addToCart(ArtworkModel artworkModel) {
        if (artworkModel != null) {
            getView().proceedToArtworkDetailScreen(artworkModel);
        }
    }

    @Override
    public void onRetrieveArtworkSuccess(ArrayList<ArtworkModel> artworkList) {
        mArtworkList = artworkList;
        getView().toggleBlockerLayout(false);
        getView().hideProgressDialog();
        getView().updateArtworkList(artworkList);
        // update the current artwork with the first artwork of the list
        getView().updateCurrentArtwork(artworkList.get(0));
    }

    @Override
    public void onRetrievalArtworkEmpty() {
        getView().hideProgressDialog();
        getView().showArtworkRetrievalEmptyErrorDialog();
    }

    @Override
    public void onRetrieveArtworkError() {
        getView().toggleBlockerLayout(false);
        getView().hideProgressDialog();
        getView().showArtworkRetrievalErrorDialog();
    }

    @Override
    public ArtworkModel getArtworkItem(int position) {
        return mArtworkList == null ? null : mArtworkList.get(position);
    }

    @Override
    public void onArtworkViewStart(final ArtworkModel viewedArtwork) {

        if (mArtworkViewTimer != null) {
            mArtworkViewTimer.cancel();
        }

        // Reset the current artwork fields
        isCurrentArtworkBeingViewed = false;
        mCurrentArtworkViewStartTime = "";

        mArtworkViewTimer = new CountDownTimer(ARTWORK_VIEW_MINIMUM_DURATION, ARTWORK_VIEW_MINIMUM_DURATION) {
            @Override
            public void onTick(long millisUntilFinished) {
                // ignore ticks
            }

            @Override
            public void onFinish() {

                if (mArtworkList.contains(viewedArtwork)) {
                    isCurrentArtworkBeingViewed = true;
                    mCurrentArtworkViewStartTime = CalendarHelper.getFormattedCurrentTime();

                    int index = mArtworkList.indexOf(viewedArtwork);

                    ArtworkModel artworkModel = mArtworkList.get(index);
                    int newViewCounter = viewedArtwork.getViewCounter() + 1;
                    artworkModel.setViewCounter(newViewCounter);

                    getView().updateArtworkList(artworkModel);
                }

            }
        };

        mArtworkViewTimer.start();
    }

    @Override
    public void onArtworkViewEnd(ArtworkModel viewedArtwork) {

        if (isCurrentArtworkBeingViewed && !mCurrentArtworkViewStartTime.isEmpty()) {

            if (mArtworkList.contains(viewedArtwork)) {

                int index = mArtworkList.indexOf(viewedArtwork);

                ArtworkModel artworkModel = mArtworkList.get(index);
                String currentArtworkViewEndTime = CalendarHelper.getFormattedCurrentTime();
                ArtworkViewDuration artworkViewDuration
                        = new ArtworkViewDuration(mCurrentArtworkViewStartTime, currentArtworkViewEndTime);

                if (artworkModel.getViews() == null) {
                    artworkModel.setViews(new ArrayList<ArtworkViewDuration>());
                }

                artworkModel.getViews().add(artworkViewDuration);

                getView().updateArtworkList(artworkModel);
            }
        }

        isCurrentArtworkBeingViewed = false;
        mCurrentArtworkViewStartTime = "";
    }

    @Override
    public void submitArtworkViewStatistics() {
        ArtzibitLogger.getInstance().viewArtworkCount(mUserModel, mArtworkList, new StatsArtworkViewCountService.Callback() {
            @Override
            public void onArtworkViewCountSuccess() {

                isCurrentArtworkBeingViewed = false;
                for (ArtworkModel artworkModel : mArtworkList) {
                    artworkModel.setViewCounter(0);
                }
            }

            @Override
            public void onArtworkViewCountError() {

            }
        });

        Log.d(TAG, "~~submitArtworkViewStatistics: mArtworkList: ");

        for (ArtworkModel artwork : mArtworkList){
            Log.d(TAG, "~~      " + artwork.getName() + ", views: " + artwork.getViews());
        }

        ArtzibitLogger.getInstance().viewArtworkDuration(mUserModel, mArtworkList, new StatsArtworkViewDurationService.Callback() {
            @Override
            public void onArtworkViewDurationSuccess() {
                mCurrentArtworkViewStartTime = "";
                isCurrentArtworkBeingViewed = false;
                for (ArtworkModel artworkModel : mArtworkList) {
                    if(artworkModel.getViews() != null){
                        artworkModel.getViews().clear();
                    }
                }
            }

            @Override
            public void onArtworkViewDurationError() {

            }
        });

        /*//reset the view counts and view durations
        for (ArtworkModel artworkModel : mArtworkList) {
            artworkModel.setViewCounter(0);
            if(artworkModel.getViews() != null){
                artworkModel.getViews().clear();
            }
        }
        //reset the view duration variables
        mCurrentArtworkViewStartTime = "";
        isCurrentArtworkBeingViewed = false;*/
    }

    @Override
    public void onFaveArtworkSuccess(int artworkId, boolean isDeleted, int artworkFavedId) {
        for (int i = 0; i < mArtworkList.size(); i++) {
            ArtworkModel artworkModel = mArtworkList.get(i);
            if (artworkModel.getId() == artworkId) {

                artworkModel.setFaved(!isDeleted);
                artworkModel.setFavoriteId(artworkFavedId);
                mArtworkList.set(i, artworkModel);

                getView().updateArtworkList(artworkModel);
            }
        }
    }

    @Override
    public void onFaveArtworkError(int artworkId) {
        getView().showArtworkFaveErrorMessage();
    }
}
