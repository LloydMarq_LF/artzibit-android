package com.livefitter.artzibit.main.browse_artwork.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.livefitter.artzibit.BaseFragment;
import com.livefitter.artzibit.view.CrossFragment;
import com.livefitter.artzibit.adapter.CrossViewPagerAdapter;

import java.util.ArrayList;


/**
 * Created by LloydM on 5/25/17
 * for Livefitter
 */

public class HomePagerAdapter extends CrossViewPagerAdapter {

    private ArrayList<BaseFragment> mFragmentList;


    public HomePagerAdapter(FragmentManager fm, ArrayList<BaseFragment> fragmentList) {
        super(fm);

        this.mFragmentList = fragmentList;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        BaseFragment fragment = (BaseFragment) object;
        return view == fragment.getView();
    }

    public void setOnChildVisible(int position){
        if(mFragmentList != null && !mFragmentList.isEmpty()){
            CrossFragment fragment = (CrossFragment) getItem(position);
            if(fragment != null){
                fragment.setOwnToolbar();
                fragment.onVisible();
            }
        }
    }
}
