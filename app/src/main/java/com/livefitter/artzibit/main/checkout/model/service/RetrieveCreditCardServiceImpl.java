package com.livefitter.artzibit.main.checkout.model.service;

import android.support.annotation.NonNull;
import android.util.Log;

import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public class RetrieveCreditCardServiceImpl implements RetrieveCreditCardService, Callback<BaseResponseModel<ArrayList<CreditCardModel>>> {

    private static final String TAG = RetrieveCreditCardServiceImpl.class.getSimpleName();
    private Callback mCallback;
    private Call<BaseResponseModel<ArrayList<CreditCardModel>>> mCall;

    @Override
    public void retrieveCreditCard(@NonNull String email, @NonNull String authToken, Callback callback) {

        this.mCallback = callback;

        if (mCall != null && mCall.isExecuted()) {
            mCall.cancel();
        }

        RetrieveCreditCardApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(RetrieveCreditCardApiMethods.class);

        this.mCall = apiMethods.retrieveCreditCards(email, authToken);

        mCall.enqueue(this);
    }

    @Override
    public void cancel() {
        if (mCall != null && mCall.isExecuted()) {
            mCall.cancel();
        }
    }

    @Override
    public void onResponse(Call<BaseResponseModel<ArrayList<CreditCardModel>>> call, Response<BaseResponseModel<ArrayList<CreditCardModel>>> response) {

        Log.d(TAG, "onResponse: " + response);

        if (response.isSuccessful()) {
            Log.d(TAG, "response success body: " + response.body().toString());
            if (response.body().getStatus()) {
                ArrayList<CreditCardModel> creditCardModels = response.body().getData();

                if (creditCardModels != null) {
                    mCallback.onRetrieveCreditCardSuccess(creditCardModels);
                } else {
                    Log.d(TAG, "shipping details null");
                    mCallback.onRetrieveCreditCardError();
                }
            } else {
                Log.d(TAG, "response unsuccessful");
                mCallback.onRetrieveCreditCardError();
            }
        } else {
            Log.d(TAG, "response failed body: " + response.errorBody());
            mCallback.onRetrieveCreditCardError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<ArrayList<CreditCardModel>>> call, Throwable t) {
        t.printStackTrace();
        mCallback.onRetrieveCreditCardError();
    }
}
