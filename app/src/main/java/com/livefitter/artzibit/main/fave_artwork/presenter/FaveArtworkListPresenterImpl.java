package com.livefitter.artzibit.main.fave_artwork.presenter;

import com.livefitter.artzibit.main.fave_artwork.FaveArtworkListContract;
import com.livefitter.artzibit.main.fave_artwork.model.service.RetrieveFaveArtworkService;
import com.livefitter.artzibit.main.fave_artwork.model.service.RetrieveFaveArtworkServiceImpl;
import com.livefitter.artzibit.model.FaveArtworkModel;
import com.livefitter.artzibit.model.UserModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class FaveArtworkListPresenterImpl implements FaveArtworkListContract.Presenter, RetrieveFaveArtworkService.Callback {

    private WeakReference<FaveArtworkListContract.View> mView;
    private RetrieveFaveArtworkService mService;

    private UserModel mUserModel;

    public FaveArtworkListPresenterImpl(FaveArtworkListContract.View view, UserModel userModel) {
        this.mView = new WeakReference<FaveArtworkListContract.View>(view);
        this.mUserModel = userModel;

        this.mService = new RetrieveFaveArtworkServiceImpl();
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private FaveArtworkListContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void retrieveFaveArtworks() {
        getView().showProgressBlocker();
        getView().showProgressIndicator();
        mService.retrieveFaveArtwork(mUserModel.getEmail(), mUserModel.getAuthToken(), this);
    }

    @Override
    public void onSuccess(ArrayList<FaveArtworkModel> artworkList) {
        getView().hideEmptyFavesError();
        getView().hideProgressBlocker();
        getView().hideProgressIndicator();
        getView().updateFaveArtworkList(artworkList);
    }

    @Override
    public void onNoFavedArtworks() {
        getView().hideProgressBlocker();
        getView().hideProgressIndicator();
        getView().showEmptyFavesError();
    }

    @Override
    public void onError() {
        getView().hideEmptyFavesError();
        getView().hideProgressBlocker();
        getView().hideProgressIndicator();
        getView().showErrorDialog();
    }

    @Override
    public void onFaveArtworkClick(FaveArtworkModel faveArtworkModel) {
        getView().proceedToArtworkDetails(faveArtworkModel);
    }
}
