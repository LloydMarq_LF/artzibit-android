package com.livefitter.artzibit.main.create_shipping_detail.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.TextView;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.checkout.view.CheckOutScreen;
import com.livefitter.artzibit.main.create_shipping_detail.CreateShippingDetailContract;
import com.livefitter.artzibit.main.create_shipping_detail.presenter.CreateShippingDetailPresenterImpl;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.util.FormValidityHelper;
import com.livefitter.artzibit.view.FormFieldTextWatcher;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateShippingDetailScreen extends BaseActivity implements CreateShippingDetailContract.View {

    private static final String TAG = CreateShippingDetailScreen.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.til_name)
    TextInputLayout tilFirstName;
    @BindView(R.id.et_name)
    TextInputEditText etFirstName;
    @BindView(R.id.til_last_name)
    TextInputLayout tilLastName;
    @BindView(R.id.et_last_name)
    TextInputEditText etLastName;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.til_phone_number)
    TextInputLayout tilPhoneNumber;
    @BindView(R.id.et_phone_number)
    TextInputEditText etPhoneNumber;
    @BindView(R.id.til_address)
    TextInputLayout tilAddress;
    @BindView(R.id.et_address)
    TextInputEditText etAddress;
    @BindView(R.id.til_city)
    TextInputLayout tilCity;
    @BindView(R.id.et_city)
    TextInputEditText etCity;
    @BindView(R.id.til_country)
    TextInputLayout tilCountry;
    @BindView(R.id.et_country)
    AutoCompleteTextView etCountry;
    @BindView(R.id.cb_set_as_default)
    CheckBox cbDefault;

    private CreateShippingDetailContract.Presenter mPresenter;
    private String[] arrCountries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_shipping_detail);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etFirstName.addTextChangedListener(new FormFieldTextWatcher(tilFirstName));
        etLastName.addTextChangedListener(new FormFieldTextWatcher(tilLastName));
        etEmail.addTextChangedListener(new FormFieldTextWatcher(tilEmail));
        etPhoneNumber.addTextChangedListener(new FormFieldTextWatcher(tilPhoneNumber));
        etAddress.addTextChangedListener(new FormFieldTextWatcher(tilAddress));
        etCity.addTextChangedListener(new FormFieldTextWatcher(tilCity));
        etCountry.addTextChangedListener(new FormFieldTextWatcher(tilCountry));
        etCountry.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSoftKeyboard(v);
                    return true;
                }
                return false;
            }
        });

        arrCountries = getResources().getStringArray(R.array.countries);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, arrCountries);

        etCountry.setAdapter(adapter);

        mPresenter = new CreateShippingDetailPresenterImpl(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_checkout_create_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_save:
                saveShippingDetails();
                return true;
            case android.R.id.home:{
                showExitPromptDialog(true);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showExitPromptDialog(false);
    }

    private void saveShippingDetails(){
        hideSoftKeyboard();
        if(areFieldsValid()){
            if(mPresenter != null){
                mPresenter.saveShippingDetail(etFirstName.getText().toString(),
                        etLastName.getText().toString(),
                        etEmail.getText().toString(),
                        etPhoneNumber.getText().toString(),
                        etAddress.getText().toString(),
                        etCity.getText().toString(),
                        etCountry.getText().toString(),
                        cbDefault.isChecked());
            }
        }
    }

    private boolean areFieldsValid(){
        boolean areFieldsValid = true;

        if(!FormValidityHelper.isFieldComplete(etFirstName)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilFirstName.setError(errorMessage);
        }

        if(!FormValidityHelper.isFieldComplete(etLastName)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilLastName.setError(errorMessage);
        }

        if(!FormValidityHelper.isFieldComplete(etEmail)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilEmail.setError(errorMessage);
        }
        else if(!FormValidityHelper.isEmailValid(etEmail)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_invalid_email);
            tilEmail.setError(errorMessage);
        }

        if(!FormValidityHelper.isFieldComplete(etPhoneNumber)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilPhoneNumber.setError(errorMessage);
        }
        /*else if(!FormValidityHelper.isPhoneNumberValid(etPhoneNumber)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_invalid_phone_number);
            tilPhoneNumber.setError(errorMessage);
        }*/

        if(!FormValidityHelper.isFieldComplete(etAddress)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilAddress.setError(errorMessage);
        }

        if(!FormValidityHelper.isFieldComplete(etCity)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilCity.setError(errorMessage);
        }

        if(tilCountry.getVisibility() == android.view.View.VISIBLE && !FormValidityHelper.isFieldComplete(etCountry)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilCountry.setError(errorMessage);
        }else if (tilCountry.getVisibility() == android.view.View.VISIBLE
                && (arrCountries!= null
                && !Arrays.asList(arrCountries).contains(etCountry.getText().toString()))){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_invalid_country);
            tilCountry.setError(errorMessage);
        }

        return areFieldsValid;
    }

    private void showExitPromptDialog(final boolean isGoingUp){
        String title = getString(R.string.label_unsaved_changes);
        String message = getString(R.string.msg_exit_with_unsaved);
        String ok = getString(R.string.label_ok);
        String cancel = getString(R.string.label_cancel);
        DialogUtil.showConfirmationDialog(this,
                title,
                message,
                ok,
                cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == DialogInterface.BUTTON_POSITIVE){
                            if(isGoingUp){
                                NavUtils.navigateUpFromSameTask(CreateShippingDetailScreen.this);
                            }else{
                                finish();
                            }
                        }
                    }
                });
    }

    @Override
    public void returnToCheckOutScreen(ShippingDetailModel shippingDetailModel) {
        Intent intent = new Intent(this, CheckOutScreen.class);
        intent.putExtra(AppConstants.BUNDLE_SHIPPING_DETAIL, shippingDetailModel);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
