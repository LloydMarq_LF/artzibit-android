package com.livefitter.artzibit.main.welcome.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.browse_artwork.view.HomeActivity;
import com.livefitter.artzibit.main.sign_in.view.SignInScreen;
import com.livefitter.artzibit.main.sign_up.view.SignUpScreen;
import com.livefitter.artzibit.main.welcome.WelcomeContract;
import com.livefitter.artzibit.main.welcome.presenter.WelcomePresenterImpl;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.util.SharedPrefsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeScreen extends BaseActivity implements WelcomeContract.View {

    private final static String TAG = WelcomeScreen.class.getSimpleName();

    WelcomePresenterImpl mPresenter;

    @BindView(R.id.btn_sign_in_facebook)
    Button btn_facebook;
    @BindView(R.id.btn_sign_in_email)
    Button btn_sign_in;
    @BindView(R.id.btn_sign_up)
    TextView btn_sign_up;
    private ProgressDialog pdLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);

        mPresenter = new WelcomePresenterImpl(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.fbCallbackResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.btn_sign_in_facebook)
    public void signInViaFacebook() {
        Log.d(TAG, "clicked signInViaFacebook");
        mPresenter.signInToFacebook();
    }

    @Override
    public void showProgressDialog() {

        if (pdLoading == null) {
            String progressMessage = getString(R.string.msg_progress_connecting_facebook);
            pdLoading = new ProgressDialog(this);
            pdLoading.setMessage(progressMessage);
            pdLoading.setCancelable(false);
        }

        pdLoading.show();
    }

    @Override
    public void hideProgressDialog() {
        if (pdLoading != null && pdLoading.isShowing()) {
            pdLoading.dismiss();
        }
    }

    @Override
    public void saveUserLogin(UserModel userModel) {
        SharedPrefsUtil.setUserAuth(this, userModel);
    }

    @Override
    public void showErrorDialog() {
        String message = getString(R.string.msg_error_facebook_auth);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public BaseActivity getViewActivity() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @OnClick(R.id.btn_sign_up)
    @Override
    public void proceedToSignUpScreen() {
        Intent intent = new Intent(this, SignUpScreen.class);
        switchActivity(intent, false);
    }

    @Override
    public void proceedToSignUpScreen(String facebookUid, String facebookName, String facebookEmail, String facebookPictureUrl) {
        Bundle extras = new Bundle();
        extras.putString(AppConstants.BUNDLE_FACEBOOK_UID, facebookUid);
        extras.putString(AppConstants.BUNDLE_FACEBOOK_NAME, facebookName);
        extras.putString(AppConstants.BUNDLE_FACEBOOK_EMAIL, facebookEmail);
        extras.putString(AppConstants.BUNDLE_FACEBOOK_PICTURE, facebookPictureUrl);

        Intent intent = new Intent(this, SignUpScreen.class);
        intent.putExtras(extras);
        switchActivity(intent, false);
    }


    @OnClick(R.id.btn_sign_in_email)
    @Override
    public void proceedToSignInScreen() {
        Intent intent = new Intent(this, SignInScreen.class);
        switchActivity(intent, false);
    }

    @Override
    public void proceedToHomeScreen(UserModel userModel) {
        Intent intent = new Intent(this, HomeActivity.class);
        //Intent intent = new Intent(this, CategorySelectScreen.class);
        intent.putExtra(AppConstants.BUNDLE_USER, userModel);
        switchActivity(intent, true);
    }
}
