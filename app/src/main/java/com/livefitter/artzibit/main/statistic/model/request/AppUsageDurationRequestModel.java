package com.livefitter.artzibit.main.statistic.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/19/17
 * for Livefitter
 */

public class AppUsageDurationRequestModel {

    /**
     * user_app_tracking_log : {"user_id":1,"start_time":"2017-06-30 10:00:00","end_time":"2017-06-30 10:01:00"}
     */

    @SerializedName("user_app_tracking_logs")
    private Payload userAppTrackingLog;

    public AppUsageDurationRequestModel(int userId, String startTime, String endTime) {
        ArrayList<UserAppTrackingLog> data = new ArrayList<>();
        UserAppTrackingLog userAppTrackingLogItem = new UserAppTrackingLog(userId, startTime, endTime);
        data.add(userAppTrackingLogItem);

        this.userAppTrackingLog = new Payload(data);
    }

    public class Payload{
        private ArrayList<UserAppTrackingLog> data;

        public Payload(ArrayList<UserAppTrackingLog> data) {
            this.data = data;
        }
    }

    public static class UserAppTrackingLog {
        /**
         * user_id : 1
         * start_time : 2017-06-30 10:00:00
         * end_time : 2017-06-30 10:01:00
         */

        @SerializedName("user_id")
        private int userId;
        @SerializedName("start_time")
        private String startTime;
        @SerializedName("end_time")
        private String endTime;

        public UserAppTrackingLog(int userId, String startTime, String endTime) {
            this.userId = userId;
            this.startTime = startTime;
            this.endTime = endTime;
        }
    }
}
