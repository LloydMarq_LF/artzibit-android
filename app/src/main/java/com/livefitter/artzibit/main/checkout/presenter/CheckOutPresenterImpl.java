package com.livefitter.artzibit.main.checkout.presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.checkout.CheckOutContract;
import com.livefitter.artzibit.main.checkout.model.request.CheckOutCartRequestModel;
import com.livefitter.artzibit.main.checkout.model.request.CheckOutRequestModel;
import com.livefitter.artzibit.main.checkout.model.service.CheckOutCartService;
import com.livefitter.artzibit.main.checkout.model.service.CheckOutCartServiceImpl;
import com.livefitter.artzibit.model.CartModel;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.model.UserModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by LloydM on 7/17/17
 * for Livefitter
 */

public class CheckOutPresenterImpl implements CheckOutContract.Presenter, CheckOutCartService.Callback {
    private static final String TAG = CheckOutPresenterImpl.class.getSimpleName();

    private WeakReference<CheckOutContract.View> mView;
    private CheckOutCartService mService;
    private BroadcastReceiver brCheckOutUpdatesReceiver;

    private final UserModel mUserModel;
    private final CartModel mCart;
    private ArrayList<ShippingDetailModel> mShippingDetails;
    private ArrayList<CreditCardModel> mCreditCards;

    public CheckOutPresenterImpl(CheckOutContract.View view, UserModel userModel, CartModel cart) {
        this.mView = new WeakReference<CheckOutContract.View>(view);
        this.mUserModel = userModel;
        this.mCart = cart;

        mService = new CheckOutCartServiceImpl();
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private CheckOutContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void registerBroadcastReceiver() {

        brCheckOutUpdatesReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Log.d(TAG, "onReceive: intent extras: " + intent.getExtras());
                onReceiveBroadcast(context, intent);
            }
        };
        LocalBroadcastManager.getInstance(getView().getViewActivity()).registerReceiver(
                brCheckOutUpdatesReceiver,
                new IntentFilter(AppConstants.INTENT_ACTION_CHECKOUT_PROCESS));

    }

    @Override
    public void unregisterBroadcastReceiver() {
        LocalBroadcastManager.getInstance(getView().getViewActivity()).unregisterReceiver(brCheckOutUpdatesReceiver);
    }

    @Override
    public ShippingDetailModel getSelectedShippingDetail() {
        if (mShippingDetails != null && !mShippingDetails.isEmpty()) {
            Log.d(TAG, "getSelectedShippingDetail: " + ShippingDetailModel.retrieveSelection(mShippingDetails));
            return ShippingDetailModel.retrieveSelection(mShippingDetails);
        }

        Log.d(TAG, "getSelectedShippingDetail: NULL");
        return null;
    }

    @Override
    public void onReceiveBroadcast(Context context, Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            if (extras.containsKey(AppConstants.BUNDLE_SHIPPING_DETAIL_LIST)) {
                mShippingDetails = extras.getParcelableArrayList(AppConstants.BUNDLE_SHIPPING_DETAIL_LIST);
                Log.d(TAG, "onReceiveBroadcast: shipping details update");
            }

            if (extras.containsKey(AppConstants.BUNDLE_CREDIT_CARD_LIST)) {
                mCreditCards = extras.getParcelableArrayList(AppConstants.BUNDLE_CREDIT_CARD_LIST);
                Log.d(TAG, "onReceiveBroadcast: credit cards update");
            }

            if (extras.containsKey(AppConstants.BUNDLE_CHECKOUT_NAVIGATION)) {
                int navigationIndex = extras.getInt(AppConstants.BUNDLE_CHECKOUT_NAVIGATION);

                // Hardcoded values to move between pages or initiate checkout procedure
                // Yes it sucks.

                if (navigationIndex == 0 || navigationIndex == 1) {
                    getView().navigateToPage(navigationIndex + 1);
                } else {
                    doCheckOut();
                }
            }
        }
    }

    private void doCheckOut() {
        Log.d(TAG, "doCheckOut: ");

        // Check if all necessary data is complete
        if (mUserModel == null
                && mCart == null
                && mShippingDetails == null || (mShippingDetails != null && mShippingDetails.isEmpty())
                && mCreditCards == null || (mCreditCards != null && mCreditCards.isEmpty())) {
            return;
        }

        getView().showCheckOutProgress();


        // Construct the cart section
        CheckOutCartRequestModel cartRequestModel;
        // Check if the selected shipping model is not new,
        // else, handle this in the "user_shipping_details" section
        ShippingDetailModel selectedShippingModel = ShippingDetailModel.retrieveSelection(mShippingDetails);
        if (!selectedShippingModel.isNew()) {
            cartRequestModel = new CheckOutCartRequestModel(true, selectedShippingModel.getId());
        } else {
            cartRequestModel = new CheckOutCartRequestModel(true);
        }

        CheckOutRequestModel checkOutRequestModel = new CheckOutRequestModel(cartRequestModel, mCreditCards, mShippingDetails);

        mService.checkOutCart(mUserModel.getEmail(),
                mUserModel.getAuthToken(),
                mCart.getId(),
                checkOutRequestModel,
                this);
    }

    @Override
    public void onCheckOutSuccess() {
        getView().hideCheckOutProgress();
        getView().proceedToReceiptScreen("");
    }

    @Override
    public void onCheckOutError() {
        getView().hideCheckOutProgress();
        getView().showCheckOutCartError();
    }
}
