package com.livefitter.artzibit.main.fave_artwork.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.livefitter.artzibit.listener.FaveArtworkClickListener;
import com.livefitter.artzibit.model.FaveArtworkModel;

import java.util.ArrayList;


/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class FaveArtworkListAdapter extends RecyclerView.Adapter<FaveArtworkListViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<FaveArtworkModel> mFaveArtworkList;
    private FaveArtworkClickListener mClickListener;

    public FaveArtworkListAdapter(Context context, FaveArtworkClickListener listener) {
        this.mContext = context;
        mClickListener = listener;

        mInflater = LayoutInflater.from(mContext);
    }

    public void setFaveArtworkList(ArrayList<FaveArtworkModel> faveArtworkList) {
        this.mFaveArtworkList = faveArtworkList;
        notifyDataSetChanged();
    }

    @Override
    public FaveArtworkListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return FaveArtworkListViewHolder.create(mInflater, parent);
    }

    @Override
    public void onBindViewHolder(FaveArtworkListViewHolder holder, int position) {
        FaveArtworkModel artwork = mFaveArtworkList.get(position);

        if(artwork != null){
            holder.bind(artwork, mClickListener);
        }
    }

    @Override
    public int getItemCount() {
        return mFaveArtworkList != null? mFaveArtworkList.size() : 0;
    }
}
