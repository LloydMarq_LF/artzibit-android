package com.livefitter.artzibit.main.artwork_detail.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.BaseResponseModel;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface RetrieveArtworkDetailsApiMethods {

    /**
     *
     * @param userEmail Used for authentication
     * @param authToken Used for authentication
     * @param artworkId ID of artwork to retrieve the full details for
     *
     * @return The Retrofit Call object to execute
     */
    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @GET(AppConstants.URL_ARTWORKS + "/{artworkId}")
    Call<BaseResponseModel<ArtworkModel>> retrieveArtworks(@NonNull @Header("X-User-Email") String userEmail,
                                                                      @NonNull @Header("X-User-Token") String authToken,
                                                                      @NonNull @Path("artworkId") int artworkId);
}