package com.livefitter.artzibit.main.browse_artwork.model;

import android.util.Log;

import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 6/7/17
 * for Livefitter
 */

public class RetrieveArtworkServiceImpl implements RetrieveArtworkService, Callback<BaseResponseModel<ArrayList<ArtworkModel>>> {

    private static final String TAG = RetrieveArtworkServiceImpl.class.getSimpleName();

    Call<BaseResponseModel<ArrayList<ArtworkModel>>> mCall;
    private RetrieveArtworkCallback mCallback;

    @Override
    public void cancel() {
        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }
    }

    @Override
    public void retrieveArtwork(String userEmail, String authToken, /*int chosenCategory,*/ Map<String, String> queries, RetrieveArtworkCallback callback) {
        mCallback = callback;

        RetrieveArtworkApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(RetrieveArtworkApiMethods.class);

        mCall = apiMethods.retrieveArtworks(userEmail, authToken, /*chosenCategory,*/ queries);
        mCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<BaseResponseModel<ArrayList<ArtworkModel>>> call, Response<BaseResponseModel<ArrayList<ArtworkModel>>> response) {
        Log.d(TAG, "onResponse: " + response);
        if(response.isSuccessful()){

            Log.d(TAG, "response success body: " + response.body().toString());

            if(response.body().getStatus()) {

                ArrayList<ArtworkModel> artworkModelArrayList = response.body().getData();

                if(artworkModelArrayList != null && !artworkModelArrayList.isEmpty()) {
                    mCallback.onRetrieveArtworkSuccess(artworkModelArrayList);
                }else{
                    mCallback.onRetrievalArtworkEmpty();
                }
            }else{
                mCallback.onRetrieveArtworkError();
            }
        }else{
            Log.d(TAG, "response failed body: "+ response.errorBody());
            mCallback.onRetrieveArtworkError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<ArrayList<ArtworkModel>>> call, Throwable t) {
        Log.d(TAG, "onFailure: ");
        t.printStackTrace();
        mCallback.onRetrieveArtworkError();
    }
}
