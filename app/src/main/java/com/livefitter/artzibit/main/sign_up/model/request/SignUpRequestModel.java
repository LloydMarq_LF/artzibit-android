package com.livefitter.artzibit.main.sign_up.model.request;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * Created by LloydM on 5/15/17
 * for Livefitter
 */

public class SignUpRequestModel {

    /**
     *  user{
         email: "example@email.com",
         name: "xxxx",
         username: "username",
         country: "country",
         password: "password",
         password_confirmation: "password"
         avatar: "https://xxxxx",
         user_authentication_attributes: {
             provider: "facebook",
             uid: "xxx"
         }
     }
     */

    private Payload user;

    public SignUpRequestModel(String name, String username, String email, String country, String password) {
        user = new Payload(name, username, email, country, password, null, null, null);
    }

    public SignUpRequestModel(String name, String username, String email, String country, @Nullable String password, @Nullable String profilePictureUrl, @Nullable String provider, @Nullable String uid) {
        user = new Payload(name, username, email, country, password, profilePictureUrl, provider, uid);
    }

    public Payload getUser() {
        return user;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .create();
        return gson.toJson(this);
    }

    private class Payload {
        private String name, username, email, country, password;
        @SerializedName("password_confirmation")
        private String passwordConfirmation;
        @SerializedName("avatar")
        private String profilePictureUrl;
        @SerializedName("user_authentication_attributes")
        private @Nullable UserAuthAttributesModel attributes;

        public Payload(String name, String username, String email, String country, String password) {
            this(name, username, email, country, password, null, null, null);
        }

        public Payload(String name, String username, String email, String country, @Nullable String password, @Nullable String profilePictureUrl, @Nullable String provider, @Nullable String uid) {
            this.name = name;
            this.username = username;
            this.email = email;
            this.country = country;
            this.password = password;
            this.passwordConfirmation = password;
            this.profilePictureUrl = profilePictureUrl;
            if(provider != null && uid != null){
                this.attributes = new UserAuthAttributesModel(provider, uid);
            }
        }

        public String getName() {
            return name;
        }

        public String getUsername() {
            return username;
        }

        public String getEmail() {
            return email;
        }

        public String getCountry() {
            return country;
        }

        public String getPassword() {
            return password;
        }

        public String getPasswordConfirmation() {
            return passwordConfirmation;
        }

        public String getProfilePictureUrl() {
            return profilePictureUrl;
        }

        public UserAuthAttributesModel getAttributes() {
            return attributes;
        }
    }
}
