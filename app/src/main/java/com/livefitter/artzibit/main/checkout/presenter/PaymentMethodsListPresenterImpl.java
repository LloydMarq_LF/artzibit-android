package com.livefitter.artzibit.main.checkout.presenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.checkout.PaymentMethodsListContract;
import com.livefitter.artzibit.main.checkout.model.service.RetrieveCreditCardService;
import com.livefitter.artzibit.main.checkout.model.service.RetrieveCreditCardServiceImpl;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.model.UserModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public class PaymentMethodsListPresenterImpl implements PaymentMethodsListContract.Presenter, RetrieveCreditCardService.Callback {

    private static final String TAG = PaymentMethodsListPresenterImpl.class.getSimpleName();
    private static final String BUNDLE_USER = "user_model";
    private static final String BUNDLE_CREDIT_CARD_LIST = "credit_cards";
    private static final String BUNDLE_CREDIT_CARD = "chosen_credit_card";

    private UserModel mUserModel;
    private ArrayList<CreditCardModel> mCreditCards;
    private CreditCardModel mChosenCreditCard;

    private WeakReference<PaymentMethodsListContract.View> mView;
    private RetrieveCreditCardService mService;

    public PaymentMethodsListPresenterImpl(UserModel userModel, PaymentMethodsListContract.View view) {
        this.mUserModel = userModel;
        this.mView = new WeakReference<PaymentMethodsListContract.View>(view);

        this.mService = new RetrieveCreditCardServiceImpl();

    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private PaymentMethodsListContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void retrieveCreditCards() {
        getView().showProgressIndicator();

        mService.retrieveCreditCard(mUserModel.getEmail(),
                mUserModel.getAuthToken(),
                this);
    }

    @Override
    public void onCreditCardSelected(CreditCardModel selectedCreditCard) {
        if (selectedCreditCard != null) {
            mChosenCreditCard = selectedCreditCard;

            for (CreditCardModel creditCardModel : mCreditCards) {
                creditCardModel.setSelected(creditCardModel.equals(selectedCreditCard));
            }

            getView().setProgressCondition(true);
            getView().setProceedButtonVisible(true);
            //broadcast this change
            broadcastToReceiver();
        }
    }

    @Override
    public void onFooterClick() {
        getView().proceedToCreatePaymentMethodScreen();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.REQUEST_CREATE_PAYMENT_METHOD && resultCode == Activity.RESULT_OK) {
            if (data != null && data.hasExtra(AppConstants.BUNDLE_CREDIT_CARD)) {
                final CreditCardModel newCreditCard = data.getParcelableExtra(AppConstants.BUNDLE_CREDIT_CARD);
                if (newCreditCard != null) {

                    if (mCreditCards != null && !mCreditCards.isEmpty()) {

                        // Go through the list of values
                        // and adjust their isDefault values
                        for (CreditCardModel creditCardModel : mCreditCards) {

                            if (newCreditCard.isDefault()) {
                                creditCardModel.setDefault(false);
                            }
                        }
                    } else {
                        if (mCreditCards == null) {
                            mCreditCards = new ArrayList<>();
                        }
                        //Automatically set that new card as the default
                        newCreditCard.setDefault(true);
                    }
                    newCreditCard.setSelected(true);
                    mCreditCards.add(newCreditCard);

                    // And set the newest card as the currently selected
                    onCreditCardSelected(newCreditCard);
                    getView().updateCreditCardList(mCreditCards);

                }
            }
        }
    }

    @Override
    public void saveInstanceState(Bundle outstate) {
        if (outstate != null) {
            outstate.putParcelable(BUNDLE_USER, mUserModel);
            outstate.putParcelableArrayList(BUNDLE_CREDIT_CARD_LIST, mCreditCards);
            outstate.putParcelable(BUNDLE_CREDIT_CARD, mChosenCreditCard);
        }
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        // Restore the existing data or fetch latest data from backend
        if (savedInstanceState != null) {
            mUserModel = savedInstanceState.getParcelable(BUNDLE_USER);
            mCreditCards = savedInstanceState.getParcelableArrayList(BUNDLE_CREDIT_CARD_LIST);
            mChosenCreditCard = savedInstanceState.getParcelable(BUNDLE_CREDIT_CARD);

            if (mUserModel != null && mCreditCards != null && mChosenCreditCard != null) {
                getView().updateCreditCardList(mCreditCards);
                getView().setProgressCondition(true);
                getView().setProceedButtonVisible(true);
            }
        } else {
            retrieveCreditCards();
        }

    }

    @Override
    public void navigateToNextStep() {
        Intent intent = new Intent(AppConstants.INTENT_ACTION_CHECKOUT_PROCESS);
        intent.putExtra(AppConstants.BUNDLE_CHECKOUT_NAVIGATION, 1);
        LocalBroadcastManager.getInstance(getView().getViewActivity()).sendBroadcast(intent);
    }

    @Override
    public void onRetrieveCreditCardSuccess(ArrayList<CreditCardModel> creditCardModels) {
        getView().hideProgressIndicator();
        getView().updateCreditCardList(creditCardModels);

        this.mCreditCards = creditCardModels;
        if (CreditCardModel.hasDefaultSelection(creditCardModels)) {
            onCreditCardSelected(CreditCardModel.retrieveDefaultSelection(creditCardModels));
        } else {
            getView().setProgressCondition(false);
            getView().setProceedButtonVisible(false);
        }
    }

    @Override
    public void onRetrieveCreditCardError() {
        getView().hideProgressIndicator();
        getView().setProgressCondition(false);
        getView().setProceedButtonVisible(false);
        getView().showRetrieveCreditCardError();
    }

    /**
     * Parses {@link #mCreditCards} and looks for credit cards that are {@link CreditCardModel#isSelected() selected}
     * or {@link CreditCardModel#isNew() new} and compiles them to be broadcast to a broadcast receiver in {@link CheckOutPresenterImpl}
     *
     */
    private void broadcastToReceiver() {

        if (mCreditCards != null && !mCreditCards.isEmpty() && mChosenCreditCard != null) {
            ArrayList<CreditCardModel> relevantcreditCardModels = new ArrayList<>();

            // look for new shipping details that were created just now
            for (CreditCardModel creditCardModel : mCreditCards) {
                if (creditCardModel.isNew() || creditCardModel.isSelected()) {
                    relevantcreditCardModels.add(creditCardModel);
                }
            }

            final Intent intent = new Intent(AppConstants.INTENT_ACTION_CHECKOUT_PROCESS);
            intent.putExtra(AppConstants.BUNDLE_CREDIT_CARD_LIST, relevantcreditCardModels);

            //broadcast this change, after a delay
            // Fix to avoid the bug that the broadcast not being sent after creation of a new card
            final Runnable r = new Runnable() {
                public void run() {
                    LocalBroadcastManager.getInstance(getView().getViewActivity()).sendBroadcast(intent);
                }
            };
            new Handler().postDelayed(r, 1000);

        }

    }
}
