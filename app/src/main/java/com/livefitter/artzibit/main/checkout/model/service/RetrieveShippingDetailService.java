package com.livefitter.artzibit.main.checkout.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.model.ShippingDetailModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public interface RetrieveShippingDetailService {

    void retrieveShippingDetail(@NonNull String email, @NonNull String authToken, Callback callback);
    void cancel();

    public interface Callback{
        void onRetrieveShippingDetailSuccess(ArrayList<ShippingDetailModel> shippingDetailModels);
        void onRetrieveShippingDetailError();
    }
}
