package com.livefitter.artzibit.main.browse_artwork.model;

import com.livefitter.artzibit.model.ArtworkModel;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by LloydM on 5/12/17
 * for Livefitter
 */

public interface RetrieveArtworkService {
    void cancel();
    void retrieveArtwork(String userEmail,
                         String authToken,
                         //int chosenCategory,
                         Map<String, String> queries,
                         RetrieveArtworkCallback callback);

    interface RetrieveArtworkCallback {
        void onRetrieveArtworkSuccess(ArrayList<ArtworkModel> artworkList);
        void onRetrievalArtworkEmpty();
        void onRetrieveArtworkError();
    }
}
