package com.livefitter.artzibit.main.statistic.model.service;

import android.util.Log;

import com.livefitter.artzibit.main.statistic.model.request.AppUsageCountRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by LloydM on 7/19/17
 * for Livefitter
 */

public class StatsAppUsageCountServiceImpl implements StatsAppUsageCountService {

    private static final String TAG = StatsAppUsageCountServiceImpl.class.getSimpleName();

    private Callback mCallback;
    private Call<BaseResponseModel<Object>> mCall;

    @Override
    public void logAppUsageCount(String userEmail, String authToken, AppUsageCountRequestModel body, Callback callback) {
        this.mCallback = callback;

        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }

        StatsAppUsageCountApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(StatsAppUsageCountApiMethods.class);

        this.mCall = apiMethods.logAppUsageCount(userEmail, authToken, body);

        mCall.enqueue(new retrofit2.Callback<BaseResponseModel<Object>>() {
            @Override
            public void onResponse(Call<BaseResponseModel<Object>> call, Response<BaseResponseModel<Object>> response) {
                Log.d(TAG, "onCountResponse: response: " + response);

                if(response.isSuccessful()){
                    Log.d(TAG, "onCountResponse: response body: " + response.body());
                    if(response.body().getStatus()){
                        mCallback.onAppUsageCountSuccess();
                    }else{
                        Log.d(TAG, "onCountResponse: failed: ");
                        mCallback.onAppUsageCountError();
                    }
                }else{
                    Log.d(TAG, "onCountResponse: unsuccessful: " + response.errorBody());
                    mCallback.onAppUsageCountError();
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel<Object>> call, Throwable t) {
                t.printStackTrace();
                mCallback.onAppUsageCountError();
            }
        });
    }
}
