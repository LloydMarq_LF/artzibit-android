package com.livefitter.artzibit.main.filter_artwork.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.R;
import com.livefitter.artzibit.model.ArtworkFilterModel;
import com.livefitter.artzibit.model.FilterOption;

import java.util.ArrayList;


/**
 * Created by LloydM on 6/15/17
 * for Livefitter
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterViewHolder> {

    private static final String TAG = FilterAdapter.class.getSimpleName();

    private Context mContext;
    private ArrayList<ArtworkFilterModel> mFilterList;
    private FilterClickListener mClickListener;
    private LayoutInflater mInflater;


    public FilterAdapter(Context context, FilterClickListener clickListener) {
        this.mContext = context;
        this.mClickListener = clickListener;

        mInflater = LayoutInflater.from(context);
    }

    public void setFilterList(ArrayList<ArtworkFilterModel> filterModelArrayList) {
        this.mFilterList = filterModelArrayList;
        notifyDataSetChanged();
    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return FilterViewHolder.create(mInflater, parent);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position) {
        final ArtworkFilterModel filterModel = mFilterList.get(position);

        if(filterModel != null){
            String chosenOption = mContext.getString(R.string.label_none);
            if(filterModel.getSelectedOptionId() != ArtworkFilterModel.NO_SELECTED_OPTION){

                for (int i = 0; i < filterModel.getOptions().size(); i++){
                    FilterOption filterOption = filterModel.getOptions().get(i);
                    if(filterOption.getId() == filterModel.getSelectedOptionId()){
                        chosenOption = filterOption.getName();
                    }
                }
            }

            holder.bind(filterModel.getName(), chosenOption, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onFilterClick(filterModel);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilterList != null? mFilterList.size() : 0;
    }

    public interface FilterClickListener{
        void onFilterClick(ArtworkFilterModel filter);
    }
}
