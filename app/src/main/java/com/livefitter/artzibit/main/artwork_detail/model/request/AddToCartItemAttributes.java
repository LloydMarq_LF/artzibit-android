package com.livefitter.artzibit.main.artwork_detail.model.request;

import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.livefitter.artzibit.gson.AddToCartItemAttributesSerializer;

/**
 * Created by LloydM on 7/3/17
 * for Livefitter
 */

public class AddToCartItemAttributes {
    /**
     * {
     * cart_id: 3,
     * artwork_id: 6,
     * fixed_size_id: 1,
     * fixed_size_price_id: 1,
     * installation: [true, false],
     * quantity: 1
     * }
     */

    @SerializedName("cart_id")
    private int cartId = -1;
    @SerializedName("artwork_id")
    private int artworkId;
    @SerializedName("fixed_size_id")
    private int fixedSizeId = -1;
    @SerializedName("fixed_size_price_id")
    private int fixedSizePriceId = -1;
    private float height = -1;
    private float width = -1;
    private boolean installation;
    private int quantity;

    public AddToCartItemAttributes(int artworkId, boolean installation, int quantity) {
        this.artworkId = artworkId;
        this.installation = installation;
        this.quantity = quantity;
    }

    public int getCartId() {
        return cartId;
    }

    public int getArtworkId() {
        return artworkId;
    }

    public int getFixedSizeId() {
        return fixedSizeId;
    }

    public int getFixedSizePriceId() {
        return fixedSizePriceId;
    }

    public float getHeight() {
        return height;
    }

    public float getWidth() {
        return width;
    }

    public boolean isInstallation() {
        return installation;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public void setFixedSizeId(int fixedSizeId) {
        this.fixedSizeId = fixedSizeId;
    }

    public void setFixedSizePriceId(int fixedSizePriceId) {
        this.fixedSizePriceId = fixedSizePriceId;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    @Override
    public String toString() {
        GsonBuilder gsonBuilder = new GsonBuilder()
                .registerTypeAdapter(AddToCartItemAttributes.class, new AddToCartItemAttributesSerializer());
        String string = gsonBuilder.create().toJson(this);

        return string;
    }
}
