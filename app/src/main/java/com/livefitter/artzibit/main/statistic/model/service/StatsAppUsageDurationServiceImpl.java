package com.livefitter.artzibit.main.statistic.model.service;

import android.util.Log;

import com.livefitter.artzibit.main.statistic.model.request.AppUsageDurationRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by LloydM on 7/19/17
 * for Livefitter
 */

public class StatsAppUsageDurationServiceImpl implements StatsAppUsageDurationService {

    private static final String TAG = StatsAppUsageDurationServiceImpl.class.getSimpleName();

    private Callback mCallback;
    private Call<BaseResponseModel<Object>> mCall;

    @Override
    public void logAppUsageDuration(String userEmail, String authToken, AppUsageDurationRequestModel body, Callback callback) {
        this.mCallback = callback;

        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }

        StatsAppUsageDurationApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(StatsAppUsageDurationApiMethods.class);

        this.mCall = apiMethods.logAppUsageDuration(userEmail, authToken, body);

        mCall.enqueue(new retrofit2.Callback<BaseResponseModel<Object>>() {
            @Override
            public void onResponse(Call<BaseResponseModel<Object>> call, Response<BaseResponseModel<Object>> response) {
                Log.d(TAG, "onDurationResponse: response: " + response);

                if(response.isSuccessful()){
                    Log.d(TAG, "onDurationResponse: response body: " + response.body());
                    if(response.body().getStatus()){
                        mCallback.onAppUsageDurationSuccess();
                    }else{
                        Log.d(TAG, "onDurationResponse: failed: ");
                        mCallback.onAppUsageDurationError();
                    }
                }else{
                    Log.d(TAG, "onDurationResponse: unsuccessful: " + response.errorBody());
                    mCallback.onAppUsageDurationError();
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel<Object>> call, Throwable t) {
                t.printStackTrace();
                mCallback.onAppUsageDurationError();
            }
        });
    }
}
