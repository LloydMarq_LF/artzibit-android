package com.livefitter.artzibit.main.statistic.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.sign_in.model.request.SignInRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.AppUsageCountRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.AppUsageDurationRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.UserModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface StatsAppUsageCountApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_APP_COUNTERS)
    Call<BaseResponseModel<Object>> logAppUsageCount(@NonNull @Header("X-User-Email") String userEmail,
                                                     @NonNull @Header("X-User-Token") String authToken,
                                                     @Body AppUsageCountRequestModel param);
}