package com.livefitter.artzibit.main.fave_artwork.model.request;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by LloydM on 6/19/17
 * for Livefitter
 */

public class FaveArtworkRequestModel {

    @SerializedName("user_favorite")
    private Payload userFavorite;

    public FaveArtworkRequestModel(int artworkId) {
        this.userFavorite = new Payload(artworkId);
    }

    public class Payload {
        @SerializedName("artwork_id")
        private int artworkId;

        public Payload(int artworkId) {
            this.artworkId = artworkId;
        }

        public int getArtworkId() {
            return artworkId;
        }

    }

    public Payload getUserFavorite() {
        return userFavorite;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
