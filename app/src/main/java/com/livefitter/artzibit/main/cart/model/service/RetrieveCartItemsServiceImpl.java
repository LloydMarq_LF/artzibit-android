package com.livefitter.artzibit.main.cart.model.service;

import android.support.annotation.NonNull;
import android.util.Log;

import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.CartModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 7/5/17
 * for Livefitter
 */

public class RetrieveCartItemsServiceImpl implements RetrieveCartItemsService, Callback<BaseResponseModel<CartModel>> {

    private static final String TAG = RetrieveCartItemsServiceImpl.class.getSimpleName();

    private RetrieveCartItemsService.Callback mCallback;
    private Call<BaseResponseModel<CartModel>> mCall;

    @Override
    public void retrieveCartItems(@NonNull String userEmail, @NonNull String authToken, Callback callback) {
        cancel();

        mCallback = callback;

        RetrieveCartItemsApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(RetrieveCartItemsApiMethods.class);

        mCall= apiMethods.retrieveCartItems(userEmail, authToken);

        mCall.enqueue(this);
    }

    @Override
    public void cancel() {
        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }
    }

    @Override
    public void onResponse(Call<BaseResponseModel<CartModel>> call, Response<BaseResponseModel<CartModel>> response) {
        Log.d(TAG, "onResponse: raw response " + response);

        if(response.isSuccessful()){
            Log.d(TAG, "onResponse: response body: " + response.body());
            if(response.body().getStatus()){
                CartModel cart = response.body().getData();

                if(cart != null){
                    if(cart.getItems() != null && !cart.getItems().isEmpty()) {
                        mCallback.onRetrieveCartItemSuccess(cart);
                    }else{
                        mCallback.onNoCartFound();
                    }
                }
            }else{
                // Cart is not yet created.
                mCallback.onNoCartFound();
            }
        }else{
            Log.d(TAG, "onResponse: unsuccessful - " + response.errorBody().toString());
            mCallback.onRetrieveCartItemError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<CartModel>> call, Throwable t) {
        t.printStackTrace();
        mCallback.onRetrieveCartItemError();
    }
}
