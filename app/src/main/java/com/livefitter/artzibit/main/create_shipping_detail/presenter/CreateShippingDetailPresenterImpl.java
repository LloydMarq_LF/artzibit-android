package com.livefitter.artzibit.main.create_shipping_detail.presenter;

import com.livefitter.artzibit.main.checkout.ShippingDetailsListContract;
import com.livefitter.artzibit.main.create_shipping_detail.CreateShippingDetailContract;
import com.livefitter.artzibit.model.ShippingDetailModel;

import java.lang.ref.WeakReference;


/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public class CreateShippingDetailPresenterImpl implements CreateShippingDetailContract.Presenter {

    WeakReference<CreateShippingDetailContract.View> mView;

    public CreateShippingDetailPresenterImpl(CreateShippingDetailContract.View view) {
        this.mView = new WeakReference<CreateShippingDetailContract.View>(view);
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private CreateShippingDetailContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void saveShippingDetail(String firstName, String lastName, String email, String phoneNumber,
                                   String address, String city, String country, boolean isDefault) {
        ShippingDetailModel shippingDetailModel = new ShippingDetailModel(firstName,
                lastName,  phoneNumber, address, city, country, isDefault, true);

        getView().returnToCheckOutScreen(shippingDetailModel);
    }
}
