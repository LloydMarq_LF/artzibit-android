package com.livefitter.artzibit.main.checkout.model.service;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.livefitter.artzibit.gson.CheckOutCartRequestSerializer;
import com.livefitter.artzibit.gson.CheckOutCreditCardListSerializer;
import com.livefitter.artzibit.gson.CheckOutShippingDetailListSerializer;
import com.livefitter.artzibit.main.checkout.model.request.CheckOutCartRequestModel;
import com.livefitter.artzibit.main.checkout.model.request.CheckOutRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 7/18/17
 * for Livefitter
 */

public class CheckOutCartServiceImpl implements CheckOutCartService, Callback<BaseResponseModel<LinkedTreeMap>> {

    private static final String TAG = CheckOutCartServiceImpl.class.getSimpleName();

    private Callback mCallback;
    private Call<BaseResponseModel<LinkedTreeMap>> mCall;

    @Override
    public void checkOutCart(@NonNull String userEmail, @NonNull String authToken, int cartId,
                             @NonNull CheckOutRequestModel body, Callback callback) {
        this.mCallback = callback;

        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }

        Type creditCardListType = new TypeToken<ArrayList<CreditCardModel>>() {}.getType();
        Type shippingDetailListType = new TypeToken<ArrayList<ShippingDetailModel>>() {}.getType();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(CheckOutCartRequestModel.class, new CheckOutCartRequestSerializer())
                .registerTypeAdapter(creditCardListType, new CheckOutCreditCardListSerializer())
                .registerTypeAdapter(shippingDetailListType, new CheckOutShippingDetailListSerializer())
                .create();

        CheckOutCartApiMethods apiMethods = RetrofitUtil.getGSONRetrofit(gson).create(CheckOutCartApiMethods.class);

        this.mCall = apiMethods.checkOutCart(userEmail, authToken, cartId, body);

        mCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<BaseResponseModel<LinkedTreeMap>> call, Response<BaseResponseModel<LinkedTreeMap>> response) {

        Log.d(TAG, "onResponse: " + response);

        if (response.isSuccessful()) {
            Log.d(TAG, "response success body: " + response.body().toString());
            if (response.body().getStatus()) {
                mCallback.onCheckOutSuccess();
            } else {
                Log.d(TAG, "response unsuccessful");
                mCallback.onCheckOutError();
            }
        } else {
            Log.d(TAG, "response failed body: " + response.errorBody());
            mCallback.onCheckOutError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<LinkedTreeMap>> call, Throwable t) {
        t.printStackTrace();
        mCallback.onCheckOutError();
    }
}
