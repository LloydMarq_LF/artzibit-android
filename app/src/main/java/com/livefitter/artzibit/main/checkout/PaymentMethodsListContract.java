package com.livefitter.artzibit.main.checkout;

import android.content.Intent;
import android.os.Bundle;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.model.ShippingDetailModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public interface PaymentMethodsListContract {
    public interface Presenter{
        void retrieveCreditCards();
        void onCreditCardSelected(CreditCardModel creditCardModel);
        void onFooterClick();
        void onActivityResult(int requestCode, int resultCode, Intent data);
        void saveInstanceState(Bundle outstate);
        void onViewCreated(Bundle savedInstanceState);
        void navigateToNextStep();
    }

    public interface View extends BaseContractView{
        void updateCreditCardList(ArrayList<CreditCardModel> creditCards);
        void proceedToCreatePaymentMethodScreen();
        void setProgressCondition(boolean canProgress);
        void setProceedButtonVisible(boolean isVisible);
        void showRetrieveCreditCardError();
        void showProgressIndicator();
        void hideProgressIndicator();
    }
}
