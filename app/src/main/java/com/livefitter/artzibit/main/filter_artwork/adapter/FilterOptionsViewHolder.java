package com.livefitter.artzibit.main.filter_artwork.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.databinding.ItemFilterSelectableBinding;
import com.livefitter.artzibit.model.FilterOption;


/**
 * Created by LloydM on 6/15/17
 * for Livefitter
 */

public class FilterOptionsViewHolder extends RecyclerView.ViewHolder {

    FilterOption mFilterOption;
    ItemFilterSelectableBinding mBinding;
    OnFilterOptionSelectedListener mListener;

    public static FilterOptionsViewHolder create(LayoutInflater inflater, ViewGroup parent, OnFilterOptionSelectedListener listener) {
        ItemFilterSelectableBinding binding = ItemFilterSelectableBinding.inflate(inflater, parent, false);

        return new FilterOptionsViewHolder(binding, listener);
    }

    private FilterOptionsViewHolder(ItemFilterSelectableBinding binding, OnFilterOptionSelectedListener listener) {
        super(binding.getRoot());
        mBinding = binding;
        mListener = listener;

        mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSelected();
                mListener.onFilterOptionSelected(mFilterOption);
            }
        });
    }

    private void toggleSelected() {
        if (mFilterOption != null) {
            mFilterOption.setSelected(!mFilterOption.isSelected());
        }
    }

    public void bind(FilterOption option) {
        mFilterOption = option;
        mBinding.setOption(mFilterOption);
        mBinding.executePendingBindings();
    }

    public interface OnFilterOptionSelectedListener {
        void onFilterOptionSelected(FilterOption selectedFilterOption);
    }
}
