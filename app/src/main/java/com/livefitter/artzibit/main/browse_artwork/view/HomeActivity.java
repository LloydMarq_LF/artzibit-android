package com.livefitter.artzibit.main.browse_artwork.view;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.BaseFragment;
import com.livefitter.artzibit.main.cart.view.CartScreen;
import com.livefitter.artzibit.main.fave_artwork.view.FaveArtworkListScreen;
import com.livefitter.artzibit.main.statistic.ArtzibitLogger;
import com.livefitter.artzibit.view.CrossFragment;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.adapter.CrossViewPagerAdapter;
import com.livefitter.artzibit.main.browse_artwork.adapter.HomePagerAdapter;
import com.livefitter.artzibit.view.CrossContainer;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements CrossContainer.OnVerticalChildChangeListener, ViewPager.OnPageChangeListener /*implements CrossContainer.CrossCallback*/ {

    private static final String TAG = HomeActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.home_layout)
    CrossContainer ccHome;

    @BindView(R.id.viewpager)
    ViewPager vpHome;

    CrossFragment fragAccount;

    CrossFragment fragFilter;

    HomePagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        FaveArtworkListScreen fragFave = new FaveArtworkListScreen();
        CrossFragment fragHome = new BrowseArtworkScreen();
        CartScreen fragCart = new CartScreen();

        Bundle args = new Bundle();
        args.putString("title", "Cart");
        fragCart.setArguments(args);

        ArrayList<BaseFragment> fragmentList = new ArrayList<>();
        fragmentList.add(fragFave);
        fragmentList.add(fragHome);
        fragmentList.add(fragCart);

        //initialize CrossViewPagerAdapter，and add child view to UltraViewPager
        mAdapter = new HomePagerAdapter(getSupportFragmentManager(), fragmentList);
        vpHome.setAdapter(mAdapter);
        if(mAdapter.getCenterIndex() != CrossViewPagerAdapter.NO_CENTER){
            vpHome.setCurrentItem(mAdapter.getCenterIndex());
        }

        fragAccount = (CrossFragment) getSupportFragmentManager().findFragmentById(R.id.frag_account);
        fragFilter = (CrossFragment) getSupportFragmentManager().findFragmentById(R.id.frag_filter);

        ccHome.setChildAbove(fragAccount);
        ccHome.setChildBelow(fragFilter);
        ccHome.setViewPager(vpHome);
        // Set the CrossContainer callbacks
        ccHome.setOnChildChangeListener(this);
        ccHome.setViewPagerPageChangeListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(vpHome.getCurrentItem() == 1) {
                    vpHome.setCurrentItem(0);
                }else if(vpHome.getCurrentItem() == 2) {
                    vpHome.setCurrentItem(1);
                }
                return true;
            case R.id.action_cart:
                vpHome.setCurrentItem(2);
                return true;
            case R.id.action_return_to_browse_artwork:
                vpHome.setCurrentItem(1);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onVerticalChildChange(int childIndex) {
        switch (childIndex){
            case CrossContainer.ACTIVE_CHILD_ABOVE:
                fragAccount.setOwnToolbar();
                break;
            case CrossContainer.ACTIVE_CHILD_BELOW:
                fragFilter.setOwnToolbar();
                fragFilter.onVisible();
                break;
            case CrossContainer.ACTIVE_CHILD_NONE:
                int position = vpHome.getCurrentItem();
                mAdapter.setOnChildVisible(position);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
        String stateMessage = "";
        int position = vpHome.getCurrentItem();
        switch (state){
            case ViewPager.SCROLL_STATE_DRAGGING:
                break;
            case ViewPager.SCROLL_STATE_IDLE:
                mAdapter.setOnChildVisible(position);
                break;
            case ViewPager.SCROLL_STATE_SETTLING:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if(mAdapter.getCenterIndex() != CrossViewPagerAdapter.NO_CENTER
                && vpHome.getCurrentItem() != mAdapter.getCenterIndex()){
            vpHome.setCurrentItem(mAdapter.getCenterIndex(), true);
        } else if(ccHome.getActiveChildIndex() != CrossContainer.ACTIVE_CHILD_NONE){
            ccHome.toggleVerticalChildVisibility(ccHome.getActiveChildIndex(), false);
        }else {
            super.onBackPressed();
        }
    }

}

