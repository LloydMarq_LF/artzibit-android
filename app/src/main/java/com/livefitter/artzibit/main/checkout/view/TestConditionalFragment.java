package com.livefitter.artzibit.main.checkout.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.livefitter.artzibit.R;
import com.livefitter.artzibit.view.ConditionalPagerFragment;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LloydM on 7/11/17
 * for Livefitter
 */

public class TestConditionalFragment extends ConditionalPagerFragment {

    private static final String TAG = TestConditionalFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String BUNDLE_SECTION_NUMBER = "section number";

    private int sectionNumber;


    @BindView(R.id.switch_widget)
    SwitchCompat switchCompat;

    public static TestConditionalFragment newInstance(int sectionNumber) {
        TestConditionalFragment fragment = new TestConditionalFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if(getArguments() != null) {
            sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
        }

        Log.d(TAG, "onCreate: " + sectionNumber);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_test_conditional, container, false);

        ButterKnife.bind(this, rootView);

        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        if(sectionNumber > 0) {
            textView.setText(getString(R.string.section_format, sectionNumber));
        }

        switchCompat.setChecked(canProgress());
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setCanProgress(isChecked, true);
            }
        });

        return rootView;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewReady: " + sectionNumber);
        if(savedInstanceState != null && savedInstanceState.containsKey(BUNDLE_SECTION_NUMBER)){
            sectionNumber = savedInstanceState.getInt(BUNDLE_SECTION_NUMBER);

            Log.d(TAG, "      restored instance state: " + sectionNumber);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
