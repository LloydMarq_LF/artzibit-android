package com.livefitter.artzibit.main.checkout.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.checkout.ShippingDetailsListContract;
import com.livefitter.artzibit.main.checkout.adapter.ShippingListAdapter;
import com.livefitter.artzibit.main.checkout.presenter.ShippingDetailsListPresenterImpl;
import com.livefitter.artzibit.main.create_shipping_detail.view.CreateShippingDetailScreen;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.view.ConditionalPagerFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by LloydM on 7/13/17
 * for Livefitter
 */

public class ShippingDetailsListScreen extends ConditionalPagerFragment implements ShippingListAdapter.ShippingAddressListCallback, ShippingDetailsListContract.View {

    private static final String TAG = ShippingDetailsListScreen.class.getSimpleName();

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout srLayout;
    @BindView(R.id.recyclerview)
    RecyclerView rvShippingDetails;
    @BindView(R.id.checkout_item_layout_proceed)
    FrameLayout flProceedLayout;

    ShippingDetailsListContract.Presenter mPresenter;
    ShippingListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getActivity().getIntent().getExtras();

        if (extras != null && extras.containsKey(AppConstants.BUNDLE_USER)) {
            UserModel userModel = ((UserModel) extras.getParcelable(AppConstants.BUNDLE_USER));

            if (userModel != null) {
                mPresenter = new ShippingDetailsListPresenterImpl(userModel, this);
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_checkout_content_list, container, false);

        ButterKnife.bind(this, rootView);

        srLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(mPresenter != null && mAdapter != null){
                    mPresenter.retrieveShippingDetails();
                    mAdapter.setShippingAddresses(null);
                }
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvShippingDetails.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(rvShippingDetails.getContext(), layoutManager.getOrientation());
        rvShippingDetails.addItemDecoration(itemDecoration);

        String footerLabel = getString(R.string.label_add_shipping_address);
        mAdapter = new ShippingListAdapter(getActivity(), footerLabel, this);
        rvShippingDetails.setAdapter(mAdapter);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mPresenter != null){
            mPresenter.onViewCreated(savedInstanceState);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mPresenter != null){
            mPresenter.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mPresenter != null){
            mPresenter.saveInstanceState(outState);
        }
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @OnClick(R.id.checkout_item_button_proceed)
    public void proceedButtonClick(){
        if(mPresenter != null){
            mPresenter.navigateToNextStep();
        }
    }

    @Override
    public void onShippingAddressSelected(ShippingDetailModel shippingAddress) {
        if(mPresenter != null){
            mPresenter.onShippingDetailSelected(shippingAddress);
        }
    }

    @Override
    public void onFooterClick() {
        if(mPresenter != null){
            mPresenter.onFooterClick();
        }
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void updateShippingList(ArrayList<ShippingDetailModel> shippingDetailModels) {
        if(mAdapter != null){
            mAdapter.setShippingAddresses(shippingDetailModels);
        }
    }

    @Override
    public void proceedToCreateShippingDetailsScreen() {
        Intent intent = new Intent(getActivity(), CreateShippingDetailScreen.class);
        startActivityForResult(intent, AppConstants.REQUEST_CREATE_SHIPPING_DETAIL);
    }

    @Override
    public void setProgressCondition(boolean canProgress) {
        setCanProgress(canProgress, true);
    }

    @Override
    public void setProceedButtonVisible(boolean isVisible) {
        if(flProceedLayout != null){
            int visibility = isVisible? View.VISIBLE : View.GONE;
            flProceedLayout.setVisibility(visibility);
        }
    }

    @Override
    public void showForeignAddressWarning() {
        String message = getString(R.string.msg_foreign_shipping_address);
        DialogUtil.showAlertDialog(getActivity(), message);
    }

    @Override
    public void showRetrieveShippingDetailError() {
        srLayout.setEnabled(true);
        Toast.makeText(getActivity(), R.string.msg_error_retrieve_shipping_detail_failed, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgressIndicator() {

        if(srLayout != null){
            srLayout.setEnabled(true);
            srLayout.setRefreshing(true);
        }
    }

    @Override
    public void hideProgressIndicator() {
        if(srLayout != null){

            srLayout.setRefreshing(false);
            srLayout.setEnabled(false);
        }
    }
}
