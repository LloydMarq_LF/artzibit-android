package com.livefitter.artzibit.main.sign_in.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.browse_artwork.view.HomeActivity;
import com.livefitter.artzibit.main.sign_in.SignInContract;
import com.livefitter.artzibit.main.sign_in.presenter.SignInPresenterImpl;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.view.FormFieldTextWatcher;
import com.livefitter.artzibit.util.FormValidityHelper;
import com.livefitter.artzibit.util.SharedPrefsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInScreen extends BaseActivity implements SignInContract.View {

    SignInPresenterImpl mPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.et_password)
    TextInputEditText etPassword;
    @BindView(R.id.btn_sign_in_email)
    Button btnSignIn;

    private ProgressDialog pdLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        mPresenter = new SignInPresenterImpl(this);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set up a text watcher for errors
        etEmail.addTextChangedListener(new FormFieldTextWatcher(tilEmail));
        etPassword.addTextChangedListener(new FormFieldTextWatcher(tilPassword));

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    signIn();
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.btn_sign_in_email)
    public void signIn(){
        hideSoftKeyboard();
        if(areFieldsValid()){
            String email = etEmail.getText().toString();
            String password = etPassword.getText().toString();
            mPresenter.doSignIn(email, password);
        }
    }

    private boolean areFieldsValid() {
        boolean areFieldsValid = true;

        if(!FormValidityHelper.isFieldComplete(etEmail)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilEmail.setError(errorMessage);
        }
        else if(!FormValidityHelper.isEmailValid(etEmail)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_invalid_email);
            tilEmail.setError(errorMessage);
        }

        if(!FormValidityHelper.isFieldComplete(etPassword)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilPassword.setError(errorMessage);
        }

        return areFieldsValid;
    }

    @Override
    public void proceedToHomeScreen(UserModel userModel) {
        Intent intent = new Intent(this, HomeActivity.class);
        //Intent intent = new Intent(this, CategorySelectScreen.class);
        intent.putExtra(AppConstants.BUNDLE_USER, userModel);
        switchActivity(intent, true);
    }

    @Override
    public void showProgressDialog() {
        if(pdLoading == null){
            String progressMessage = getString(R.string.msg_progress_sign_in);
            pdLoading = new ProgressDialog(this);
            pdLoading.setMessage(progressMessage);
            pdLoading.setCancelable(false);
        }

        pdLoading.show();
    }

    @Override
    public void hideProgressDialog() {
        if(pdLoading != null && pdLoading.isShowing()){
            pdLoading.dismiss();
        }
    }

    @Override
    public void saveUserLogin(UserModel userModel) {
        SharedPrefsUtil.setUserAuth(this, userModel);
    }

    @Override
    public void showErrorDialog() {
        String message = getString(R.string.msg_error_sign_in);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void showInvalidSignInDialog() {
        String message = getString(R.string.msg_error_sign_in_invalid);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void clearPasswordField() {
        etPassword.setText(null);
        etPassword.requestFocus();
    }

    @Override
    public BaseActivity getViewActivity() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }
}
