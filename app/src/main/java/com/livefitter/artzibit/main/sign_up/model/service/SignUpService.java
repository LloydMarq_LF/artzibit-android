package com.livefitter.artzibit.main.sign_up.model.service;

import com.livefitter.artzibit.main.sign_up.model.request.SignUpRequestModel;
import com.livefitter.artzibit.model.UserModel;

/**
 * Created by LloydM on 5/15/17
 * for Livefitter
 */

public interface SignUpService {
    void doSignUp(SignUpRequestModel params, final SignUpCallback callback);

    public interface SignUpCallback {
        void onSuccess(UserModel userModel);
        void onAccountAlreadyTakenError();
        void onError();
    }
}
