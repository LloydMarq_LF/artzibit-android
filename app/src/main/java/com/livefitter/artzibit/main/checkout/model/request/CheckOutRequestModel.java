package com.livefitter.artzibit.main.checkout.model.request;

import com.google.gson.annotations.SerializedName;
import com.livefitter.artzibit.gson.CheckOutCreditCardListSerializer;
import com.livefitter.artzibit.gson.CheckOutShippingDetailListSerializer;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.model.ShippingDetailModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/18/17
 * for Livefitter
 */

public class CheckOutRequestModel {
    /**
     *
     * {
         cart: {},
         tokens: [{ }],
         user_shipping_details: [{}]
     }
     */

    /**
     * {@link CheckOutCartRequestModel} being serialized using {@link com.livefitter.artzibit.gson.CheckOutCartRequestSerializer CheckOutCartRequestSerializer}
     */
    private CheckOutCartRequestModel cart;
    /**
     * {@link CreditCardModel CreditCardModels} being serialized using {@link CheckOutCreditCardListSerializer CheckOutCreditCardListSerializer}
     */
    private ArrayList<CreditCardModel> tokens;
    /**
     * {@link ShippingDetailModel ShippingDetailModels} being serialized using {@link CheckOutShippingDetailListSerializer CheckOutShippingDetailListSerializer}
     */
    @SerializedName("user_shipping_details")
    private ArrayList<ShippingDetailModel> shippingDetails;

    public CheckOutRequestModel(CheckOutCartRequestModel cart, ArrayList<CreditCardModel> tokens, ArrayList<ShippingDetailModel> shippingDetails) {
        this.cart = cart;
        this.tokens = tokens;
        this.shippingDetails = shippingDetails;
    }

    public CheckOutCartRequestModel getCart() {
        return cart;
    }

    public ArrayList<CreditCardModel> getTokens() {
        return tokens;
    }

    public ArrayList<ShippingDetailModel> getShippingDetails() {
        return shippingDetails;
    }
}
