package com.livefitter.artzibit.main.checkout.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.livefitter.artzibit.view.ConditionalPagerFragment;

import java.util.ArrayList;


/**
 * Created by LloydM on 7/12/17
 * for Livefitter
 */

public class CheckOutPagerAdapter extends FragmentStatePagerAdapter implements ConditionalPagerFragment.OnConditionChangeListener {

    private static final String TAG = CheckOutPagerAdapter.class.getSimpleName();
    private static final int MAX_PAGES = 3;

    private FragmentManager mFragmentManager;
    /**
     * Holds all the fragments for usage, but visibility is dictated by the size of mIndexList
     */
    private ArrayList<ConditionalPagerFragment> mFragmentList;
    /**
     * Mirrored list that holds the count for the active fragments in the dataset
     */
    private ArrayList<Integer> mIndexList = new ArrayList<>();
    private OnPageConditionChangeListener mListener;

    public interface OnPageConditionChangeListener{
        /**
         * Called when the adapter notices that one of the pages has changed their conditions
         * @param position of page in the dataset
         * @param canProgress flag that determines if the pages after the affected page should be enabled or not.
         */
        void onPageConditionChange(int position, boolean canProgress);
    }

    public CheckOutPagerAdapter(@NonNull FragmentManager fm,@NonNull ArrayList<ConditionalPagerFragment> fragments, OnPageConditionChangeListener listener) {
        super(fm);
        this.mFragmentManager = fm;
        this.mFragmentList = fragments;
        this.mListener = listener;

        initialize();
    }

    private void initialize(){

        if(mFragmentList != null && !mFragmentList.isEmpty()){
            // first page
            mIndexList.add(0);

            for(int i = 0; i < mFragmentList.size(); i++){
                ConditionalPagerFragment fragment = mFragmentList.get(i);
                fragment.setConditionChangeListener(this);

                // Determine which fragments enabled for navigation
                if(fragment.canProgress() && (i + 1) < mFragmentList.size()){

                    if(!mIndexList.contains(i + 1)) {
                        mIndexList.add(i + 1);
                    }
                }
            }
        }
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getItemPosition(Object object) {

        if (object instanceof ConditionalPagerFragment) {
            int indexOfFragment = mFragmentList.indexOf(object);

            if (indexOfFragment != -1) {
                if (indexOfFragment < mIndexList.size()) {
                    return indexOfFragment;
                } else {
                    return POSITION_NONE;
                }
            } else {
                return POSITION_NONE;
            }
        }

        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mIndexList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public void onConditionChanged(ConditionalPagerFragment fragment, boolean canProgress) {
        int indexOfFragment = mFragmentList.indexOf(fragment);
        if (indexOfFragment != -1) {
            if (canProgress && mIndexList.size() < MAX_PAGES) {
                // Add a new page
                mIndexList.add(indexOfFragment + 1);

                notifyDataSetChanged();
            } else if (!canProgress) {
                // Remove pages from current page until end of fragment list
                mIndexList.subList(indexOfFragment + 1, mIndexList.size()).clear();

                notifyDataSetChanged();
            }

            if(mListener != null){
                // Call the listener of this valid condition change
                mListener.onPageConditionChange(indexOfFragment, canProgress);
            }

        }
    }
}
