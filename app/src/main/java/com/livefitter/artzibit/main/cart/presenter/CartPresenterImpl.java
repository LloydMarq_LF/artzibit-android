package com.livefitter.artzibit.main.cart.presenter;

import android.util.Log;

import com.livefitter.artzibit.main.cart.CartComputationManager;
import com.livefitter.artzibit.main.cart.CartContract;
import com.livefitter.artzibit.model.CartItemAttributes;
import com.livefitter.artzibit.main.cart.model.request.CartRequestModel;
import com.livefitter.artzibit.main.cart.model.service.RetrieveCartItemsService;
import com.livefitter.artzibit.main.cart.model.service.RetrieveCartItemsServiceImpl;
import com.livefitter.artzibit.main.cart.model.service.UpdateCartService;
import com.livefitter.artzibit.main.cart.model.service.UpdateCartServiceImpl;
import com.livefitter.artzibit.model.CartItemModel;
import com.livefitter.artzibit.model.CartModel;
import com.livefitter.artzibit.model.UserModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by LloydM on 7/6/17
 * for Livefitter
 */

public class CartPresenterImpl implements CartContract.Presenter, RetrieveCartItemsService.Callback, CartComputationManager.CartComputationCallback, UpdateCartService.Callback {

    private static final String TAG = CartPresenterImpl.class.getSimpleName();

    private RetrieveCartItemsService mRetrieveCartItemsService;
    private UpdateCartService mUpdateCartService;
    private WeakReference<CartContract.View> mView;
    private CartComputationManager mCartComputationManager;

    private UserModel mUserModel;
    private CartModel mCart;
    private String mCurrencySymbol;
    private ArrayList<CartItemModel> mRemovedCartItems = new ArrayList<>();
    private boolean isCartCheckout = false;
    private boolean hasUnsavedChanges = false;

    public CartPresenterImpl(UserModel userModel, CartContract.View view) {
        this.mUserModel = userModel;
        this.mView = new WeakReference<CartContract.View>(view);
        mCartComputationManager = new CartComputationManager(this);

        mRetrieveCartItemsService = new RetrieveCartItemsServiceImpl();
        mUpdateCartService = new UpdateCartServiceImpl();
    }

    @Override
    public void setCurrencySymbol(String currencySymbol) {
        this.mCurrencySymbol = currencySymbol;
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private CartContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void retrieveCartItems() {
        getView().showProgress();
        mRetrieveCartItemsService.retrieveCartItems(mUserModel.getEmail(), mUserModel.getAuthToken(), this);
    }

    @Override
    public void removeItem(CartItemModel cartItem) {
        if (mCart != null && mCart.getItems() != null) {
            Log.d(TAG, "removeItem: removedCartItem: " + cartItem);

            if (mCart.getItems().remove(cartItem)) {
                Log.d(TAG, "removeItem: successful");
                cartItem.setRemoved(true);
                mRemovedCartItems.add(cartItem);

                // Always compute for installation at this step
                mCartComputationManager.compute(mCart, true);

                hasUnsavedChanges = true;
            }
        }

        if (mCart != null && mCart.getItems().isEmpty()) {
            getView().showEmptyCartBlocker();
        }
    }

    @Override
    public void changedItemQuantity(CartItemModel cartItem) {
        if (mCart != null && mCart.getItems() != null) {
            int indexOfAffectedCartItem = mCart.getItems().indexOf(cartItem);

            if (indexOfAffectedCartItem != -1) {
                mCart.getItems().set(indexOfAffectedCartItem, cartItem);
            }

            // Always compute for installation at this step
            mCartComputationManager.compute(mCart, true);

            hasUnsavedChanges = true;

        }
    }

    @Override
    public void checkOutCart() {
        if(hasUnsavedChanges) {
            updateCart(true);
        }else{
            getView().proceedToCheckOutScreen(mCart, mUserModel);
        }
    }

    @Override
    public void updateCart(boolean cartCheckout) {

        if(mCart == null || !hasUnsavedChanges){
            return;
        }

        Log.d(TAG, "updateCart: cartCheckout? " + cartCheckout);

        isCartCheckout = cartCheckout;

        if (isCartCheckout) {
            getView().showUpdateCartProgress();
        }

        ArrayList<CartItemAttributes> cartItemAttributesList = new ArrayList<>();

        if(mCart.getItems() != null) {
            // For the non-removed cart items
            for (CartItemModel cartItem : mCart.getItems()) {
                CartItemAttributes cartItemAttributes = new CartItemAttributes(cartItem.getId(),
                        cartItem.isInstallation(),
                        cartItem.getQuantity(),
                        false);

                cartItemAttributesList.add(cartItemAttributes);
            }
        }

        if(mRemovedCartItems != null) {
            // For the removed cart items
            for (CartItemModel cartItem : mRemovedCartItems) {
                CartItemAttributes cartItemAttributes = new CartItemAttributes(cartItem.getId(),
                        cartItem.isInstallation(),
                        cartItem.getQuantity(),
                        true);

                cartItemAttributesList.add(cartItemAttributes);
            }
        }

        CartRequestModel body = new CartRequestModel(cartItemAttributesList);

        mUpdateCartService.updateCart(mUserModel.getEmail(),
                mUserModel.getAuthToken(),
                mCart.getId(),
                body,
                this);
    }

    @Override
    public void onRetrieveCartItemSuccess(CartModel cart) {
        mCart = cart;
        hasUnsavedChanges = false;

        getView().hideProgressBlocker();
        getView().hideEmptyCartBlocker();
        getView().hideProgress();

        getView().updateCart(cart, true);
    }

    @Override
    public void onNoCartFound() {

        getView().hideProgressBlocker();
        getView().hideProgress();
        getView().showEmptyCartBlocker();
    }

    @Override
    public void onRetrieveCartItemError() {

        getView().hideEmptyCartBlocker();
        getView().hideProgress();

        getView().showRetrieveCartItemError();
    }

    @Override
    public void onComputationFinished(CartModel cartModel, boolean didComputeForInstallation) {
        mCart = cartModel;

        getView().updateCart(cartModel, false);
    }

    @Override
    public void onUpdateCartSuccess() {

        hasUnsavedChanges = false;

        mRemovedCartItems.clear();

        if(isCartCheckout) {
            getView().hideUpdateCartProgress();

            getView().proceedToCheckOutScreen(mCart, mUserModel);
        }else {
            getView().showUpdateCartSuccess();
        }
    }

    @Override
    public void onUpdateCartItemError() {
        hasUnsavedChanges = true;
        if (isCartCheckout) {
            getView().hideUpdateCartProgress();
        }
        getView().showUpdateCartError();
    }
}
