package com.livefitter.artzibit.main.checkout.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.main.checkout.model.request.CheckOutRequestModel;

/**
 * Created by LloydM on 7/18/17
 * for Livefitter
 */

public interface CheckOutCartService {
    void checkOutCart(@NonNull String userEmail,
                      @NonNull String authToken,
                      @NonNull int cartId,
                      @NonNull CheckOutRequestModel body,
                      Callback callback);

    public interface Callback {
        void onCheckOutSuccess();

        void onCheckOutError();
    }
}
