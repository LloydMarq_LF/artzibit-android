package com.livefitter.artzibit.main.checkout.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.databinding.ItemCheckoutFooterBinding;

import butterknife.ButterKnife;


/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class CheckOutItemFooterViewHolder extends RecyclerView.ViewHolder {


    private final ItemCheckoutFooterBinding mBinding;

    public static CheckOutItemFooterViewHolder create(LayoutInflater inflater, ViewGroup parent){
        ItemCheckoutFooterBinding binding = ItemCheckoutFooterBinding.inflate(inflater, parent, false);

        return new CheckOutItemFooterViewHolder(binding);
    }

    private CheckOutItemFooterViewHolder(ItemCheckoutFooterBinding mBinding) {
        super(mBinding.getRoot());
        this.mBinding = mBinding;

        ButterKnife.bind(this, mBinding.getRoot());
    }

    public void bind(String label, View.OnClickListener clickListener){
        mBinding.getRoot().setOnClickListener(clickListener);
        mBinding.setLabel(label);
        mBinding.executePendingBindings();
    }
}
