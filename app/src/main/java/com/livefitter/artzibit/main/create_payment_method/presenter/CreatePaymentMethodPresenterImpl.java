package com.livefitter.artzibit.main.create_payment_method.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.livefitter.artzibit.main.create_payment_method.CreatePaymentMethodContract;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.util.StripeHelper;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.lang.ref.WeakReference;


/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public class CreatePaymentMethodPresenterImpl implements CreatePaymentMethodContract.Presenter {

    private static final String TAG = CreatePaymentMethodPresenterImpl.class.getSimpleName();

    WeakReference<CreatePaymentMethodContract.View> mView;

    public CreatePaymentMethodPresenterImpl(CreatePaymentMethodContract.View view) {
        this.mView = new WeakReference<CreatePaymentMethodContract.View>(view);
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private CreatePaymentMethodContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void saveCreditCard(@NonNull String cardNumber, @NonNull int cardExpMonth, @NonNull int cardExpYear, @NonNull String cardCVC, @NonNull final boolean isDefault) {
        getView().showValidationProgressIndicator();

        final Card card = new Card(cardNumber,
                cardExpMonth,
                cardExpYear,
                cardCVC);

        Log.d(TAG, "saveCreditCard: card: " + card);

        if (card.validateCard()) {

            Stripe stripe = StripeHelper.getInstance(getView().getAppContext());
            stripe.createToken(
                    card,
                    new TokenCallback() {
                        public void onSuccess(Token token) {
                            // Create the creditCard model
                            CreditCardModel creditCardModel = new CreditCardModel(card, token.getId(), isDefault, true);
                            getView().hideValidationProgressIndicator();
                            getView().returnToCheckOutScreen(creditCardModel);
                        }

                        public void onError(Exception error) {
                            error.printStackTrace();
                            getView().hideValidationProgressIndicator();
                            getView().showCreateCardError();
                        }
                    }
            );
        } else {
            getView().hideValidationProgressIndicator();
            getView().showInvalidCardError();
        }
    }

}
