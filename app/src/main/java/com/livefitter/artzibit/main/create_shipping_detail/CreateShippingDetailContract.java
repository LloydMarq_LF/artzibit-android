package com.livefitter.artzibit.main.create_shipping_detail;

import com.livefitter.artzibit.model.ShippingDetailModel;

/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public interface CreateShippingDetailContract {
    public interface Presenter{
        void saveShippingDetail(String firstName,
                                String lastName,
                                String email,
                                String phoneNumber,
                                String address,
                                String city,
                                String country,
                                boolean isDefault);
    }

    public interface View{
        void returnToCheckOutScreen(ShippingDetailModel shippingDetailModel);
    }
}
