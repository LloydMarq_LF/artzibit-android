package com.livefitter.artzibit.main.artwork_detail.model.service;

import com.livefitter.artzibit.model.ArtworkModel;

/**
 * Created by LloydM on 6/28/17
 * for Livefitter
 */

public interface RetrieveArtworkDetailsService {

    void retrieveArtworkDetails(String userEmail,
                         String authToken,
                         int artworkID,
                         RetrieveArtworkDetailsCallback callback);

    interface RetrieveArtworkDetailsCallback {
        void onRetrieveArtworkSuccess(ArtworkModel artwork);
        void onRetrieveArtworkError();
    }
}
