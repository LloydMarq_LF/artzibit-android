package com.livefitter.artzibit.main.artwork_detail.model.service;


import com.livefitter.artzibit.main.artwork_detail.model.request.AddToCartRequestModel;

/**
 * Created by LloydM on 6/30/17
 * for Livefitter
 */

public interface AddToCartService {
    void addToCart(String userEmail, String authToken, AddToCartRequestModel params, Callback callback);
    void updateCart(String userEmail, String authToken, int cartId, AddToCartRequestModel params, Callback callback);
    void cancel();

    public interface Callback{
        void onAddToCartSuccess();
        void onAddToCartError();
    }
}
