package com.livefitter.artzibit.main.cart;

import android.support.annotation.NonNull;
import android.util.Log;

import com.livefitter.artzibit.model.CartItemModel;
import com.livefitter.artzibit.model.CartModel;

import java.math.BigDecimal;

/**
 * Created by LloydM on 7/7/17
 * for Livefitter
 */

public class CartComputationManager {

    private static final String TAG = CartComputationManager.class.getSimpleName();

    private CartModel mCartModel;
    private CartComputationCallback mCallback;

    public interface CartComputationCallback {
        void onComputationFinished(CartModel cartModel, boolean didComputeForInstallation);
    }

    public CartComputationManager(CartComputationCallback callback) {
        this.mCallback = callback;
    }

    public void compute(@NonNull CartModel cartModel, boolean computeForInstallation) {
        Log.d(TAG, "compute: Start");
        this.mCartModel = cartModel;

        if (mCartModel.getItems() == null
                || (mCartModel.getItems() != null && mCartModel.getItems().isEmpty())) {
            // Nothing to compute, call the callback and exit this method
            mCallback.onComputationFinished(mCartModel, computeForInstallation);

            return;
        }

        computeInstallationFees(computeForInstallation);

        computeShippingFees();

        computeItemPrice(computeForInstallation);

        computeSubtotal();

        mCallback.onComputationFinished(mCartModel, computeForInstallation);

        Log.d(TAG, "compute: Done");
    }

    private void computeInstallationFees(boolean computeForInstallation) {
        float tempInstallationFee = 0;

        for (int i = 0; i < mCartModel.getItems().size(); i++) {
            CartItemModel cartItem = mCartModel.getItems().get(i);
            if (cartItem.isInstallation()) {
                float installationAmount = cartItem.getBaseInstallationPrice() * cartItem.getQuantity();

                BigDecimal roundInstallationAmount = new BigDecimal(installationAmount).setScale(2, BigDecimal.ROUND_HALF_UP);
                installationAmount = roundInstallationAmount.floatValue();

                tempInstallationFee += installationAmount;
                cartItem.setInstallationAmount(installationAmount);
            }
        }

        if (computeForInstallation) {
            mCartModel.setInstallationFees(tempInstallationFee);
            Log.d(TAG, "computeInstallationFees: final installation fee: " + tempInstallationFee);
        } else {
            mCartModel.setInstallationFees(0);
            Log.d(TAG, "computeInstallationFees: final installation fee: 0 due to ignoring installation computation");
        }
    }

    private void computeShippingFees() {

        float tempShippingFee = 0;

        for (int i = 0; i < mCartModel.getItems().size(); i++) {
            CartItemModel cartItem = mCartModel.getItems().get(i);
            Log.d(TAG, "computeInstallationFees: " + i + " shipping amount: " + cartItem.getDeliveryAmount());
            if (tempShippingFee < cartItem.getDeliveryAmount()) {
                tempShippingFee = cartItem.getDeliveryAmount();
            }
        }

        mCartModel.setShipping(tempShippingFee);
        Log.d(TAG, "computeShippingFees: final shipping fee: " + tempShippingFee);
    }

    private void computeItemPrice(boolean includeInstallationFee) {

        for (int i = 0; i < mCartModel.getItems().size(); i++) {
            CartItemModel cartItem = mCartModel.getItems().get(i);

            float tempPrice = cartItem.getBasePrice() * cartItem.getQuantity();

            if (includeInstallationFee) {
                tempPrice += cartItem.getInstallationAmount();
            }
            BigDecimal roundPrice = new BigDecimal(tempPrice).setScale(2, BigDecimal.ROUND_HALF_UP);

            cartItem.setPrice(roundPrice.floatValue());
        }

    }

    private void computeSubtotal() {

        float tempSubtotal = 0;

        for (int i = 0; i < mCartModel.getItems().size(); i++) {
            CartItemModel cartItem = mCartModel.getItems().get(i);

            tempSubtotal += cartItem.getPrice();
        }

        // add the shipping fee on top
        tempSubtotal += mCartModel.getShipping();

        mCartModel.setSubtotalAmount(tempSubtotal);
    }
}
