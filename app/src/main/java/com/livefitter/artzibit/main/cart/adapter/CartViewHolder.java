package com.livefitter.artzibit.main.cart.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.livefitter.artzibit.R;
import com.livefitter.artzibit.databinding.ItemCartBinding;
import com.livefitter.artzibit.listener.CartItemListener;
import com.livefitter.artzibit.model.CartItemModel;
import com.livefitter.artzibit.util.DialogUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class CartViewHolder extends RecyclerView.ViewHolder {

    Context mContext;
    ItemCartBinding mBinding;
    CartItemModel mCartItem;
    CartItemListener mListener;
    int mQuantity;

    @BindView(R.id.quantity_stepper_count)
    TextView tvQuantityCount;
    @BindView(R.id.quantity_stepper_minus)
    ImageButton btnQuantityMinus;
    @BindView(R.id.quantity_stepper_plus)
    ImageButton btnQuantityPlus;
    @BindView(R.id.cart_item_button_remove)
    ImageView btnRemove;


    public static CartViewHolder create(Context context, LayoutInflater inflater, ViewGroup parent){
        ItemCartBinding binding = ItemCartBinding.inflate(inflater, parent, false);

        return new CartViewHolder(context, binding);
    }

    private CartViewHolder(Context context, ItemCartBinding mBinding) {
        super(mBinding.getRoot());
        this.mContext = context;
        this.mBinding = mBinding;

        ButterKnife.bind(this, mBinding.getRoot());
    }

    public void bind(String currencySymbol, CartItemModel cartItem, CartItemListener listener){
        this.mCartItem = cartItem;
        this.mListener = listener;
        this.mQuantity = mCartItem.getQuantity();

        mBinding.setCurrencySymbol(currencySymbol);
        mBinding.setCartItem(cartItem);
        mBinding.executePendingBindings();
    }

    @OnClick({R.id.quantity_stepper_minus, R.id.quantity_stepper_plus})
    public void changeQuantity(View view){
        int addend = view.getId() == R.id.quantity_stepper_plus? 1 : -1;

        int tempSum = mQuantity + addend;

        if(tempSum >= 1){
            mQuantity = tempSum;
            tvQuantityCount.setText(String.valueOf(mQuantity));

            mCartItem.setQuantity(mQuantity);
            mListener.onQuantityChange(mCartItem, mQuantity);
        }
    }

    @OnClick(R.id.cart_item_button_remove)
    public void removeItem(){
        String title = mContext.getString(R.string.msg_cart_remove_item_title);
        String message = mContext.getString(R.string.msg_cart_remove_item_message);
        String ok = mContext.getString(R.string.label_ok);
        String cancel = mContext.getString(R.string.label_cancel);
        DialogUtil.showConfirmationDialog(mContext, title, message, ok, cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == DialogInterface.BUTTON_POSITIVE){
                    mListener.onRemoveItem(mCartItem);
                }
            }
        });
    }
}
