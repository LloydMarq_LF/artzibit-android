package com.livefitter.artzibit.main.statistic.model.request;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.ArtworkViewDuration;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by LloydM on 7/19/17
 * for Livefitter
 */

public class ArtworkViewDurationRequestModel {
    private static final String TAG = ArtworkViewDurationRequestModel.class.getSimpleName();

    /**
     * user_app_tracking_log : {"user_id":1,"artwork_id":8,"start_time":"2017-06-30 10:00:00","end_time":"2017-06-30 10:01:00"}
     */

    @SerializedName("user_app_tracking_logs")
    private Payload userAppTrackingLog;

    public ArtworkViewDurationRequestModel(int userId, ArrayList<ArtworkModel> artworkModels) {
        ArrayList<UserAppTrackingLog> userAppTrackingLogs = new ArrayList<>();
        Log.d(TAG, "ArtworkViewDurationRequestModel: params artworkModels " + artworkModels);
        for (ArtworkModel artworkModel : artworkModels){
            if(artworkModel.getViews() != null && !artworkModel.getViews().isEmpty()) {
                for (ArtworkViewDuration artworkViewDuration : artworkModel.getViews()) {
                    UserAppTrackingLog userAppTrackingLogItem = new UserAppTrackingLog(userId,
                            artworkModel.getId(),
                            artworkViewDuration.getStartTime(),
                            artworkViewDuration.getEndTime());

                    userAppTrackingLogs.add(userAppTrackingLogItem);
                }
            }
        }

        this.userAppTrackingLog = new Payload(userAppTrackingLogs);
    }

    public class Payload{
        private ArrayList<UserAppTrackingLog> data;

        public Payload(ArrayList<UserAppTrackingLog> data) {
            this.data = data;
        }
    }

    public class UserAppTrackingLog {
        /**
         * user_id : 1
         * artwork_id : 8
         * start_time : 2017-06-30 10:00:00
         * end_time : 2017-06-30 10:01:00
         */

        @SerializedName("user_id")
        private int userId;
        @SerializedName("artwork_id")
        private int artworkId;
        @SerializedName("start_time")
        private String startTime;
        @SerializedName("end_time")
        private String endTime;

        public UserAppTrackingLog(int userId, int artworkId, String startTime, String endTime) {
            this.userId = userId;
            this.artworkId = artworkId;
            this.startTime = startTime;
            this.endTime = endTime;
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
