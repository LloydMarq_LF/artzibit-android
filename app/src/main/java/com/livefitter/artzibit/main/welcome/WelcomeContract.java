package com.livefitter.artzibit.main.welcome;

import android.content.Intent;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.UserModel;

/**
 * Created by LloydM on 5/11/17
 * for Livefitter
 */

public interface WelcomeContract {

    public interface Presenter {
        void signInToFacebook();
        void fbCallbackResult(int requestCode, int resultCode, Intent data);
    }

    public interface View extends BaseContractView{
        void showProgressDialog();
        void hideProgressDialog();
        void showErrorDialog();
        void saveUserLogin(UserModel userModel);
        void proceedToSignUpScreen();
        void proceedToSignUpScreen(String facebookUid, String facebookName, String facebookEmail, String facebookPictureUrl);
        void proceedToSignInScreen();
        void proceedToHomeScreen(UserModel userModel);
    }
}
