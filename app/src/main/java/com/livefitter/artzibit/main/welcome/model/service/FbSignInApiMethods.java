package com.livefitter.artzibit.main.welcome.model.service;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.welcome.model.request.FbSignInRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface FbSignInApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_SESSIONS)
    Call<BaseResponseModel<Object>> signInViaFacebook(@Body FbSignInRequestModel param);
}