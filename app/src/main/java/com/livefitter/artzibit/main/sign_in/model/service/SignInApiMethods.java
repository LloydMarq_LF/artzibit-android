package com.livefitter.artzibit.main.sign_in.model.service;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.sign_in.model.request.SignInRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.UserModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface SignInApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_SESSIONS)
    Call<BaseResponseModel<UserModel>> signUp(@Body SignInRequestModel param);
}