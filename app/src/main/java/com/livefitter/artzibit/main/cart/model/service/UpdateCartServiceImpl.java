package com.livefitter.artzibit.main.cart.model.service;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.internal.LinkedTreeMap;
import com.livefitter.artzibit.main.cart.model.request.CartRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.CartModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 7/5/17
 * for Livefitter
 */

public class UpdateCartServiceImpl implements UpdateCartService, Callback<BaseResponseModel<LinkedTreeMap>> {

    private static final String TAG = UpdateCartServiceImpl.class.getSimpleName();

    private Callback mCallback;
    private Call<BaseResponseModel<LinkedTreeMap>> mCall;

    @Override
    public void updateCart(@NonNull String userEmail, @NonNull String authToken, @NonNull int cartId, @NonNull CartRequestModel body, Callback callback) {
        cancel();

        mCallback = callback;

        UpdateCartApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(UpdateCartApiMethods.class);

        mCall = apiMethods.updateCart(userEmail,
                authToken,
                cartId,
                body);

        mCall.enqueue(this);
    }

    @Override
    public void cancel() {
        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }
    }

    @Override
    public void onResponse(Call<BaseResponseModel<LinkedTreeMap>> call, Response<BaseResponseModel<LinkedTreeMap>> response) {

        Log.d(TAG, "onResponse: raw response " + response);

        if(response.isSuccessful()){
            Log.d(TAG, "onResponse: response body: " + response.body());
            if(response.body().getStatus()){
                mCallback.onUpdateCartSuccess();
            }else{
                mCallback.onUpdateCartItemError();
            }
        }else{
            Log.d(TAG, "onResponse: unsuccessful - " + response.errorBody().toString());
            mCallback.onUpdateCartItemError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<LinkedTreeMap>> call, Throwable t) {
        t.printStackTrace();
        mCallback.onUpdateCartItemError();
    }
}
