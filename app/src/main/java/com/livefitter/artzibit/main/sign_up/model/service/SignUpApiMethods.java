package com.livefitter.artzibit.main.sign_up.model.service;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.sign_up.model.request.SignUpRequestModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface SignUpApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_REGISTRATIONS)
    Call<Object> signUp(@Body SignUpRequestModel param);
}