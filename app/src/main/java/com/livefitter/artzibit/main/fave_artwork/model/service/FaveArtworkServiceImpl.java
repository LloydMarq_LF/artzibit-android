package com.livefitter.artzibit.main.fave_artwork.model.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkRequestModel;
import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkUpdateRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 6/7/17
 * for Livefitter
 */

public class FaveArtworkServiceImpl implements FaveArtworkService, Callback<BaseResponseModel<LinkedTreeMap>> {

    private static final String TAG = FaveArtworkServiceImpl.class.getSimpleName();

    private Call<BaseResponseModel<LinkedTreeMap>> mCall;
    private FaveArtworkCallback mCallback;
    private int mArtworkId;


    @Override
    public void faveArtwork(String userEmail, String userAuthToken, final FaveArtworkRequestModel body, final FaveArtworkCallback callback) {
        mCallback = callback;
        mArtworkId = body.getUserFavorite().getArtworkId();

        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }

        FaveArtworkApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(FaveArtworkApiMethods.class);

        mCall = apiMethods.faveArtwork(userEmail, userAuthToken, body);

        mCall.enqueue(this);
    }

    @Override
    public void updateFaveArtwork(String userEmail, String userAuthToken, final int artworkId, int favoriteId, FaveArtworkUpdateRequestModel body, final FaveArtworkCallback callback) {
        mCallback = callback;
        mArtworkId = artworkId;

        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }

        FaveArtworkApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(FaveArtworkApiMethods.class);

        mCall = apiMethods.faveArtwork(userEmail, userAuthToken, favoriteId, body);

        mCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<BaseResponseModel<LinkedTreeMap>> call, Response<BaseResponseModel<LinkedTreeMap>> response) {

        Log.d(TAG, "onResponse: " + response);
        if(response.isSuccessful()){

            Log.d(TAG, "response success body: " + response.body().toString());

            if(response.body().getStatus()) {
                // Convert the received data into a UserModel
                LinkedTreeMap map = (LinkedTreeMap) response.body().getData();
                Gson gson = new Gson();
                JsonObject jsonObject = gson.toJsonTree(map).getAsJsonObject();
                Log.d(TAG, "onResponse: json object: " + jsonObject);

                if((jsonObject != null && jsonObject.size() > 0)
                    && jsonObject.has("artwork_id") && jsonObject.has("deleted") && jsonObject.has("id")){
                        mArtworkId = jsonObject.get("artwork_id").getAsInt();
                        boolean isDeleted = jsonObject.get("deleted").getAsBoolean();
                        int mArtworkFavedId = jsonObject.get("id").getAsInt();
                        mCallback.onFaveArtworkSuccess(mArtworkId, isDeleted, mArtworkFavedId);
                }else{
                    mCallback.onFaveArtworkError(mArtworkId);
                }
            }else{
                mCallback.onFaveArtworkError(mArtworkId);
            }
        }else{
            Log.d(TAG, "response failed body: "+ response.errorBody());
            mCallback.onFaveArtworkError(mArtworkId);
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<LinkedTreeMap>> call, Throwable t) {
        t.printStackTrace();
        mCallback.onFaveArtworkError(mArtworkId);
    }
}
