package com.livefitter.artzibit.main.sign_up.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.browse_artwork.view.HomeActivity;
import com.livefitter.artzibit.main.sign_up.SignUpContract;
import com.livefitter.artzibit.main.sign_up.presenter.SignUpPresenterImpl;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.view.FormFieldTextWatcher;
import com.livefitter.artzibit.util.FormValidityHelper;
import com.livefitter.artzibit.util.SharedPrefsUtil;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpScreen extends BaseActivity implements SignUpContract.View {
    private static final String TAG = SignUpScreen.class.getSimpleName();

    private SignUpPresenterImpl mPresenter;
    private String[] arrCountries;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.til_name)
    TextInputLayout tilName;
    @BindView(R.id.til_username)
    TextInputLayout tilUsername;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.til_confirm_password)
    TextInputLayout tilConfirmPassword;
    @BindView(R.id.til_country)
    TextInputLayout tilCountry;
    @BindView(R.id.et_name)
    TextInputEditText etName;
    @BindView(R.id.et_username)
    TextInputEditText etUsername;
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.et_password)
    TextInputEditText etPassword;
    @BindView(R.id.et_confirm_password)
    TextInputEditText etConfirmPassword;
    @BindView(R.id.et_country)
    AutoCompleteTextView etCountry;
    @BindView(R.id.btn_sign_up)
    Button btnSignUp;

    private ProgressDialog pdLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mPresenter = new SignUpPresenterImpl(this);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(getIntent().getExtras() != null){
            Bundle extras = getIntent().getExtras();

            if(extras.containsKey(AppConstants.BUNDLE_FACEBOOK_UID)
                    && extras.containsKey(AppConstants.BUNDLE_FACEBOOK_NAME)
                    && extras.containsKey(AppConstants.BUNDLE_FACEBOOK_EMAIL)){

                String uid = extras.getString(AppConstants.BUNDLE_FACEBOOK_UID);
                String name = extras.getString(AppConstants.BUNDLE_FACEBOOK_NAME);
                String email = extras.getString(AppConstants.BUNDLE_FACEBOOK_EMAIL);
                String profilePictureUrl = extras.getString(AppConstants.BUNDLE_FACEBOOK_PICTURE);

                mPresenter.setFacebookFields(uid, name, email, profilePictureUrl);
            }
        }


        // Set up a text watcher for errors
        etName.addTextChangedListener(new FormFieldTextWatcher(tilName));
        etUsername.addTextChangedListener(new FormFieldTextWatcher(tilUsername));
        etEmail.addTextChangedListener(new FormFieldTextWatcher(tilEmail));
        etPassword.addTextChangedListener(new FormFieldTextWatcher(tilPassword));
        etConfirmPassword.addTextChangedListener(new FormFieldTextWatcher(tilConfirmPassword));
        etCountry.addTextChangedListener(new FormFieldTextWatcher(tilCountry));

        etCountry.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    signUp();
                    return true;
                }
                return false;
            }
        });

        arrCountries = getResources().getStringArray(R.array.countries);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, arrCountries);

        etCountry.setAdapter(adapter);
    }

    @Override
    public void hideNonFacebookFields() {
        tilName.setVisibility(android.view.View.GONE);
        tilEmail.setVisibility(android.view.View.GONE);
        tilPassword.setVisibility(android.view.View.GONE);
        tilConfirmPassword.setVisibility(android.view.View.GONE);
    }

    @Override
    public void setFieldsFromFacebook(String name) {
        // Set a suggested username based on their name
        String trimmedName = name.replaceAll("\\s","");
        etUsername.setText(trimmedName);
        etUsername.setNextFocusDownId(R.id.et_country);
    }

    @OnClick(R.id.btn_sign_up)
    public void signUp(){
        hideSoftKeyboard();
        if(areFieldsValid()){
            String name = etName.getText().toString();
            String username = etUsername.getText().toString();
            String email = etEmail.getText().toString();
            String country = etCountry.getText().toString();
            String password = etPassword.getText().toString();

            mPresenter.doSignUp(name, username, email, country, password);
        }
    }

    private boolean areFieldsValid() {
        boolean areFieldsValid = true;
        int passwordMinLength = getResources().getInteger(R.integer.password_min_length);

        if(tilName.getVisibility() == android.view.View.VISIBLE && !FormValidityHelper.isFieldComplete(etName)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilName.setError(errorMessage);
        }

        if(tilUsername.getVisibility() == android.view.View.VISIBLE && !FormValidityHelper.isFieldComplete(etUsername)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilUsername.setError(errorMessage);
        }

        if(tilEmail.getVisibility() == android.view.View.VISIBLE && !FormValidityHelper.isFieldComplete(etEmail)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilEmail.setError(errorMessage);
        }
        else if(tilEmail.getVisibility() == android.view.View.VISIBLE && !FormValidityHelper.isEmailValid(etEmail)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_invalid_email);
            tilEmail.setError(errorMessage);
        }

        if(tilPassword.getVisibility() == android.view.View.VISIBLE && !FormValidityHelper.isFieldComplete(etPassword)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilPassword.setError(errorMessage);
        }else if(tilPassword.getVisibility() == android.view.View.VISIBLE && !FormValidityHelper.doesFieldHaveEnoughCharacters(etPassword, passwordMinLength)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_password_short, passwordMinLength);
            tilPassword.setError(errorMessage);
        }

        if(tilConfirmPassword.getVisibility() == android.view.View.VISIBLE && !FormValidityHelper.isFieldComplete(etConfirmPassword)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilConfirmPassword.setError(errorMessage);
        }

        if( (tilPassword.getVisibility() == android.view.View.VISIBLE
                && tilConfirmPassword.getVisibility() == android.view.View.VISIBLE)
                && !FormValidityHelper.doFieldsMatch(etPassword, etConfirmPassword)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_password_mismatch);
            tilPassword.setError(errorMessage);
        }

        if(tilCountry.getVisibility() == android.view.View.VISIBLE && !FormValidityHelper.isFieldComplete(etCountry)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilCountry.setError(errorMessage);
        }else if (tilCountry.getVisibility() == android.view.View.VISIBLE
                && (arrCountries!= null
                && !Arrays.asList(arrCountries).contains(etCountry.getText().toString()))){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_invalid_country);
            tilCountry.setError(errorMessage);
        }

        return areFieldsValid;
    }

    @Override
    public BaseActivity getViewActivity() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgressDialog() {
        if(pdLoading == null){
            String progressMessage = getString(R.string.msg_progress_sign_up);
            pdLoading = new ProgressDialog(this);
            pdLoading.setMessage(progressMessage);
            pdLoading.setCancelable(false);
        }

        pdLoading.show();
    }

    @Override
    public void hideProgressDialog() {
        if(pdLoading != null && pdLoading.isShowing()){
            pdLoading.dismiss();
        }
    }

    @Override
    public void saveUserLogin(UserModel userModel) {
        SharedPrefsUtil.setUserAuth(this, userModel);
    }

    @Override
    public void proceedToHomeScreen(UserModel userModel) {
        Intent intent = new Intent(this, HomeActivity.class);
        //Intent intent = new Intent(this, CategorySelectScreen.class);
        intent.putExtra(AppConstants.BUNDLE_USER, userModel);
        switchActivity(intent, true);
    }

    @Override
    public void showErrorDialog() {
        String message = getString(R.string.msg_error_sign_up);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void showDupeEmailErrorDialog() {
        String message = getString(R.string.msg_error_duplicate_email);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void clearPasswordFields() {
        etPassword.setText("");
        etConfirmPassword.setText("");
    }

    @Override
    public void clearEmailField() {
        etEmail.setText("");
        etEmail.requestFocus();
    }

}
