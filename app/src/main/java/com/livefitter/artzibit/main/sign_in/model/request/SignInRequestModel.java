package com.livefitter.artzibit.main.sign_in.model.request;

import com.google.gson.Gson;

/**
 * Created by LloydM on 5/15/17
 * for Livefitter
 */

public class SignInRequestModel {

    /**
     {
         user: {
             email: "example@gmail.com",
             password: "xxxx"
         }
     }
     */

    private Payload user;

    public SignInRequestModel(String email, String password) {
        user = new Payload(email, password);
    }

    public Payload getUser() {
        return user;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    private class Payload {
        private String email, password;

        public Payload(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
