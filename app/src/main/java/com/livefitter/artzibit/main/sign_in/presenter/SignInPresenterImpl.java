package com.livefitter.artzibit.main.sign_in.presenter;

import com.livefitter.artzibit.main.sign_in.SignInContract;
import com.livefitter.artzibit.main.sign_in.model.request.SignInRequestModel;
import com.livefitter.artzibit.main.sign_in.model.service.SignInService;
import com.livefitter.artzibit.main.sign_in.model.service.SignInServiceImpl;
import com.livefitter.artzibit.model.UserModel;

import java.lang.ref.WeakReference;

/**
 * Created by LloydM on 5/12/17
 * for Livefitter
 */

public class SignInPresenterImpl implements SignInContract.Presenter, SignInService.SignInCallback {

    private WeakReference<SignInContract.View> mView;
    private SignInServiceImpl mModel;

    public SignInPresenterImpl(SignInContract.View view) {
        mView = new WeakReference<SignInContract.View>(view);

        mModel = new SignInServiceImpl();
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private SignInContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void doSignIn(String email, String password) {
        getView().showProgressDialog();
        mModel.doSignIn(new SignInRequestModel(email, password), this);
    }

    @Override
    public void onSuccess(UserModel userModel) {
        getView().hideProgressDialog();
        getView().saveUserLogin(userModel);
        getView().proceedToHomeScreen(userModel);
    }

    @Override
    public void onError() {
        getView().hideProgressDialog();
        getView().clearPasswordField();
        getView().showErrorDialog();
    }

    @Override
    public void onInvalidSignIn() {
        getView().hideProgressDialog();
        getView().clearPasswordField();
        getView().showInvalidSignInDialog();
    }
}