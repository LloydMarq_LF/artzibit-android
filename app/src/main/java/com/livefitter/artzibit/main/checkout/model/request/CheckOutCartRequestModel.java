package com.livefitter.artzibit.main.checkout.model.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.livefitter.artzibit.model.CartItemAttributes;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/18/17
 * for Livefitter
 */

public class CheckOutCartRequestModel {
    private static final String STATUS_CHECKOUT = "checkout";
    public static final int SHIPPING_DETAIL_ID_NULL = -3;

    private String status;
    @SerializedName("user_shipping_detail_id")
    private int selectedShippingDetailId;

    public CheckOutCartRequestModel(@NonNull boolean forCheckOut) {
        this(forCheckOut, SHIPPING_DETAIL_ID_NULL);
    }

    public CheckOutCartRequestModel(@NonNull boolean forCheckOut, @Nullable int selectedShippingDetailId) {
        if(forCheckOut) {
            this.status = STATUS_CHECKOUT;
        }
        this.selectedShippingDetailId = selectedShippingDetailId;
    }

    public String getStatus() {
        return status;
    }

    public int getSelectedShippingDetailId() {
        return selectedShippingDetailId;
    }
}
