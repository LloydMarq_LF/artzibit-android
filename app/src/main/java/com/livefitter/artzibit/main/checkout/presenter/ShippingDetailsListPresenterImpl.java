package com.livefitter.artzibit.main.checkout.presenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.checkout.ShippingDetailsListContract;
import com.livefitter.artzibit.main.checkout.model.service.RetrieveShippingDetailService;
import com.livefitter.artzibit.main.checkout.model.service.RetrieveShippingDetailServiceImpl;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.model.UserModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public class ShippingDetailsListPresenterImpl implements ShippingDetailsListContract.Presenter, RetrieveShippingDetailService.Callback {

    private static final String TAG = ShippingDetailsListPresenterImpl.class.getSimpleName();
    private static final String BUNDLE_USER = "user_model";
    private static final String BUNDLE_SHIPPING_DETAIL_LIST = "shipping_details";
    private static final String BUNDLE_CHOSEN_SHIPPING_DETAIL = "chosen_shipping_detail";

    private UserModel mUserModel;
    private ArrayList<ShippingDetailModel> mShippingDetails;
    private ShippingDetailModel mChosenShippingDetail;

    private WeakReference<ShippingDetailsListContract.View> mView;
    private RetrieveShippingDetailService mService;

    public ShippingDetailsListPresenterImpl(UserModel userModel, ShippingDetailsListContract.View view) {
        this.mUserModel = userModel;
        this.mView = new WeakReference<ShippingDetailsListContract.View>(view);

        this.mService = new RetrieveShippingDetailServiceImpl();
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private ShippingDetailsListContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void retrieveShippingDetails() {
        getView().showProgressIndicator();

        mService.retrieveShippingDetail(mUserModel.getEmail(),
                mUserModel.getAuthToken(),
                this);
    }

    @Override
    public void onShippingDetailSelected(ShippingDetailModel selectedShippingDetailModel) {
        if (selectedShippingDetailModel != null) {
            mChosenShippingDetail = selectedShippingDetailModel;


            for (ShippingDetailModel shippingDetailModel : mShippingDetails) {
                shippingDetailModel.setSelected(shippingDetailModel.equals(selectedShippingDetailModel));
            }

            getView().setProgressCondition(true);
            getView().setProceedButtonVisible(true);
            //broadcast this change
            broadcastToReceiver();

            if(ShippingDetailModel.shouldShowShippingChargeWarning(mChosenShippingDetail)){
                getView().showForeignAddressWarning();
            }
        }
    }

    @Override
    public void onFooterClick() {
        getView().proceedToCreateShippingDetailsScreen();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.REQUEST_CREATE_SHIPPING_DETAIL && resultCode == Activity.RESULT_OK) {
            if (data != null && data.hasExtra(AppConstants.BUNDLE_SHIPPING_DETAIL)) {
                final ShippingDetailModel newShippingDetailModel = data.getParcelableExtra(AppConstants.BUNDLE_SHIPPING_DETAIL);
                if (newShippingDetailModel != null) {

                    if (mShippingDetails != null && !mShippingDetails.isEmpty()) {

                        // Go through the list of values
                        // and adjust their isDefault values
                        for (ShippingDetailModel shippingDetailModel : mShippingDetails) {

                            if (newShippingDetailModel.isDefault()) {
                                shippingDetailModel.setDefault(false);
                            }
                        }
                    } else {
                        if (mShippingDetails == null) {
                            mShippingDetails = new ArrayList<>();
                        }
                        //Automatically set that new shipping detail as the default
                        newShippingDetailModel.setDefault(true);
                    }
                    newShippingDetailModel.setSelected(true);
                    mShippingDetails.add(newShippingDetailModel);

                    // And set the newest shipping detail as the currently selected
                    onShippingDetailSelected(newShippingDetailModel);
                    getView().updateShippingList(mShippingDetails);

                }
            }
        }
    }

    @Override
    public void saveInstanceState(Bundle outstate) {
        if (outstate != null) {
            outstate.putParcelable(BUNDLE_USER, mUserModel);
            outstate.putParcelableArrayList(BUNDLE_SHIPPING_DETAIL_LIST, mShippingDetails);
            outstate.putParcelable(BUNDLE_CHOSEN_SHIPPING_DETAIL, mChosenShippingDetail);
        }
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        // Restore the existing data or fetch latest data from backend
        if (savedInstanceState != null) {
            mUserModel = savedInstanceState.getParcelable(BUNDLE_USER);
            mShippingDetails = savedInstanceState.getParcelableArrayList(BUNDLE_SHIPPING_DETAIL_LIST);
            mChosenShippingDetail = savedInstanceState.getParcelable(BUNDLE_CHOSEN_SHIPPING_DETAIL);

            if (mUserModel != null && mShippingDetails != null && mChosenShippingDetail != null) {
                getView().updateShippingList(mShippingDetails);
                getView().setProgressCondition(true);
                getView().setProceedButtonVisible(true);
            }
        } else {
            retrieveShippingDetails();
        }

    }

    @Override
    public void navigateToNextStep() {
        Intent intent = new Intent(AppConstants.INTENT_ACTION_CHECKOUT_PROCESS);
        intent.putExtra(AppConstants.BUNDLE_CHECKOUT_NAVIGATION, 0);
        LocalBroadcastManager.getInstance(getView().getViewActivity()).sendBroadcast(intent);
    }

    @Override
    public void onRetrieveShippingDetailSuccess(ArrayList<ShippingDetailModel> shippingDetailModels) {
        getView().hideProgressIndicator();
        getView().updateShippingList(shippingDetailModels);

        this.mShippingDetails = shippingDetailModels;
        if (ShippingDetailModel.hasDefaultSelection(shippingDetailModels)) {
            onShippingDetailSelected(ShippingDetailModel.retrieveDefaultSelection(shippingDetailModels));
        } else {
            getView().setProgressCondition(false);
            getView().setProceedButtonVisible(false);
        }
    }

    @Override
    public void onRetrieveShippingDetailError() {
        getView().hideProgressIndicator();
        getView().setProgressCondition(false);
        getView().setProceedButtonVisible(false);
        getView().showRetrieveShippingDetailError();
    }

    /**
     * Parses {@link #mShippingDetails} and looks for shipping details that are {@link ShippingDetailModel#isSelected() selected}
     * or {@link ShippingDetailModel#isNew() new} and compiles them to be broadcast to a broadcast receiver in {@link CheckOutPresenterImpl}
     *
     */
    private void broadcastToReceiver() {

        if (mShippingDetails != null && !mShippingDetails.isEmpty() && mChosenShippingDetail != null) {
            ArrayList<ShippingDetailModel> relevantShippingDetails = new ArrayList<>();

            // look for new shipping details that were created just now
            for (ShippingDetailModel shippingDetail : mShippingDetails) {
                if (shippingDetail.isNew() || shippingDetail.isSelected()) {
                    relevantShippingDetails.add(shippingDetail);
                }
            }



            final Intent intent = new Intent(AppConstants.INTENT_ACTION_CHECKOUT_PROCESS);
            intent.putExtra(AppConstants.BUNDLE_SHIPPING_DETAIL_LIST, relevantShippingDetails);

            //broadcast this change, after a delay
            // Fix to avoid the bug that the broadcast not being sent after creation of a new shipping detail
            final Runnable r = new Runnable() {
                public void run() {
                    LocalBroadcastManager.getInstance(getView().getViewActivity()).sendBroadcast(intent);
                }
            };
            new Handler().postDelayed(r, 1000);

        }

    }
}
