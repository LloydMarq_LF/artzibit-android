package com.livefitter.artzibit.main.fave_artwork.model.service;

import android.support.annotation.NonNull;

import com.google.gson.internal.LinkedTreeMap;
import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkRequestModel;
import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkUpdateRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FaveArtworkApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_ARTWORKS_FAVORITES)
    Call<BaseResponseModel<LinkedTreeMap>> faveArtwork(@NonNull @Header("X-User-Email") String userEmail,
                                                                      @NonNull @Header("X-User-Token") String authToken,
                                                                      @Body FaveArtworkRequestModel body);


    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @PUT(AppConstants.URL_ARTWORKS_FAVORITES + "/{favorite_id}")
    Call<BaseResponseModel<LinkedTreeMap>> faveArtwork(@NonNull @Header("X-User-Email") String userEmail,
                                                       @NonNull @Header("X-User-Token") String authToken,
                                                       @Path("favorite_id") int favoriteId,
                                                       @Body FaveArtworkUpdateRequestModel body);
}