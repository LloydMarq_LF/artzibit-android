package com.livefitter.artzibit.main.statistic.model.service;

import com.livefitter.artzibit.main.fave_artwork.model.service.RetrieveFaveArtworkService;
import com.livefitter.artzibit.main.sign_up.model.request.SignUpRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.AppUsageCountRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.AppUsageDurationRequestModel;
import com.livefitter.artzibit.model.UserModel;

/**
 * Created by LloydM on 5/15/17
 * for Livefitter
 */

public interface StatsAppUsageCountService {
    void logAppUsageCount(String userEmail, String authToken, AppUsageCountRequestModel body, Callback callback);

    public interface Callback {
        void onAppUsageCountSuccess();
        void onAppUsageCountError();
    }
}
