package com.livefitter.artzibit.main.artwork_detail.model.request;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by LloydM on 6/19/17
 * for Livefitter
 */

public class AddToCartRequestModel {

    private Payload cart;

    public AddToCartRequestModel() {
    }

    public AddToCartRequestModel(ArrayList<AddToCartItemAttributes> addToCartItemAttributes) {
        this.cart = new Payload(addToCartItemAttributes);
    }

    public class Payload {
        @SerializedName("cart_items_attributes")
        private ArrayList<AddToCartItemAttributes> addToCartItemAttributes;

        public Payload(ArrayList<AddToCartItemAttributes> addToCartItemAttributes) {
            this.addToCartItemAttributes = addToCartItemAttributes;
        }

        public ArrayList<AddToCartItemAttributes> getAddToCartItemAttributes() {
            return addToCartItemAttributes;
        }

    }

    public Payload getCart() {
        return cart;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
