package com.livefitter.artzibit.main.artwork_detail.presenter;

import android.util.Log;

import com.livefitter.artzibit.main.artwork_detail.ArtworkDetailContract;
import com.livefitter.artzibit.main.artwork_detail.model.service.RetrieveArtworkDetailsService;
import com.livefitter.artzibit.main.artwork_detail.model.service.RetrieveArtworkDetailsServiceImpl;
import com.livefitter.artzibit.main.artwork_detail.model.request.AddToCartRequestModel;
import com.livefitter.artzibit.main.artwork_detail.model.request.AddToCartItemAttributes;
import com.livefitter.artzibit.main.artwork_detail.model.service.AddToCartService;
import com.livefitter.artzibit.main.artwork_detail.model.service.AddToCartServiceImpl;
import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkRequestModel;
import com.livefitter.artzibit.main.fave_artwork.model.request.FaveArtworkUpdateRequestModel;
import com.livefitter.artzibit.main.fave_artwork.model.service.FaveArtworkService;
import com.livefitter.artzibit.main.fave_artwork.model.service.FaveArtworkServiceImpl;
import com.livefitter.artzibit.main.statistic.ArtzibitLogger;
import com.livefitter.artzibit.main.statistic.model.service.StatsArtworkViewDurationService;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.ArtworkSizeModel;
import com.livefitter.artzibit.model.ArtworkViewDuration;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.AspectRatioUtil;
import com.livefitter.artzibit.util.CalendarHelper;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by LloydM on 6/28/17
 * for Livefitter
 */

public class ArtworkDetailPresenterImpl implements ArtworkDetailContract.Presenter, RetrieveArtworkDetailsService.RetrieveArtworkDetailsCallback, FaveArtworkService.FaveArtworkCallback, AddToCartService.Callback {

    private static final String TAG = ArtworkDetailPresenterImpl.class.getSimpleName();

    private UserModel mUserModel;
    private int mArtworkId;
    private ArtworkModel mArtwork;
    private ArtworkSizeModel mArtworkSize;
    private int mQuantity = 0;
    private boolean isUsingCustomSizes;
    private boolean isArtworkInstalled;

    private WeakReference<ArtworkDetailContract.View> mView;
    private RetrieveArtworkDetailsService mRetrieveArtworkDetailsService;
    private FaveArtworkService mFaveArtworkService;
    private AddToCartService mAddToCartService;
    private boolean allowSizeFieldChange = true;
    private String mCurrentViewStartTime = "";

    public ArtworkDetailPresenterImpl(ArtworkDetailContract.View view, int artworkId, UserModel userModel) {
        this.mView = new WeakReference<ArtworkDetailContract.View>(view);
        mRetrieveArtworkDetailsService = new RetrieveArtworkDetailsServiceImpl();
        mFaveArtworkService = new FaveArtworkServiceImpl();
        mAddToCartService = new AddToCartServiceImpl();

        mArtworkId = artworkId;

        mUserModel = userModel;
    }


    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private ArtworkDetailContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void retrieveArtworkDetails() {
        getView().showRetrievalProgress();
        mRetrieveArtworkDetailsService.retrieveArtworkDetails(mUserModel.getEmail(), mUserModel.getAuthToken(), mArtworkId, this);
    }

    @Override
    public void onInstallationCheckboxChecked(boolean isChecked) {
        isArtworkInstalled = isChecked;
    }

    @Override
    public void onSizeSpinnerItemSelected(String selectedSize) {

        if (selectedSize.equals(ArtworkSizeModel.CUSTOM_SIZE)) {

            if (isUsingCustomSizes == selectedSize.equals(ArtworkSizeModel.CUSTOM_SIZE)) {
                // Do nothing and ignore this
                return;
            } else {
                isUsingCustomSizes = selectedSize.equals(ArtworkSizeModel.CUSTOM_SIZE);
                getView().resetCustomSizeFields();
                getView().toggleCustomSizeLayoutVisibility(isUsingCustomSizes);
            }
        } else {
            ArtworkSizeModel tempSize = new ArtworkSizeModel(selectedSize, -1, -1, -1, -1, "");
            int selectedSizeIndex = mArtwork.getAvailableSizes().indexOf(tempSize);
            if (selectedSizeIndex >= 0 && mArtwork.getAvailableSizes().get(selectedSizeIndex) != null) {
                isUsingCustomSizes = false;
                mArtworkSize = mArtwork.getAvailableSizes().get(selectedSizeIndex);
                Log.d(TAG, "onSizeSpinnerItemSelected: selected size: " + mArtworkSize);
            }
        }

    }

    @Override
    public void changeArtworkQuantity(boolean isIncreased) {
        if (isIncreased) {
            mQuantity++;
        } else {
            mQuantity--;

            if (mQuantity < 0) {
                mQuantity = 0;
            }
        }

        getView().updateArtworkQuantity(mQuantity);
    }

    @Override
    public void favoriteArtwork() {

        if (mArtwork.getFavoriteId() == 0 && !mArtwork.isFaved()) {
            // Artwork faved value is not yet created in the backend
            FaveArtworkRequestModel body = new FaveArtworkRequestModel(mArtwork.getId());
            mFaveArtworkService.faveArtwork(mUserModel.getEmail(),
                    mUserModel.getAuthToken(),
                    body,
                    this);
        } else {
            // Artwork faved value is already created in the backend, update it
            FaveArtworkUpdateRequestModel body = new FaveArtworkUpdateRequestModel(mArtwork.isFaved());
            mFaveArtworkService.updateFaveArtwork(mUserModel.getEmail(),
                    mUserModel.getAuthToken(),
                    mArtwork.getId(),
                    mArtwork.getFavoriteId(),
                    body,
                    this);
        }
    }

    @Override
    public void viewArtworkInAR() {

    }

    @Override
    public void addArtworkToCart() {

        AddToCartItemAttributes addToCartItemAttributes = new AddToCartItemAttributes(mArtwork.getId(),
                isArtworkInstalled,
                mQuantity);

        addToCartItemAttributes.setFixedSizeId(mArtworkSize.getSizeId());
        addToCartItemAttributes.setFixedSizePriceId(mArtworkSize.getPriceId());

        doAddArtworkToCart(addToCartItemAttributes);
    }

    @Override
    public void addArtworkToCart(float height, float width) {

        AddToCartItemAttributes addToCartItemAttributes = new AddToCartItemAttributes(mArtwork.getId(),
                isArtworkInstalled,
                mQuantity);

        addToCartItemAttributes.setHeight(height);
        addToCartItemAttributes.setWidth(width);

        doAddArtworkToCart(addToCartItemAttributes);

    }

    private void doAddArtworkToCart(AddToCartItemAttributes addToCartItemAttributes) {

        getView().showAddToCartProgress();

        boolean isCartExisting = mArtwork.getCartId() > 0;

        if (isCartExisting) {
            addToCartItemAttributes.setCartId(mArtwork.getCartId());
            ArrayList<AddToCartItemAttributes> payload = new ArrayList<AddToCartItemAttributes>();

            payload.add(addToCartItemAttributes);
            AddToCartRequestModel params = new AddToCartRequestModel(payload);

            mAddToCartService.updateCart(mUserModel.getEmail(),
                    mUserModel.getAuthToken(),
                    mArtwork.getCartId(),
                    params,
                    this);
        } else {

            ArrayList<AddToCartItemAttributes> payload = new ArrayList<AddToCartItemAttributes>();

            payload.add(addToCartItemAttributes);
            AddToCartRequestModel params = new AddToCartRequestModel(payload);

            mAddToCartService.addToCart(mUserModel.getEmail(),
                    mUserModel.getAuthToken(),
                    params,
                    this);
        }
    }

    @Override
    public void onAddToCartCancel() {
        mAddToCartService.cancel();
    }

    @Override
    public void onSizeValueChange(float width, float height) {
        if(allowSizeFieldChange) {
            boolean hasChangedWidth = width > 0;
            Log.d(TAG, "onSizeValueChange: width: " + width + ", height: " + height);

            allowSizeFieldChange = false;

            if (hasChangedWidth) {
                float heightCounterpart = AspectRatioUtil.computeHeight(width, mArtwork.getMinimumSize().getAspectRatio());

                heightCounterpart = Math.round(heightCounterpart);
                Log.d(TAG, "onSizeValueChange: affecting height: " + heightCounterpart);

                if (heightCounterpart > mArtwork.getMaximumSize().getHeight()) {
                    Log.d(TAG, "onSizeValueChange: capping to max");
                    heightCounterpart = mArtwork.getMaximumSize().getHeight();
                } else if (heightCounterpart < mArtwork.getMinimumSize().getHeight()) {
                    Log.d(TAG, "onSizeValueChange: capping to min");
                    heightCounterpart = mArtwork.getMinimumSize().getHeight();
                }

                Log.d(TAG, "onSizeValueChange: affecting height - final: " + heightCounterpart);

                getView().changeHeightField(heightCounterpart);
            } else {
                float widthCounterpart = AspectRatioUtil.computeWidth(height, mArtwork.getMinimumSize().getAspectRatio());

                widthCounterpart = Math.round(widthCounterpart);
                Log.d(TAG, "onSizeValueChange: affecting width: " + widthCounterpart);

                if (widthCounterpart > mArtwork.getMaximumSize().getWidth()) {
                    Log.d(TAG, "onSizeValueChange: capping to max");
                    widthCounterpart = mArtwork.getMaximumSize().getWidth();
                } else if (widthCounterpart < mArtwork.getMinimumSize().getWidth()) {
                    Log.d(TAG, "onSizeValueChange: capping to min");
                    widthCounterpart = mArtwork.getMinimumSize().getWidth();
                }

                Log.d(TAG, "onSizeValueChange: affecting width - final: " + widthCounterpart);

                getView().changeWidthField(widthCounterpart);
            }

            allowSizeFieldChange = true;
        }

    }

    @Override
    public boolean allowSizeFieldChange() {
        return allowSizeFieldChange;
    }

    @Override
    public ArtworkSizeModel getMinimumSize() {
        if(mArtwork != null){
            return mArtwork.getMinimumSize();
        }
        return null;
    }

    @Override
    public ArtworkSizeModel getMaximumSize() {
        if(mArtwork != null){
            return mArtwork.getMaximumSize();
        }
        return null;
    }

    @Override
    public void onArtworkViewStart() {
        mCurrentViewStartTime = CalendarHelper.getFormattedCurrentTime();
    }

    @Override
    public void onArtworkViewEnd() {

        if(mArtwork != null) {
            if (mArtwork.getViews() == null) {
                mArtwork.setViews(new ArrayList<ArtworkViewDuration>());
            }

            ArtworkViewDuration artworkViewDuration = new ArtworkViewDuration(mCurrentViewStartTime, CalendarHelper.getFormattedCurrentTime());
            mArtwork.getViews().add(artworkViewDuration);

            submitArtworkViewStatistics();
        }
    }

    @Override
    public void submitArtworkViewStatistics() {
        if(mArtwork != null){
            ArrayList<ArtworkModel> artworkModelArrayList = new ArrayList<>();
            artworkModelArrayList.add(mArtwork);
            ArtzibitLogger.getInstance().viewArtworkDuration(mUserModel, artworkModelArrayList, new StatsArtworkViewDurationService.Callback() {
                @Override
                public void onArtworkViewDurationSuccess() {

                    mCurrentViewStartTime = "";
                }

                @Override
                public void onArtworkViewDurationError() {

                }
            });

        }
    }

    @Override
    public void onRetrieveArtworkSuccess(ArtworkModel artwork) {
        mArtwork = artwork;

        onArtworkViewStart();

        // Compute for the min-max size of the artwork
        artwork.computeMinMaxSizes();

        getView().hideRetrievalProgress();
        getView().updateArtwork(mArtwork);
        ArrayList<String> artworkSizeLabels = new ArrayList<>(mArtwork.getAvailableSizes().size());
        for (int i = 0; i < mArtwork.getAvailableSizes().size(); i++) {
            ArtworkSizeModel artworkSizeModel = mArtwork.getAvailableSizes().get(i);

            artworkSizeLabels.add(artworkSizeModel.getLabel());
        }
        // Add the custom size to the end
        artworkSizeLabels.add(ArtworkSizeModel.CUSTOM_SIZE);
        getView().updateSizeSpinnerItems(artworkSizeLabels);

        if (mQuantity != 0) {
            mQuantity = 0;
            getView().updateArtworkQuantity(mQuantity);
        }

        getView().toggleCustomSizeLayoutVisibility(false);
    }

    @Override
    public void onRetrieveArtworkError() {
        getView().hideRetrievalProgress();
        getView().showArtworkRetrievalError();

        if (mQuantity != 0) {
            mQuantity = 0;
            getView().updateArtworkQuantity(mQuantity);
        }

        getView().toggleCustomSizeLayoutVisibility(false);
    }

    @Override
    public void onFaveArtworkSuccess(int artworkId, boolean isDeleted, int artworkFavedId) {

        mArtwork.setFaved(!isDeleted);
        mArtwork.setFavoriteId(artworkFavedId);

        getView().updateArtwork(mArtwork);
    }

    @Override
    public void onFaveArtworkError(int artworkId) {
        getView().showArtworkFaveErrorMessage();
    }

    @Override
    public void onAddToCartSuccess() {
        getView().hideAddToCartProgress();
        getView().showAddToCartSuccess();
    }

    @Override
    public void onAddToCartError() {
        getView().hideAddToCartProgress();
        getView().showAddToCartError();
    }
}
