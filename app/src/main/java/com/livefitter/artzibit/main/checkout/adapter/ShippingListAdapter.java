package com.livefitter.artzibit.main.checkout.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.main.checkout.adapter.viewholder.CheckOutItemFooterViewHolder;
import com.livefitter.artzibit.main.checkout.adapter.viewholder.ShippingAddressItemViewHolder;
import com.livefitter.artzibit.model.ShippingDetailModel;

import java.util.ArrayList;


/**
 * Created by LloydM on 7/13/17
 * for Livefitter
 */

public class ShippingListAdapter extends RecyclerView.Adapter {

    private static final String TAG = ShippingListAdapter.class.getSimpleName();

    private static final int ITEM_TYPE_NORMAL = 12;
    private static final int ITEM_TYPE_FOOTER = 13;

    private Context mContext;
    private LayoutInflater mInflater;
    private ShippingAddressListCallback mCallback;
    private ArrayList<ShippingDetailModel> mShippingAddresses;
    private String mFooterLabel;

    public interface ShippingAddressListCallback {
        void onShippingAddressSelected(ShippingDetailModel shippingAddress);
        void onFooterClick();
    }

    public ShippingListAdapter(Context context, String footerLabel, ShippingAddressListCallback shippingAddressListCallback) {
        this.mContext = context;
        this.mCallback = shippingAddressListCallback;
        this.mFooterLabel = footerLabel;

        this.mInflater = LayoutInflater.from(context);
    }

    public void setShippingAddresses(ArrayList<ShippingDetailModel> shippingAddresses) {
        this.mShippingAddresses = shippingAddresses;

        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM_TYPE_NORMAL){
            return ShippingAddressItemViewHolder.create(mInflater, parent);
        }
        return CheckOutItemFooterViewHolder.create(mInflater, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);

        switch (viewType){
            case ITEM_TYPE_NORMAL:
                final ShippingDetailModel shippingDetailModel = mShippingAddresses.get(position);

                if(shippingDetailModel != null){
                    ((ShippingAddressItemViewHolder)holder).bind(shippingDetailModel, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCallback.onShippingAddressSelected(shippingDetailModel);
                            setSelectedItem(shippingDetailModel);
                        }
                    });
                }

                break;

            case ITEM_TYPE_FOOTER:
                ((CheckOutItemFooterViewHolder)holder).bind(mFooterLabel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallback.onFooterClick();
                    }
                });

                break;
        }
    }


    @Override
    public int getItemViewType(int position) {

        if(mShippingAddresses != null
                && (position >= 0 && position < mShippingAddresses.size())){

            return ITEM_TYPE_NORMAL;
        }

        return ITEM_TYPE_FOOTER;
    }

    @Override
    public int getItemCount() {
        if(mShippingAddresses != null && !mShippingAddresses.isEmpty()){

            return mShippingAddresses.size() + 1;
        }

        // For the footer
        return 1;
    }

    private void setSelectedItem(ShippingDetailModel selectedItem) {
        if(selectedItem != null && mShippingAddresses != null && !mShippingAddresses.isEmpty()){
            for(ShippingDetailModel shippingAddress : mShippingAddresses){
                shippingAddress.setSelected(shippingAddress.equals(selectedItem));
            }

            notifyDataSetChanged();
        }
    }
}
