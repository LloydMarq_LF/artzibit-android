package com.livefitter.artzibit.main.statistic;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;

import com.livefitter.artzibit.main.statistic.model.request.AppUsageCountRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.AppUsageDurationRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.ArtworkViewCountRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.ArtworkViewDurationRequestModel;
import com.livefitter.artzibit.main.statistic.model.service.StatsAppUsageCountService;
import com.livefitter.artzibit.main.statistic.model.service.StatsAppUsageCountServiceImpl;
import com.livefitter.artzibit.main.statistic.model.service.StatsAppUsageDurationService;
import com.livefitter.artzibit.main.statistic.model.service.StatsAppUsageDurationServiceImpl;
import com.livefitter.artzibit.main.statistic.model.service.StatsArtworkViewCountService;
import com.livefitter.artzibit.main.statistic.model.service.StatsArtworkViewCountServiceImpl;
import com.livefitter.artzibit.main.statistic.model.service.StatsArtworkViewDurationService;
import com.livefitter.artzibit.main.statistic.model.service.StatsArtworkViewDurationServiceImpl;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.ArtworkViewDuration;
import com.livefitter.artzibit.model.UserModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/19/17
 * for Livefitter
 */

public class ArtzibitLogger
        implements StatsAppUsageCountService.Callback,
        StatsAppUsageDurationService.Callback,
        StatsArtworkViewCountService.Callback,
        StatsArtworkViewDurationService.Callback{

    private static final String TAG = ArtzibitLogger.class.getSimpleName();

    private static ArtzibitLogger _instance;
    private ArrayList<ArtworkModel> mArtworkViewCountList;
    private ArrayList<ArtworkModel> mArtworkViewDurationList;
    private StatsAppUsageCountService mAppUsageCountService;
    private StatsAppUsageDurationService mAppUsageDurationService;
    private StatsArtworkViewCountService mArtworkViewCountService;
    private StatsArtworkViewDurationService mArtworkViewDurationService;
    private String mAppUsageStartTime;

    public static ArtzibitLogger getInstance(){

        if (_instance == null)
        {
            _instance = new ArtzibitLogger();
        }
        return _instance;
    }

    public ArtzibitLogger() {
        mArtworkViewCountList = new ArrayList<>();
        mArtworkViewDurationList = new ArrayList<>();
        mAppUsageCountService = new StatsAppUsageCountServiceImpl();
        mAppUsageDurationService = new StatsAppUsageDurationServiceImpl();
        mArtworkViewCountService = new StatsArtworkViewCountServiceImpl();
        mArtworkViewDurationService = new StatsArtworkViewDurationServiceImpl();
    }

    /**
     * Logs each usage of the app
     * @param userModel needed to associate this stat to a specific user
     */
    public void appUsageCount(@NonNull UserModel userModel){
        AppUsageCountRequestModel body = new AppUsageCountRequestModel(userModel.getId());
        mAppUsageCountService.logAppUsageCount(userModel.getEmail(), userModel.getAuthToken(), body, this);
    }

    /**
     * Sets the starting time of the app. Supposedly called on each activity's {@link Activity#onStart() onStart} method
     * @param startTime
     */
    public void appUsageDurationStart(@NonNull String startTime ){
        this.mAppUsageStartTime = startTime;
    }

    /**
     * Logs the duration of a single usage of the app. Supposedly called on each activity's {@link Activity#onStop() onStop} method
     * Format of time is in "YYYY-MM-DD HH:MM:SS"
     * @param userModel needed to associate this stat to a specific user
     * @param endTime
     */
    public void appUsageDurationEnd(@NonNull UserModel userModel, @NonNull String endTime ){
        AppUsageDurationRequestModel body = new AppUsageDurationRequestModel(userModel.getId(), mAppUsageStartTime, endTime);
        mAppUsageDurationService.logAppUsageDuration(userModel.getEmail(), userModel.getAuthToken(), body, this);
    }

    public void viewArtworkCount(@NonNull UserModel userModel, @NonNull ArrayList<ArtworkModel> viewedArtworkList, final StatsArtworkViewCountService.Callback callback){

        for (ArtworkModel artworkModel : viewedArtworkList){
            // Determine if this artwork does not yet exist in mArtworkViewCountList

            if(mArtworkViewCountList.contains(artworkModel)){
                //update the view counter
                ArtworkModel cachedArtwork = mArtworkViewCountList.get(mArtworkViewCountList.indexOf(artworkModel));
                int updatedViewCounter = cachedArtwork.getViewCounter() + artworkModel.getViewCounter();
                cachedArtwork.setViewCounter(updatedViewCounter);
            }else{
                mArtworkViewCountList.add(artworkModel);
            }
        }

        if(mArtworkViewCountList != null && !mArtworkViewCountList.isEmpty()) {
            ArtworkViewCountRequestModel body = new ArtworkViewCountRequestModel(userModel.getId(), mArtworkViewCountList);

            Log.d(TAG, "~~viewArtworkCount: body: " + body);
            mArtworkViewCountService = new StatsArtworkViewCountServiceImpl();
            mArtworkViewCountService.logArtworkViewCount(userModel.getEmail(), userModel.getAuthToken(), body, new StatsArtworkViewCountService.Callback() {
                @Override
                public void onArtworkViewCountSuccess() {
                    Log.d(TAG, "onArtworkViewCountSuccess: ");
                    // clear out the mArtworkViewCountList to make room for the next batch of view updates
                    mArtworkViewCountList.clear();
                    callback.onArtworkViewCountSuccess();
                }

                @Override
                public void onArtworkViewCountError() {
                    Log.d(TAG, "onArtworkViewCountError: ");
                    callback.onArtworkViewCountError();
                }
            });
        }
    }

    public void viewArtworkDuration(@NonNull UserModel userModel, @NonNull ArrayList<ArtworkModel> viewedArtworkList, final StatsArtworkViewDurationService.Callback callback){

        Log.d(TAG, "viewArtworkDuration: viewedArtworkList null? " + (viewedArtworkList == null) + ", empty? " + viewedArtworkList.isEmpty());
        for (ArtworkModel artworkModel : viewedArtworkList){
            // Determine if this artwork does not yet exist in mArtworkViewCountList

            if(mArtworkViewDurationList.contains(artworkModel)){
                //update the view counter
                ArtworkModel cachedArtwork = mArtworkViewDurationList.get(mArtworkViewDurationList.indexOf(artworkModel));

                if(artworkModel.getViews() != null && !artworkModel.getViews().isEmpty()) {
                    if(cachedArtwork.getViews() == null){
                        cachedArtwork.setViews(new ArrayList<ArtworkViewDuration>());
                    }

                    cachedArtwork.getViews().addAll(artworkModel.getViews());
                }
            }else{
                mArtworkViewDurationList.add(artworkModel);
            }
        }

        Log.d(TAG, "viewArtworkDuration: mArtworkViewDurationList null? " + (mArtworkViewDurationList == null) + ", empty? " + mArtworkViewDurationList.isEmpty());
        if(mArtworkViewDurationList!= null && !mArtworkViewDurationList.isEmpty()) {
            ArtworkViewDurationRequestModel body = new ArtworkViewDurationRequestModel(userModel.getId(), mArtworkViewDurationList);
            Log.d(TAG, "~~viewArtworkDuration: ArtworkViewDurationRequestModel params: " + body);
            mArtworkViewDurationService = new StatsArtworkViewDurationServiceImpl();
            mArtworkViewDurationService.logArtworkViewDuration(userModel.getEmail(), userModel.getAuthToken(), body, new StatsArtworkViewDurationService.Callback() {
                @Override
                public void onArtworkViewDurationSuccess() {
                    Log.d(TAG, "onArtworkViewDurationSuccess: ");
                    // clear out the mArtworkViewDurationList to make room for the next batch of view updates
                    mArtworkViewDurationList.clear();
                    callback.onArtworkViewDurationSuccess();
                }

                @Override
                public void onArtworkViewDurationError() {
                    Log.d(TAG, "onArtworkViewDurationError: ");
                    callback.onArtworkViewDurationError();
                }
            });
        }
    }

    @Override
    public void onAppUsageCountSuccess() {
        Log.d(TAG, "onAppUsageCountSuccess: ");
    }

    @Override
    public void onAppUsageCountError() {
        Log.d(TAG, "onAppUsageCountError: ");
    }

    @Override
    public void onAppUsageDurationSuccess() {
        Log.d(TAG, "onAppUsageDurationSuccess: ");
    }

    @Override
    public void onAppUsageDurationError() {
        Log.d(TAG, "onAppUsageDurationError: ");
    }

    @Override
    public void onArtworkViewCountSuccess() {
        Log.d(TAG, "onArtworkViewCountSuccess: ");
        // clear out the mArtworkViewCountList to make room for the next batch of view updates
//        mArtworkViewCountList.clear();
    }

    @Override
    public void onArtworkViewCountError() {
        Log.d(TAG, "onArtworkViewCountError: ");
    }

    @Override
    public void onArtworkViewDurationSuccess() {
        Log.d(TAG, "onArtworkViewDurationSuccess: ");
        // clear out the mArtworkViewDurationList to make room for the next batch of view updates
//        mArtworkViewDurationList.clear();
    }

    @Override
    public void onArtworkViewDurationError() {
        Log.d(TAG, "onArtworkViewDurationError: ");
    }
}
