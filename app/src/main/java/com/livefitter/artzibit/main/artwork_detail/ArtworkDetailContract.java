package com.livefitter.artzibit.main.artwork_detail;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.ArtworkSizeModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 6/28/17
 * for Livefitter
 */

public interface ArtworkDetailContract {
    public interface Presenter{
        void retrieveArtworkDetails();
        void onInstallationCheckboxChecked(boolean isChecked);
        void onSizeSpinnerItemSelected(String selectedSize);
        void changeArtworkQuantity(boolean isIncreased);
        void favoriteArtwork();
        void viewArtworkInAR();
        void addArtworkToCart();
        void addArtworkToCart(float height, float width);
        void onAddToCartCancel();
        void onSizeValueChange(float width, float height);
        boolean allowSizeFieldChange();
        ArtworkSizeModel getMinimumSize();
        ArtworkSizeModel getMaximumSize();
        void onArtworkViewStart();
        void onArtworkViewEnd();
        void submitArtworkViewStatistics();
    }

    public interface View extends BaseContractView{
        void updateArtwork(ArtworkModel artwork);
        void updateSizeSpinnerItems(ArrayList<String> artworkSizeLabels);
        void updateArtworkQuantity(int quantity);
        void toggleCustomSizeLayoutVisibility(boolean isVisible);
        void resetCustomSizeFields();
        void showRetrievalProgress();
        void hideRetrievalProgress();
        void showAddToCartProgress();
        void hideAddToCartProgress();
        void showAddToCartSuccess();
        void showAddToCartError();
        void showArtworkRetrievalError();
        void showArtworkFaveErrorMessage();
        void changeWidthField(float width);
        void changeHeightField(float height);
    }
}
