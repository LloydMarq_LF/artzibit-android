package com.livefitter.artzibit.main.checkout.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.livefitter.artzibit.databinding.ItemCartReviewBinding;
import com.livefitter.artzibit.model.CartItemModel;

import butterknife.ButterKnife;


/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class CartReviewViewHolder extends RecyclerView.ViewHolder {

    private ItemCartReviewBinding mBinding;

    public static CartReviewViewHolder create(LayoutInflater inflater, ViewGroup parent){
        ItemCartReviewBinding binding = ItemCartReviewBinding.inflate(inflater, parent, false);

        return new CartReviewViewHolder(binding);
    }

    private CartReviewViewHolder(ItemCartReviewBinding mBinding) {
        super(mBinding.getRoot());
        this.mBinding = mBinding;

        ButterKnife.bind(this, mBinding.getRoot());
    }

    public void bind(String currencySymbol, CartItemModel cartItem){

        mBinding.setCurrencySymbol(currencySymbol);
        mBinding.setCartItem(cartItem);
        mBinding.executePendingBindings();
    }
}
