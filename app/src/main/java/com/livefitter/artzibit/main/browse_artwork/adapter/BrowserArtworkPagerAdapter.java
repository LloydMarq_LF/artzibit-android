package com.livefitter.artzibit.main.browse_artwork.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.livefitter.artzibit.R;
import com.livefitter.artzibit.listener.ArtworkClickListener;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.util.GlideApp;

import java.util.ArrayList;


/**
 * Created by LloydM on 5/29/17
 * for Livefitter
 */

public class BrowserArtworkPagerAdapter extends PagerAdapter {

    private static final String TAG = BrowserArtworkPagerAdapter.class.getSimpleName();

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<ArtworkModel> mArtworkList;
    private ArtworkClickListener mClickListener;

    public BrowserArtworkPagerAdapter(Context context, ArtworkClickListener clickListener) {
        this.mContext = context;
        mClickListener = clickListener;

        mInflater = LayoutInflater.from(context);
    }

    public void setArtworkList(ArrayList<ArtworkModel> artworkList) {
        this.mArtworkList = artworkList;
        notifyDataSetChanged();
    }

    public void updateArtwork(ArtworkModel artworkModel){
        if(mArtworkList.contains(artworkModel)){
            int affectedArtworkIndex = mArtworkList.indexOf(artworkModel);

            mArtworkList.set(affectedArtworkIndex, artworkModel);

            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return mArtworkList == null ? 0 : mArtworkList.size();
    }

    public ArtworkModel getArtwork(int position){
        return mArtworkList != null? mArtworkList.get(position) : null;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_browse_artwork, container, false);
        ImageView imageView = (ImageView) viewGroup.findViewById(R.id.browse_artwork_image);

        if (mArtworkList != null) {
            ArtworkModel artworkModel = mArtworkList.get(position);

            GlideApp.with(mContext)
                    .load(artworkModel.getImage())
                    .centerInside()
                    .placeholder(R.drawable.img_logo)
                    .into(imageView);

            viewGroup.setTag(artworkModel);

            viewGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArtworkModel artworkModel = (ArtworkModel) v.getTag();
                    if (artworkModel != null) {
                        Log.d(TAG, "on click: " + artworkModel.getName() + " - view count " + artworkModel.getViewCounter());
                        mClickListener.onArtworkClick(artworkModel);
                    }
                }
            });
        }

        container.addView(viewGroup);
        return viewGroup;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(Object object) {

        if(object instanceof View) {
            View objectView = (View) object;

            if (objectView.getTag() instanceof ArtworkModel) {

                if (mArtworkList.contains(objectView.getTag())) {
                    return mArtworkList.indexOf(objectView.getTag());
                }
            }
        }

        return POSITION_NONE;
    }
}
