package com.livefitter.artzibit.main.sign_in;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.UserModel;

/**
 * Created by LloydM on 5/12/17
 * for Livefitter
 */

public interface SignInContract {

    public interface Presenter {
        void doSignIn(String email, String password);
    }

    public interface View extends BaseContractView {
        void proceedToHomeScreen(UserModel userModel);
        void showProgressDialog();
        void hideProgressDialog();
        void saveUserLogin(UserModel userModel);
        void showErrorDialog();
        void showInvalidSignInDialog();
        void clearPasswordField();
    }
}
