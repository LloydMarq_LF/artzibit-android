package com.livefitter.artzibit.main.checkout;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.broadcastreceiver.CheckOutUpdatesReceiver;
import com.livefitter.artzibit.model.ShippingDetailModel;

/**
 * Created by LloydM on 7/17/17
 * for Livefitter
 */

public interface CheckOutContract {
    public interface Presenter extends CheckOutUpdatesReceiver.OnReceiveBroadcastListener{
        void registerBroadcastReceiver();
        void unregisterBroadcastReceiver();
        ShippingDetailModel getSelectedShippingDetail();
    }

    public interface View extends BaseContractView{
        void navigateToPage(int childIndex);
        void proceedToReceiptScreen(String orderNumber);
        void showCheckOutProgress();
        void hideCheckOutProgress();
        void showCheckOutCartError();
    }
}
