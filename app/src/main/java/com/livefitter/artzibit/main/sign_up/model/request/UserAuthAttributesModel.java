package com.livefitter.artzibit.main.sign_up.model.request;

/**
 * Used as the inner class for some API calls for sign in/up using facebook
 */

public class UserAuthAttributesModel {
    private String provider, uid;

    public UserAuthAttributesModel(String provider, String uid) {
        this.provider = provider;
        this.uid = uid;
    }

    public String getProvider() {
        return provider;
    }

    public String getUid() {
        return uid;
    }
}
