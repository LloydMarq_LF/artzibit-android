package com.livefitter.artzibit.main.splash.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.main.browse_artwork.view.HomeActivity;
import com.livefitter.artzibit.main.splash.SplashContract;
import com.livefitter.artzibit.main.splash.presenter.SplashPresenterImpl;
import com.livefitter.artzibit.main.welcome.view.WelcomeScreen;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.SharedPrefsUtil;


/**
 * Created by LloydM on 5/11/17
 * for Livefitter
 */

public class SplashScreen extends BaseActivity implements SplashContract.View {

    SplashPresenterImpl mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new SplashPresenterImpl(this);
        mPresenter.checkUserLoggedIn();
    }

    @Override
    public UserModel retrieveUserDetails() {
        return SharedPrefsUtil.getUserAuth(this);
    }

    @Override
    public int retrieveChosenCategory() {
        return 0;
    }

    @Override
    public void proceedToWelcomeScreen() {
        switchActivity(new Intent(this,WelcomeScreen.class), true);
    }

    @Override
    public void proceedToHomeScreen(UserModel userModel) {
        Intent intent = new Intent(this, HomeActivity.class);
        //Intent intent = new Intent(this, CategorySelectScreen.class);
        intent.putExtra(AppConstants.BUNDLE_USER, userModel);
        switchActivity(intent, true);
    }

    @Override
    public void proceedToCategorySelectScreen(UserModel userModel, int chosenCategory) {

    }

    @Override
    public BaseActivity getViewActivity() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }
}
