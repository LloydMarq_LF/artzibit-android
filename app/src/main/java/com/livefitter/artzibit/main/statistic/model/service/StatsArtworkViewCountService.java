package com.livefitter.artzibit.main.statistic.model.service;

import com.livefitter.artzibit.main.statistic.model.request.ArtworkViewCountRequestModel;

/**
 * Created by LloydM on 5/15/17
 * for Livefitter
 */

public interface StatsArtworkViewCountService {
    void logArtworkViewCount(String userEmail, String authToken, ArtworkViewCountRequestModel body, Callback callback);

    public interface Callback {
        void onArtworkViewCountSuccess();

        void onArtworkViewCountError();
    }
}
