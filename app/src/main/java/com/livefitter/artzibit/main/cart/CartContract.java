package com.livefitter.artzibit.main.cart;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.CartItemModel;
import com.livefitter.artzibit.model.CartModel;
import com.livefitter.artzibit.model.UserModel;

/**
 * Created by LloydM on 7/6/17
 * for Livefitter
 */

public interface CartContract {
    public interface Presenter{
        void setCurrencySymbol(String currencySymbol);
        void retrieveCartItems();
        void removeItem(CartItemModel cartItem);
        void changedItemQuantity(CartItemModel cartItem);
        void checkOutCart();

        /**
         * Called whenever the user moves away from or closes the app entirely
         * Calls a request to save the cart items to the backend
         */
        void updateCart(boolean cartCheckout);
    }

    public interface View extends BaseContractView{
        void updateCart(CartModel cart, boolean refreshListDisplay);
        void proceedToCheckOutScreen(CartModel cart, UserModel userModel);
        void hideProgressBlocker();
        void showProgress();
        void hideProgress();
        void showEmptyCartBlocker();
        void showRetrieveCartItemError();
        void hideEmptyCartBlocker();
        void showUpdateCartProgress();
        void hideUpdateCartProgress();
        void showUpdateCartSuccess();
        void showUpdateCartError();
    }
}
