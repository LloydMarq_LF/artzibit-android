package com.livefitter.artzibit.main.splash;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.UserModel;

/**
 * Created by LloydM on 5/11/17
 * for Livefitter
 */

public interface SplashContract {
    public interface Presenter {
        void checkUserLoggedIn();
    }

    public interface View extends BaseContractView{
        UserModel retrieveUserDetails();
        int retrieveChosenCategory();
        void proceedToWelcomeScreen();
        void proceedToHomeScreen(UserModel userModel);
        void proceedToCategorySelectScreen(UserModel userModel, int chosenCategory);
    }
}
