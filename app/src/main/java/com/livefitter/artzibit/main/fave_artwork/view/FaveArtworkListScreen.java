package com.livefitter.artzibit.main.fave_artwork.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.artwork_detail.view.ArtworkDetailScreen;
import com.livefitter.artzibit.main.fave_artwork.adapter.FaveArtworkListAdapter;
import com.livefitter.artzibit.main.fave_artwork.FaveArtworkListContract;
import com.livefitter.artzibit.main.fave_artwork.presenter.FaveArtworkListPresenterImpl;
import com.livefitter.artzibit.model.FaveArtworkModel;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.view.CrossFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;


/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class FaveArtworkListScreen extends CrossFragment implements FaveArtworkListContract.View {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.blocker_layout)
    FrameLayout flEmptyBlocker;
    @BindView(R.id.progress_bar_layout)
    FrameLayout flProgressBar;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout srLayout;
    @BindView(R.id.recyclerview)
    RecyclerView rvFavorites;

    FaveArtworkListContract.Presenter mPresenter;
    FaveArtworkListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Bundle extras = getActivity().getIntent().getExtras();

        if (extras != null && extras.containsKey(AppConstants.BUNDLE_USER)) {
            UserModel userModel = ((UserModel) extras.getParcelable(AppConstants.BUNDLE_USER));

            mPresenter = new FaveArtworkListPresenterImpl(this, userModel);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fave_artwork_list, container, false);

        ButterKnife.bind(this, rootView);

        srLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(mPresenter != null && mAdapter != null){
                    mPresenter.retrieveFaveArtworks();
                    mAdapter.setFaveArtworkList(null);
                }
            }
        });

        int spanCount = getActivity().getResources().getInteger(R.integer.fave_artwork_column_count);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
        layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        rvFavorites.setLayoutManager(layoutManager);
        mAdapter = new FaveArtworkListAdapter(getActivity(), mPresenter);
        rvFavorites.setAdapter(mAdapter);
        rvFavorites.setItemAnimator(new FadeInAnimator());

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fave_artwork_list, menu);
    }

    @Override
    public void setOwnToolbar() {
        getViewActivity().setSupportActionBar(null);
        getViewActivity().setSupportActionBar(mToolbar);
        getViewActivity().getSupportActionBar().setTitle(R.string.title_faves);
        getViewActivity().invalidateOptionsMenu();
    }

    @Override
    public void onVisible() {
        //

        mPresenter.retrieveFaveArtworks();
    }

    @Override
    public void onHidden() {
        //
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void updateFaveArtworkList(ArrayList<FaveArtworkModel> faveArtworkList) {
        if(mAdapter != null){
            mAdapter.setFaveArtworkList(faveArtworkList);
        }
    }

    @Override
    public void proceedToArtworkDetails(FaveArtworkModel faveArtworkModel) {
        Intent intent = new Intent(getActivity(), ArtworkDetailScreen.class);
        intent.putExtra(AppConstants.BUNDLE_ARTWORK_ID, faveArtworkModel.getArtworkId());
        startActivity(intent);
    }

    @Override
    public void showProgressBlocker() {
        if(flProgressBar != null && flProgressBar.getVisibility() != View.VISIBLE){
            flProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBlocker() {
        if(flProgressBar != null && flProgressBar.getVisibility() != View.GONE){
            flProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgressIndicator() {
        if(!srLayout.isRefreshing()){
            srLayout.setRefreshing(true);
        }
    }

    @Override
    public void hideProgressIndicator() {
        if(srLayout.isRefreshing()){
            srLayout.setRefreshing(false);
        }
    }

    @Override
    public void showEmptyFavesError() {
        if(flEmptyBlocker != null && flEmptyBlocker.getVisibility() != View.VISIBLE){
            flEmptyBlocker.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideEmptyFavesError() {
        if(flEmptyBlocker != null && flEmptyBlocker.getVisibility() != View.GONE){
            flEmptyBlocker.setVisibility(View.GONE);
        }
    }

    @Override
    public void showErrorDialog() {
        String message = getString(R.string.msg_error_retrieve_fave_artworks);
        DialogUtil.showAlertDialog(getActivity(), message);
    }
}
