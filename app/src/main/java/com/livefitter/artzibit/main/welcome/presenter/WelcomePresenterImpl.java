package com.livefitter.artzibit.main.welcome.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.welcome.WelcomeContract;
import com.livefitter.artzibit.main.welcome.model.request.FbSignInRequestModel;
import com.livefitter.artzibit.main.welcome.model.service.FbSignInServiceImpl;
import com.livefitter.artzibit.model.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Arrays;

/**
 * Created by LloydM on 5/11/17
 * for Livefitter
 */

public class WelcomePresenterImpl
        implements WelcomeContract.Presenter,
        FbSignInServiceImpl.SignInCallback{

    private static final String TAG = WelcomePresenterImpl.class.getSimpleName();

    private WeakReference<WelcomeContract.View> mView;
    private FbSignInServiceImpl mService;

    private CallbackManager mCallbackManager;

    private String mFacebookUid, mFacebookEmail, mFacebookName, mFacebookPictureUrl;

    public WelcomePresenterImpl(@NonNull WelcomeContract.View view) {
        mView = new WeakReference<>(view);
        mService = new FbSignInServiceImpl();
    }

    @Override
    public void signInToFacebook() {
        Log.d(TAG, "signInToFacebook");

        if(mCallbackManager == null) {
            createFbCallbackManager();
        }

        LoginManager.getInstance().logInWithReadPermissions(
                getView().getViewActivity(),
                Arrays.asList("email", "public_profile")
        );
    }

    private void createFbCallbackManager() {

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "signInToFacebook success: " + loginResult.toString());

                AccessToken token = loginResult.getAccessToken();
                if(token != null){
                    Log.d(TAG, "token: " + token.getToken());
                    Log.d(TAG, "token user ID: " + token.getUserId());
                }

                Log.d(TAG, "granted fields: " + loginResult.getRecentlyGrantedPermissions().toString());

                getGraphRequest(token);
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "signInToFacebook cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "signInToFacebook error: " + error.toString());
                getView().showErrorDialog();
            }
        });
    }

    private void getGraphRequest(AccessToken token) {

        GraphRequest graphRequest = GraphRequest.newMeRequest(
                token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsonObject, GraphResponse response) {

                        Log.d(TAG, "Graph response jsonObject: " + jsonObject.toString());

                        if(jsonObject.has("email")
                                && jsonObject.has("name")
                                && jsonObject.has("id")) {
                            try {
                                mFacebookEmail = jsonObject.getString("email");
                                mFacebookName = jsonObject.getString("name");
                                mFacebookUid = jsonObject.getString("id");

                                // Check for the optional profile picture url
                                if(jsonObject.has("picture")){
                                    JSONObject profilePictureJSON = jsonObject.getJSONObject("picture");
                                    JSONObject profilePictureDataJSON = profilePictureJSON.getJSONObject("data");

                                    Log.d(TAG, "profilePictureDataJSON: " + profilePictureDataJSON);

                                    if(profilePictureDataJSON.has("url")){

                                        mFacebookPictureUrl = profilePictureDataJSON.getString("url");
                                        Log.d(TAG, "pictureUrl: " + mFacebookPictureUrl);
                                    }
                                }

                                doSignInViaFacebook(mFacebookEmail, mFacebookUid);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                onSignInError();
                            }
                        }else{
                            onSignInError();
                        }
                    }
                }
        );

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private void doSignInViaFacebook(String email, String uid) {
        getView().showProgressDialog();
        FbSignInRequestModel params = new FbSignInRequestModel(email, AppConstants.PROVIDER_FACEBOOK, uid);
        mService.doSignInViaFacebook(params, this);
    }

    @Override
    public void fbCallbackResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSignInSuccess(UserModel userModel) {
        getView().hideProgressDialog();
        getView().saveUserLogin(userModel);
        getView().proceedToHomeScreen(userModel);
    }

    @Override
    public void onUnregisteredSignIn() {
        getView().hideProgressDialog();
        getView().proceedToSignUpScreen(mFacebookUid, mFacebookName, mFacebookEmail, mFacebookPictureUrl);
    }

    @Override
    public void onSignInError() {
        getView().hideProgressDialog();
        getView().showErrorDialog();
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private WelcomeContract.View getView() throws NullPointerException{
        if ( mView != null )
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }
}
