package com.livefitter.artzibit.main.filter_artwork.model;

import com.livefitter.artzibit.model.ArtworkFilterModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 5/12/17
 * for Livefitter
 */

public interface RetrieveArtworkFiltersService {
    void retrieveArtworkFilters(Callback callback);

    interface Callback {
        void onSuccess(ArrayList<ArtworkFilterModel> filterList);
        void onError();
    }
}
