package com.livefitter.artzibit.main.filter_artwork.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.livefitter.artzibit.model.ArtworkFilterModel;
import com.livefitter.artzibit.model.FilterOption;


/**
 * Created by LloydM on 6/15/17
 * for Livefitter
 */

public class FilterOptionsAdapter extends RecyclerView.Adapter<FilterOptionsViewHolder> implements FilterOptionsViewHolder.OnFilterOptionSelectedListener {

    private static final String TAG = FilterOptionsAdapter.class.getSimpleName();

    private Context mContext;
    private ArtworkFilterModel mFilterModel;
    private LayoutInflater mInflater;

    public FilterOptionsAdapter(Context context, ArtworkFilterModel filterModel) {
        this.mContext = context;
        this.mFilterModel = filterModel;

        mInflater = LayoutInflater.from(context);
    }

    @Override
    public FilterOptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return FilterOptionsViewHolder.create(mInflater, parent, this);
    }

    @Override
    public void onBindViewHolder(FilterOptionsViewHolder holder, int position) {
        final FilterOption filterOption = mFilterModel.getOptions().get(position);

        if(filterOption != null){
            holder.bind(filterOption);
        }
    }

    @Override
    public int getItemCount() {
        return mFilterModel.getOptions() != null? mFilterModel.getOptions().size() : 0;
    }

    @Override
    public void onFilterOptionSelected(FilterOption selectedFilterOption) {

        for(FilterOption filterOption : mFilterModel.getOptions()){
            boolean doesMatchSelectedFilterOption = filterOption.equals(selectedFilterOption);
            boolean selectedValue = doesMatchSelectedFilterOption && selectedFilterOption.isSelected();
            filterOption.setSelected(selectedValue);

            if(doesMatchSelectedFilterOption){
                int selectedOptionId = selectedFilterOption.isSelected()? filterOption.getId() : ArtworkFilterModel.NO_SELECTED_OPTION;
                mFilterModel.setSelectedOptionId(selectedOptionId);
            }
        }


        notifyDataSetChanged();
    }

    public ArtworkFilterModel getFilterModel() {
        return mFilterModel;
    }
}
