package com.livefitter.artzibit.main.fave_artwork.model.service;

import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.FaveArtworkModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public interface RetrieveFaveArtworkService {
    void retrieveFaveArtwork(String userEmail, String authToken, Callback callback);

    public interface Callback {
        void onSuccess(ArrayList<FaveArtworkModel> artworkList);
        void onNoFavedArtworks();
        void onError();
    }
}
