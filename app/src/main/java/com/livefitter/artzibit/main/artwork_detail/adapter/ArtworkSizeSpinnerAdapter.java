package com.livefitter.artzibit.main.artwork_detail.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.livefitter.artzibit.model.ArtworkSizeModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by LloydM on 6/29/17
 * for Livefitter
 */

public class ArtworkSizeSpinnerAdapter extends ArrayAdapter<String> {

    private LayoutInflater mInflater;
    private List<String> mLabels;



    public ArtworkSizeSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, int textViewResourceId, @NonNull List<String> objects) {
        super(context, resource, textViewResourceId, objects);
        mInflater = LayoutInflater.from(context);
        mLabels = objects;

    }

    public String getLabelAt(int position) {
        return mLabels.get(position);
    }
}
