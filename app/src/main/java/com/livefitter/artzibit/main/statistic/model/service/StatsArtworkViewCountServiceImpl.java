package com.livefitter.artzibit.main.statistic.model.service;

import android.util.Log;

import com.livefitter.artzibit.main.statistic.model.request.ArtworkViewCountRequestModel;
import com.livefitter.artzibit.main.statistic.model.request.ArtworkViewDurationRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by LloydM on 7/20/17
 * for Livefitter
 */

public class StatsArtworkViewCountServiceImpl implements StatsArtworkViewCountService {

    private static final String TAG = StatsArtworkViewCountServiceImpl.class.getSimpleName();


    private Callback mCallback;
    private Call<BaseResponseModel<Object>> mCall;

    @Override
    public void logArtworkViewCount(String userEmail, String authToken, ArtworkViewCountRequestModel body, Callback callback) {
        this.mCallback = callback;

        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }

        StatsArtworkViewCountApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(StatsArtworkViewCountApiMethods.class);

        this.mCall = apiMethods.logArtworkViewCount(userEmail, authToken, body);

        mCall.enqueue(new retrofit2.Callback<BaseResponseModel<Object>>() {
            @Override
            public void onResponse(Call<BaseResponseModel<Object>> call, Response<BaseResponseModel<Object>> response) {

                Log.d(TAG, "onViewCount: response: " + response);

                if(response.isSuccessful()){
                    Log.d(TAG, "onViewCount: response body: " + response.body());
                    if(response.body().getStatus()){
                        mCallback.onArtworkViewCountSuccess();
                    }else{
                        Log.d(TAG, "onViewCount: failed: ");
                        mCallback.onArtworkViewCountError();
                    }
                }else{
                    Log.d(TAG, "onViewCount: unsuccessful: " + response.errorBody());
                    mCallback.onArtworkViewCountError();
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel<Object>> call, Throwable t) {
                t.printStackTrace();
                mCallback.onArtworkViewCountError();
            }
        });
    }
}
