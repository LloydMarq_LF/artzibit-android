package com.livefitter.artzibit.main.fave_artwork.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.databinding.ItemFaveArtworkBinding;
import com.livefitter.artzibit.listener.FaveArtworkClickListener;
import com.livefitter.artzibit.model.FaveArtworkModel;


/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class FaveArtworkListViewHolder extends RecyclerView.ViewHolder {

    ItemFaveArtworkBinding mBinding;


    public static FaveArtworkListViewHolder create(LayoutInflater inflater, ViewGroup parent){
        ItemFaveArtworkBinding binding = ItemFaveArtworkBinding.inflate(inflater, parent, false);

        return new FaveArtworkListViewHolder(binding);
    }

    private FaveArtworkListViewHolder(ItemFaveArtworkBinding mBinding) {
        super(mBinding.getRoot());
        this.mBinding = mBinding;
    }

    public void bind(final FaveArtworkModel artwork, final FaveArtworkClickListener clickListener){
        mBinding.setArtwork(artwork);
        mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onFaveArtworkClick(artwork);
            }
        });
        mBinding.executePendingBindings();
    }
}
