package com.livefitter.artzibit.main.browse_artwork.model;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.BaseResponseModel;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface RetrieveArtworkApiMethods {

    /**
     *
     * @param userEmail Used for authentication
     * @param authToken Used for authentication
     * @param queries Valid keys and values are as follows:
     *                <ol>
     *                <li>category_id:1 for Fixed | 2 for Custom</li>
     *                <li>artwork_style_id</li>
     *                <li>size:small|medium|large</li>
     *                <li>order:price|date</li>
     *                <li>by:asc for ascending |desc for descending</li>
     *                </ol>
     *
     *
     * @return The Retrofit Call object to execute
     */
    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @GET(AppConstants.URL_ARTWORKS)
    Call<BaseResponseModel<ArrayList<ArtworkModel>>> retrieveArtworks(@NonNull @Header("X-User-Email") String userEmail,
                                                                      @NonNull @Header("X-User-Token") String authToken,
                                                                      //@NonNull @Query("size_option_id") int categoryId,
                                                                      @NonNull @QueryMap Map<String, String> queries);
}