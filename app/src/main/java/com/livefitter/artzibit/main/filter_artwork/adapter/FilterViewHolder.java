package com.livefitter.artzibit.main.filter_artwork.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.databinding.ItemFilterTextBinding;


/**
 * Created by LloydM on 6/15/17
 * for Livefitter
 */

public class FilterViewHolder extends RecyclerView.ViewHolder {

    ItemFilterTextBinding mBinding;

    public static FilterViewHolder create(LayoutInflater inflater, ViewGroup parent){
        ItemFilterTextBinding binding = ItemFilterTextBinding.inflate(inflater, parent, false);

        return new FilterViewHolder(binding);
    }

    public FilterViewHolder(ItemFilterTextBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(String name, String value, View.OnClickListener clickListener){
        mBinding.getRoot().setOnClickListener(clickListener);
        mBinding.setName(name);
        mBinding.setValue(value);
        mBinding.executePendingBindings();
    }
}
