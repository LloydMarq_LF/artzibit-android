package com.livefitter.artzibit.main.filter_artwork.model;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.model.ArtworkFilterModel;
import com.livefitter.artzibit.model.BaseResponseModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface RetrieveArtworkApiMethods {

    /**
     *
     * @param userEmail Used for authentication
     * @param authToken Used for authentication
     * @param queries Valid keys and values are as follows:
     *                <ol>
     *                <li>category_id:1 for Fixed | 2 for Custom</li>
     *                <li>artwork_style_id</li>
     *                <li>size:small|medium|large</li>
     *                <li>order:price|date</li>
     *                <li>by:asc for ascending |desc for descending</li>
     *                </ol>
     *
     *
     * @return The Retrofit Call object to execute
     */
    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @GET(AppConstants.URL_ARTWORKS_FILTERS)
    Call<BaseResponseModel<ArrayList<ArtworkFilterModel>>> retrieveArtworkFilters();
}