package com.livefitter.artzibit.main.sign_in.model.service;

import com.livefitter.artzibit.main.sign_in.model.request.SignInRequestModel;
import com.livefitter.artzibit.model.UserModel;

/**
 * Created by LloydM on 5/12/17
 * for Livefitter
 */

public interface SignInService {
    void doSignIn(SignInRequestModel params, final SignInCallback callback);

    interface SignInCallback{
        void onSuccess(UserModel userModel);
        void onError();
        void onInvalidSignIn();
    }
}
