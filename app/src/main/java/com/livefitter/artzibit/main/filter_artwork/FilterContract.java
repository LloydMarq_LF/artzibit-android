package com.livefitter.artzibit.main.filter_artwork;

import android.content.Intent;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.ArtworkFilterModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 6/14/17
 * for Livefitter
 */

public interface FilterContract {

    public interface Presenter{
        void retrieveFilterOptions();
        void onFilterOptionSelected(ArtworkFilterModel filterModel);
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

    public interface View extends BaseContractView{
        void saveFilterPreferences(ArrayList<ArtworkFilterModel> filterModelList);
        void updateFilterList(ArrayList<ArtworkFilterModel> filterModelList);
        void showProgressDialog();
        void hideProgressDialog();
        void showErrorDialog();
        void proceedToFilterSubmenu(ArtworkFilterModel filterModel);
    }
}
