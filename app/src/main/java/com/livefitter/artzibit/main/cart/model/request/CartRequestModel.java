package com.livefitter.artzibit.main.cart.model.request;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.livefitter.artzibit.model.CartItemAttributes;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/7/17
 * for Livefitter
 */

public class CartRequestModel {
    public static final String STATUS_CHECKOUT = "checkout";

    private Payload cart;

    public CartRequestModel(@Nullable String status, ArrayList<CartItemAttributes> cartItemAttributes) {
        this.cart = new Payload(status, cartItemAttributes);
    }



    public CartRequestModel(ArrayList<CartItemAttributes> cartItemAttributes) {
        this.cart = new Payload(cartItemAttributes);
    }

    private class Payload{
        private String status;
        @SerializedName("cart_items_attributes")
        private ArrayList<CartItemAttributes> cartItemAttributes;

        public Payload(@Nullable String status, ArrayList<CartItemAttributes> cartItemAttributes) {
            this.status = status;
            this.cartItemAttributes = cartItemAttributes;
        }

        public Payload(ArrayList<CartItemAttributes> cartItemAttributes) {
            this.cartItemAttributes = cartItemAttributes;
        }
    }
}
