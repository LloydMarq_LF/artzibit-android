package com.livefitter.artzibit.main.cart.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.main.fave_artwork.model.service.RetrieveFaveArtworkService;
import com.livefitter.artzibit.model.CartModel;

/**
 * Created by LloydM on 7/5/17
 * for Livefitter
 */

public interface RetrieveCartItemsService {

    void retrieveCartItems(@NonNull String userEmail,
                           @NonNull String authToken,
                           Callback callback);
    void cancel();

    public interface Callback{
        void onRetrieveCartItemSuccess(CartModel cart);
        void onNoCartFound();
        void onRetrieveCartItemError();
    }
}
