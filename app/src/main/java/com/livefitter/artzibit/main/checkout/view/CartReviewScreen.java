package com.livefitter.artzibit.main.checkout.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.databinding.FragmentCartReviewBinding;
import com.livefitter.artzibit.main.checkout.CartReviewContract;
import com.livefitter.artzibit.main.checkout.adapter.CartReviewAdapter;
import com.livefitter.artzibit.main.checkout.presenter.CartReviewPresenterImpl;
import com.livefitter.artzibit.model.CartItemModel;
import com.livefitter.artzibit.model.CartModel;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.view.ConditionalPagerFragment;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by LloydM on 7/17/17
 * for Livefitter
 */

public class CartReviewScreen extends ConditionalPagerFragment implements CartReviewContract.View {

    private static final String TAG = CartReviewScreen.class.getSimpleName();

    @BindView(R.id.recyclerview)
    RecyclerView rvCart;

    private FragmentCartReviewBinding mBinding;
    private CartReviewContract.Presenter mPresenter;
    private CartReviewAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null && extras.containsKey(AppConstants.BUNDLE_CART)){
            CartModel cartModel = extras.getParcelable(AppConstants.BUNDLE_CART);
            ShippingDetailModel chosenShippingDetail = ((CheckOutScreen) getActivity()).getSelectedShippingModel();
            if(cartModel != null && chosenShippingDetail != null){

                //Set the default currency
                String currencySymbol = Currency.getInstance(Locale.getDefault()).getSymbol();
                mPresenter = new CartReviewPresenterImpl(this, cartModel, currencySymbol, chosenShippingDetail);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mBinding = FragmentCartReviewBinding.inflate(inflater);

        ButterKnife.bind(this, mBinding.getRoot());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvCart.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL);
        rvCart.addItemDecoration(itemDecoration);

        //Set the default currency
        String currencySymbol = Currency.getInstance(Locale.getDefault()).getSymbol();
        mAdapter = new CartReviewAdapter(getActivity(), currencySymbol);
        rvCart.setAdapter(mAdapter);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mPresenter != null){
            mPresenter.onViewReady();
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void updateCartItems(String currencySymbol, CartModel cart, boolean installationFeeEnabled) {
        mBinding.setCurrencySymbol(currencySymbol);

        if (cart.getShipping() > 0) {
            mBinding.setShippingFee(String.valueOf(cart.getShipping()));
        } else {
            mBinding.setShippingFee(null);
        }

        if (cart.getInstallationFees() > 0 && installationFeeEnabled) {
            mBinding.setInstallationFee(String.valueOf(cart.getInstallationFees()));
        } else {
            mBinding.setInstallationFee(null);
        }

        if (cart.getSubtotalAmount() > 0) {
            mBinding.setSubtotal(String.valueOf(cart.getSubtotalAmount()));
        } else {
            mBinding.setSubtotal(null);
        }
        mBinding.executePendingBindings();

        if(mAdapter != null){
            mAdapter.setCartItems(cart.getItems());
        }
    }

    @OnClick(R.id.checkout_item_button_proceed)
    public void proceedToNextStep(){
        if(mPresenter != null){
            mPresenter.navigateToNextStep();
        }
    }

}
