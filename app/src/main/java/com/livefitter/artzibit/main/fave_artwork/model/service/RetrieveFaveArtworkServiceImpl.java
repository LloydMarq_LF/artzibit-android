package com.livefitter.artzibit.main.fave_artwork.model.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.FaveArtworkModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 6/7/17
 * for Livefitter
 */

public class RetrieveFaveArtworkServiceImpl implements RetrieveFaveArtworkService, Callback<BaseResponseModel<ArrayList<FaveArtworkModel>>> {

    private static final String TAG = RetrieveFaveArtworkServiceImpl.class.getSimpleName();

    private Call<BaseResponseModel<ArrayList<FaveArtworkModel>>> mCall;
    private RetrieveFaveArtworkService.Callback mCallback;

    @Override
    public void retrieveFaveArtwork(String userEmail, String authToken, Callback callback) {
        mCallback = callback;

        if (mCall != null && mCall.isExecuted()) {
            mCall.cancel();
        }

        RetrieveFaveArtworkApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(RetrieveFaveArtworkApiMethods.class);

        mCall = apiMethods.retrieveFaveArtwork(userEmail, authToken);

        mCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<BaseResponseModel<ArrayList<FaveArtworkModel>>> call, Response<BaseResponseModel<ArrayList<FaveArtworkModel>>> response) {
        Log.d(TAG, "onResponse: " + response);
        if(response.isSuccessful()){

            Log.d(TAG, "response success body: " + response.body().toString());

            if(response.body().getStatus()) {
                // Convert the received data into a UserModel
                ArrayList<FaveArtworkModel> faveArtworkList = response.body().getData();

                if(faveArtworkList != null){
                    if(!faveArtworkList.isEmpty()) {
                        mCallback.onSuccess(faveArtworkList);
                    }else{
                        mCallback.onNoFavedArtworks();
                    }
                }else{
                    mCallback.onError();
                }
            }else{
                mCallback.onError();
            }
        }else{
            Log.d(TAG, "response failed body: "+ response.errorBody());
            mCallback.onError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<ArrayList<FaveArtworkModel>>> call, Throwable t) {
        t.printStackTrace();
        mCallback.onError();
    }
}
