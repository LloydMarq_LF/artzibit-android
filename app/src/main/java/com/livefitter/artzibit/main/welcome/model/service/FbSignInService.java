package com.livefitter.artzibit.main.welcome.model.service;

import com.livefitter.artzibit.main.welcome.model.request.FbSignInRequestModel;
import com.livefitter.artzibit.model.UserModel;

/**
 * Created by LloydM on 5/30/17
 * for Livefitter
 */

public interface FbSignInService {
    void doSignInViaFacebook(FbSignInRequestModel params, FbSignInService.SignInCallback callback);

    interface SignInCallback{
        void onSignInSuccess(UserModel userModel);
        void onUnregisteredSignIn();
        void onSignInError();
    }
}
