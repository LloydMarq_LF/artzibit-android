package com.livefitter.artzibit.main.sign_up;

import android.support.annotation.Nullable;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.UserModel;

/**
 * Created by LloydM on 5/12/17
 * for Livefitter
 */

public interface SignUpContract {
    interface Presenter {
        void setFacebookFields(String uid, String name, String email, String profilePictureUrl);

        /**
         * Instantiate the model based on whether we are signing up from facebook or manually
         * If so, use the fields instantiated from setFacebookFields();
         * Otherwise, use the parameters included
         */
        void doSignUp(@Nullable String name, String username, @Nullable String email, String country, String password);
    }

    interface View extends BaseContractView{
        void setFieldsFromFacebook(String name);
        void hideNonFacebookFields();
        void showProgressDialog();
        void hideProgressDialog();
        void showErrorDialog();
        void showDupeEmailErrorDialog();
        void clearPasswordFields();
        void clearEmailField();
        void saveUserLogin(UserModel userModel);
        void proceedToHomeScreen(UserModel userModel);
    }
}
