package com.livefitter.artzibit.main.cart.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.databinding.FragmentCartBinding;
import com.livefitter.artzibit.listener.CartItemListener;
import com.livefitter.artzibit.main.cart.adapter.CartAdapter;
import com.livefitter.artzibit.main.cart.CartContract;
import com.livefitter.artzibit.main.cart.presenter.CartPresenterImpl;
import com.livefitter.artzibit.main.checkout.view.CheckOutScreen;
import com.livefitter.artzibit.model.CartItemModel;
import com.livefitter.artzibit.model.CartModel;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.view.CrossFragment;

import java.util.Currency;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;


/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class CartScreen extends CrossFragment implements CartContract.View, CartItemListener {

    private static final String TAG = CartScreen.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout srLayout;
    @BindView(R.id.blocker_layout)
    FrameLayout flEmptyBlocker;
    @BindView(R.id.progress_bar_layout)
    FrameLayout flProgressBar;
    @BindView(R.id.recyclerview)
    RecyclerView rvCartItems;


    ProgressDialog dProgress;

    FragmentCartBinding mBinding;
    CartContract.Presenter mPresenter;
    CartAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getActivity().getIntent().getExtras();

        if (extras != null && extras.containsKey(AppConstants.BUNDLE_USER)) {
            UserModel userModel = ((UserModel) extras.getParcelable(AppConstants.BUNDLE_USER));

            if (userModel != null) {
                mPresenter = new CartPresenterImpl(userModel, this);
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = FragmentCartBinding.inflate(inflater, container, false);

        //Set the default currency
        String currencySymbol = Currency.getInstance(Locale.getDefault()).getSymbol();
        mPresenter.setCurrencySymbol(currencySymbol);
        mBinding.setCurrencySymbol(currencySymbol);
        mBinding.setSubtotal("0");
        mBinding.executePendingBindings();


        ButterKnife.bind(this, mBinding.getRoot());

        srLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mPresenter != null && mAdapter!= null) {
                    mPresenter.retrieveCartItems();
                    mAdapter.setCartItems(null, true);
                }
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvCartItems.setLayoutManager(layoutManager);
        mAdapter = new CartAdapter(getActivity(), currencySymbol, this);
        rvCartItems.setAdapter(mAdapter);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL);
        rvCartItems.addItemDecoration(itemDecoration);
        rvCartItems.setItemAnimator(new FadeInAnimator());

        return mBinding.getRoot();
    }

    @Override
    public void setOwnToolbar() {
        getViewActivity().setSupportActionBar(null);
        getViewActivity().setSupportActionBar(mToolbar);
        getViewActivity().getSupportActionBar().setTitle(R.string.title_cart);
        getViewActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getViewActivity().invalidateOptionsMenu();
    }

    @Override
    public void onVisible() {
        //
        if (mPresenter != null) {
            mPresenter.retrieveCartItems();
        }
    }

    @Override
    public void onHidden() {
        //

        if(mPresenter != null){
            mPresenter.updateCart(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if(mPresenter != null){
            mPresenter.updateCart(false);
        }
    }

    @OnClick(R.id.cart_button_checkout)
    public void checkOut(){
        if(mPresenter != null){
            mPresenter.checkOutCart();
        }
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void updateCart(CartModel cart, boolean refreshListDisplay) {
        Log.d(TAG, "updateCart: cart " + cart);

        if (cart.getShipping() > 0) {
            mBinding.setShippingFee(String.valueOf(cart.getShipping()));
        } else {
            mBinding.setShippingFee(null);
        }

        //TODO implement this later
        mBinding.setDiscount(null);

        if (cart.getInstallationFees() > 0) {
            mBinding.setInstallationFee(String.valueOf(cart.getInstallationFees()));
        } else {
            mBinding.setInstallationFee(null);
        }

        if (cart.getSubtotalAmount() > 0) {
            mBinding.setSubtotal(String.valueOf(cart.getSubtotalAmount()));
        } else {
            mBinding.setSubtotal(null);
        }
        mBinding.executePendingBindings();

        mAdapter.setCartItems(cart.getItems(), refreshListDisplay);
    }

    @Override
    public void proceedToCheckOutScreen(CartModel cart, UserModel userModel) {
        Intent intent = new Intent(getActivity(), CheckOutScreen.class);
        intent.putExtra(AppConstants.BUNDLE_USER, userModel);
        intent.putExtra(AppConstants.BUNDLE_CART, cart);
        startActivityForResult(intent, AppConstants.REQUEST_CHECKOUT_CART);

    }

    @Override
    public void hideProgressBlocker() {
        if (flProgressBar != null && flProgressBar.getVisibility() != View.GONE) {
            flProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress() {
        if (srLayout != null && !srLayout.isRefreshing()) {
            srLayout.setRefreshing(true);
        }
    }

    @Override
    public void hideProgress() {
        if (srLayout != null && srLayout.isRefreshing()) {
            srLayout.setRefreshing(false);
        }
    }

    @Override
    public void showEmptyCartBlocker() {
        if (flEmptyBlocker != null) {
            flEmptyBlocker.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showRetrieveCartItemError() {
        String message = getString(R.string.msg_error_retrieve_cart);
        DialogUtil.showAlertDialog(getActivity(), message);
    }

    @Override
    public void hideEmptyCartBlocker() {
        if (flEmptyBlocker != null) {
            flEmptyBlocker.setVisibility(View.GONE);
        }
    }

    @Override
    public void showUpdateCartProgress() {
        String message = getString(R.string.msg_progress_updating_cart);
        dProgress = DialogUtil.showIndeterminateProgressDialog(getContext(),
                "",
                message,
                false);
    }

    @Override
    public void hideUpdateCartProgress() {
        if(dProgress != null && dProgress.isShowing()){
            dProgress.dismiss();
        }
    }

    @Override
    public void showUpdateCartSuccess() {
        Toast.makeText(getContext(), R.string.msg_success_cart_updated, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showUpdateCartError() {
        Toast.makeText(getContext(), R.string.msg_error_cart_update_failed, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRemoveItem(CartItemModel removedCartItem) {
        if (mPresenter != null) {
            mPresenter.removeItem(removedCartItem);
        }
    }

    @Override
    public void onQuantityChange(CartItemModel cartItem, int quantity) {
        if (mPresenter != null) {
            mPresenter.changedItemQuantity(cartItem);
        }
    }
}
