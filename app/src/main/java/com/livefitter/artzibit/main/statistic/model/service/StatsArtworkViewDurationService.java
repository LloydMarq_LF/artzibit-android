package com.livefitter.artzibit.main.statistic.model.service;

import com.livefitter.artzibit.main.statistic.model.request.ArtworkViewDurationRequestModel;

/**
 * Created by LloydM on 5/15/17
 * for Livefitter
 */

public interface StatsArtworkViewDurationService {
    void logArtworkViewDuration(String userEmail, String authToken, ArtworkViewDurationRequestModel body, Callback callback);

    public interface Callback {
        void onArtworkViewDurationSuccess();
        void onArtworkViewDurationError();
    }
}
