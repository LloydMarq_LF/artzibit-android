package com.livefitter.artzibit.main.fave_artwork.model.request;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by LloydM on 6/19/17
 * for Livefitter
 */

public class FaveArtworkUpdateRequestModel {

    @SerializedName("user_favorite")
    private Data userFavorite;

    public FaveArtworkUpdateRequestModel(boolean deleted) {
        this.userFavorite = new Data(deleted);
    }

    public class Data {
        private boolean deleted;

        public Data(boolean deleted) {
            this.deleted = deleted;
        }

        public boolean isDeleted() {
            return deleted;
        }

        public void setDeleted(boolean deleted) {
            this.deleted = deleted;
        }
    }

    public Data getUserFavorite() {
        return userFavorite;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
