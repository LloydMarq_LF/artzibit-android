package com.livefitter.artzibit.main.checkout.model.service;

import android.support.annotation.NonNull;
import android.util.Log;

import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.util.RetrofitUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public class RetrieveShippingDetailServiceImpl implements RetrieveShippingDetailService, Callback<BaseResponseModel<ArrayList<ShippingDetailModel>>> {

    private static final String TAG = RetrieveShippingDetailServiceImpl.class.getSimpleName();

    private Callback mCallback;
    private Call<BaseResponseModel<ArrayList<ShippingDetailModel>>> mCall;

    @Override
    public void retrieveShippingDetail(@NonNull String email, @NonNull String authToken, Callback callback) {
        this.mCallback = callback;

        if (mCall != null && mCall.isExecuted()) {
            mCall.cancel();
        }

        RetrieveShippingDetailApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(RetrieveShippingDetailApiMethods.class);

        this.mCall = apiMethods.retrieveShippingDetail(email, authToken);

        mCall.enqueue(this);
    }

    @Override
    public void cancel() {
        if (mCall != null && mCall.isExecuted()) {
            mCall.cancel();
        }
    }

    @Override
    public void onResponse(Call<BaseResponseModel<ArrayList<ShippingDetailModel>>> call, Response<BaseResponseModel<ArrayList<ShippingDetailModel>>> response) {

        Log.d(TAG, "onResponse: " + response);

        if (response.isSuccessful()) {
            Log.d(TAG, "response success body: " + response.body().toString());
            if (response.body().getStatus()) {
                ArrayList<ShippingDetailModel> shippingDetailModels = response.body().getData();

                if(shippingDetailModels != null){
                    mCallback.onRetrieveShippingDetailSuccess(shippingDetailModels);
                }else{
                    Log.d(TAG, "shipping details null");
                    mCallback.onRetrieveShippingDetailError();
                }
            } else {
                Log.d(TAG, "response unsuccessful");
                mCallback.onRetrieveShippingDetailError();
            }
        } else {
            Log.d(TAG, "response failed body: " + response.errorBody());
            mCallback.onRetrieveShippingDetailError();
        }
    }

    @Override
    public void onFailure(Call<BaseResponseModel<ArrayList<ShippingDetailModel>>> call, Throwable t) {
        t.printStackTrace();
        mCallback.onRetrieveShippingDetailError();
    }
}
