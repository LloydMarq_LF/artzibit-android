package com.livefitter.artzibit.main.browse_artwork;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.UserModel;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by LloydM on 6/5/17
 * for Livefitter
 */

public interface BrowseArtworkContract {
    public interface Presenter{
        int getChosenCategory();
        void setChosenCategory(int chosenCategory);

        /**
         * Called by the View when the user has chosen a category from a the spinner widget
         * @param position
         */
        void setChosenSpinnerCategory(int position);
        void setUserModel(UserModel userModel);
        String[] getCategorySpinnerItems();
        void retrieveArtworks(Map<String, String> queries);
        void favoriteArtwork(ArtworkModel artwork);
        void addToCart(ArtworkModel artwork);
        ArtworkModel getArtworkItem(int position);

        /**
         * Called when the view determines an artwork is being shown to the user.
         * It's up to the implementation on how to determine if this counts as a view for the artwork.
         * If so, increment the artwork's {@link ArtworkModel#getViewCounter() view count}
         * @param viewedArtwork
         */
        void onArtworkViewStart(ArtworkModel viewedArtwork);
        void onArtworkViewEnd(ArtworkModel viewedArtwork);
        void submitArtworkViewStatistics();
    }


    public interface View  extends BaseContractView {
        void saveChosenCategoryPreference(int chosenCategory);
        void updateArtworkList(ArrayList<ArtworkModel> artworkList);
        void updateArtworkList(ArtworkModel artworkModel);
        void updateCurrentArtwork(ArtworkModel artworkModel);
        void toggleBlockerLayout(boolean shouldShow);
        void showArtworkRetrievalProgressDialog();
        void showArtworkRetrievalErrorDialog();
        void showArtworkRetrievalEmptyErrorDialog();
        void showArtworkFaveErrorMessage();
        void hideProgressDialog();
        void proceedToArtworkDetailScreen(ArtworkModel artworkModel);
    }
}
