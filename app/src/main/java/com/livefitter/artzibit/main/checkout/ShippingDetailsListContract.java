package com.livefitter.artzibit.main.checkout;

import android.content.Intent;
import android.os.Bundle;

import com.livefitter.artzibit.BaseContractView;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.model.ShippingDetailModel;
import com.livefitter.artzibit.util.DialogUtil;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/14/17
 * for Livefitter
 */

public interface ShippingDetailsListContract {
    public interface Presenter{
        void retrieveShippingDetails();
        void onShippingDetailSelected(ShippingDetailModel shippingDetailModel);
        void onFooterClick();
        void onActivityResult(int requestCode, int resultCode, Intent data);
        void saveInstanceState(Bundle outstate);
        void onViewCreated(Bundle savedInstanceState);
        void navigateToNextStep();
    }

    public interface View extends BaseContractView{
        void updateShippingList(ArrayList<ShippingDetailModel> shippingDetailModels);
        void proceedToCreateShippingDetailsScreen();
        void setProgressCondition(boolean canProgress);
        void setProceedButtonVisible(boolean isVisible);
        void showForeignAddressWarning();
        void showRetrieveShippingDetailError();
        void showProgressIndicator();
        void hideProgressIndicator();
    }
}
