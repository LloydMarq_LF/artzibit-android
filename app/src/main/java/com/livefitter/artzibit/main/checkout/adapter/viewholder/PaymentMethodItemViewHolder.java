package com.livefitter.artzibit.main.checkout.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.artzibit.databinding.ItemCheckoutPaymentMethodBinding;
import com.livefitter.artzibit.model.CreditCardModel;

import butterknife.ButterKnife;


/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class PaymentMethodItemViewHolder extends RecyclerView.ViewHolder {


    private final ItemCheckoutPaymentMethodBinding mBinding;

    public static PaymentMethodItemViewHolder create(LayoutInflater inflater, ViewGroup parent){
        ItemCheckoutPaymentMethodBinding binding = ItemCheckoutPaymentMethodBinding.inflate(inflater, parent, false);

        return new PaymentMethodItemViewHolder(binding);
    }

    private PaymentMethodItemViewHolder(ItemCheckoutPaymentMethodBinding mBinding) {
        super(mBinding.getRoot());
        this.mBinding = mBinding;

        ButterKnife.bind(this, mBinding.getRoot());
    }

    public void bind(CreditCardModel creditCard, View.OnClickListener clickListener){
        mBinding.getRoot().setOnClickListener(clickListener);
        mBinding.setCard(creditCard);
        mBinding.executePendingBindings();
    }
}
