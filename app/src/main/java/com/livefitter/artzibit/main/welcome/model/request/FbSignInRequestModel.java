package com.livefitter.artzibit.main.welcome.model.request;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.livefitter.artzibit.main.sign_up.model.request.UserAuthAttributesModel;

/**
 * Created by LloydM on 5/30/17
 * for Livefitter
 */

public class FbSignInRequestModel {

    /*{
        user: {
            email: "example@gmail.com",
                    user_authentication_attributes: {
                        "provider": "facebook",
                        "uid": "123456789"
            }
        }
    }*/

    private Payload user;

    public FbSignInRequestModel(@NonNull String email, @NonNull String provider, @NonNull String uid) {
        UserAuthAttributesModel attributes = new UserAuthAttributesModel(provider, uid);
        this.user = new Payload(email, attributes);
    }

    private class Payload{
        private String email;
        @SerializedName("user_authentication_attributes")
        private UserAuthAttributesModel attributes;

        public Payload(String email, UserAuthAttributesModel attributes) {
            this.email = email;
            this.attributes = attributes;
        }
    }


}
