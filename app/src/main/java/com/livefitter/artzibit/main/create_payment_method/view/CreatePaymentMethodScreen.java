package com.livefitter.artzibit.main.create_payment_method.view;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.BaseActivity;
import com.livefitter.artzibit.R;
import com.livefitter.artzibit.main.checkout.view.CheckOutScreen;
import com.livefitter.artzibit.main.create_payment_method.CreatePaymentMethodContract;
import com.livefitter.artzibit.main.create_payment_method.presenter.CreatePaymentMethodPresenterImpl;
import com.livefitter.artzibit.model.CreditCardModel;
import com.livefitter.artzibit.util.DialogUtil;
import com.livefitter.artzibit.util.FormValidityHelper;
import com.livefitter.artzibit.view.CardExpirationPickerDialog;
import com.livefitter.artzibit.view.FormFieldTextWatcher;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreatePaymentMethodScreen extends BaseActivity implements CreatePaymentMethodContract.View, CardExpirationPickerDialog.OnExpirationDateSetListener {

    private static final String TAG = CreatePaymentMethodScreen.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.til_card_number)
    TextInputLayout tilCardNumber;
    @BindView(R.id.et_card_number)
    TextInputEditText etCardNumber;
    @BindView(R.id.til_expiry_date)
    TextInputLayout tilExpiryDate;
    @BindView(R.id.et_expiry_date)
    TextInputEditText etExpiryDate;
    @BindView(R.id.til_cvc)
    TextInputLayout tilCvc;
    @BindView(R.id.et_cvc)
    TextInputEditText etCvc;
    @BindView(R.id.cb_set_as_default)
    CheckBox cbDefault;

    private CardExpirationPickerDialog dExpirationDate;
    private ProgressDialog dProgress;

    private CreatePaymentMethodContract.Presenter mPresenter;
    private int mExpiryMonth = CardExpirationPickerDialog.NO_VALUE;
    private int mExpiryYear = CardExpirationPickerDialog.NO_VALUE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_payment_method);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etCardNumber.addTextChangedListener(new FormFieldTextWatcher(tilCardNumber));
        etExpiryDate.addTextChangedListener(new FormFieldTextWatcher(tilExpiryDate));
        etCvc.addTextChangedListener(new FormFieldTextWatcher(tilCvc));

        etExpiryDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    // Forcing the soft keyboard to close
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    toggleDatePicker(true);
                } else {
                    toggleDatePicker(false);
                }
            }
        });
        etExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleDatePicker(true);
            }
        });

        etCvc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSoftKeyboard(v);
                    return true;
                }
                return false;
            }
        });

        mPresenter = new CreatePaymentMethodPresenterImpl(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_checkout_create_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_save:
                saveCreditCard();
                return true;
            case android.R.id.home:{
                showExitPromptDialog(true);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showExitPromptDialog(false);
    }

    private void toggleDatePicker(boolean show){

        if(show){
            dExpirationDate = CardExpirationPickerDialog.create(mExpiryMonth, mExpiryYear, this);
            dExpirationDate.show(getSupportFragmentManager(), "");
        }else{
            dExpirationDate.dismiss();
        }
    }

    @Override
    public void onExpirationDateSet(int month, int year) {
        this.mExpiryMonth = month;
        this.mExpiryYear = year;
        // Convert the date into something readable
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, mExpiryMonth);
        calendar.set(Calendar.YEAR, mExpiryYear);

        SimpleDateFormat format = new SimpleDateFormat("MMM/yyyy");
        String convertedDate = format.format(calendar.getTime());

        etExpiryDate.setText(convertedDate);
    }

    private void saveCreditCard(){
        hideSoftKeyboard();
        if(areFieldsValid()){
            if(mPresenter != null){
                mPresenter.saveCreditCard(etCardNumber.getText().toString().trim(),
                        mExpiryMonth,
                        mExpiryYear,
                        etCvc.getText().toString().trim(),
                        cbDefault.isChecked());
            }
        }
    }

    private boolean areFieldsValid(){
        boolean areFieldsValid = true;

        if(!FormValidityHelper.isFieldComplete(etCardNumber)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilCardNumber.setError(errorMessage);
        }

        if(!FormValidityHelper.isFieldComplete(etExpiryDate)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilExpiryDate.setError(errorMessage);
        }

        if(!FormValidityHelper.isFieldComplete(etCvc)){
            areFieldsValid = false;
            String errorMessage = getString(R.string.msg_error_required_field);
            tilCvc.setError(errorMessage);
        }

        return areFieldsValid;
    }

    private void showExitPromptDialog(final boolean isGoingUp){
        String title = getString(R.string.label_unsaved_changes);
        String message = getString(R.string.msg_exit_with_unsaved);
        String ok = getString(R.string.label_ok);
        String cancel = getString(R.string.label_cancel);
        DialogUtil.showConfirmationDialog(this,
                title,
                message,
                ok,
                cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == DialogInterface.BUTTON_POSITIVE){
                            if(isGoingUp){
                                NavUtils.navigateUpFromSameTask(CreatePaymentMethodScreen.this);
                            }else{
                                finish();
                            }
                        }
                    }
                });
    }

    @Override
    public void returnToCheckOutScreen(CreditCardModel creditCard) {
        Intent intent = new Intent(this, CheckOutScreen.class);
        intent.putExtra(AppConstants.BUNDLE_CREDIT_CARD, creditCard);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void showValidationProgressIndicator() {
        if(dProgress == null){
            String message = getString(R.string.msg_progress_validating_card);
            dProgress = DialogUtil.showIndeterminateProgressDialog(this, "", message, false);
        }else{
            dProgress.show();
        }
    }

    @Override
    public void hideValidationProgressIndicator() {
        if(dProgress != null && dProgress.isShowing()){
            dProgress.dismiss();
        }
    }

    @Override
    public void showInvalidCardError() {
        String message = getString(R.string.msg_error_invalid_card);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void showCreateCardError() {
        String message = getString(R.string.msg_error_create_card_token);
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public BaseActivity getViewActivity() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return this;
    }
}
