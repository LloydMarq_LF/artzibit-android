package com.livefitter.artzibit.main.cart.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.livefitter.artzibit.R;
import com.livefitter.artzibit.listener.CartItemListener;
import com.livefitter.artzibit.model.CartItemModel;

import java.util.ArrayList;


/**
 * Created by LloydM on 7/4/17
 * for Livefitter
 */

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder> implements CartItemListener {

    Context mContext;
    LayoutInflater mInflater;
    ArrayList<CartItemModel> mCartItems;
    String mCurrencySymbol;
    CartItemListener mListener;

    public CartAdapter(Context context, String currencySymbol, CartItemListener listener) {
        this.mContext = context;
        this.mCurrencySymbol = currencySymbol;
        this.mListener = listener;

        mInflater = LayoutInflater.from(context);
    }

    public void setCartItems(ArrayList<CartItemModel> cartItems, boolean notifyDataSetChanged) {
        this.mCartItems = cartItems;
        if(notifyDataSetChanged) {
            notifyDataSetChanged();
        }
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return CartViewHolder.create(mContext, mInflater, parent);
    }

    @Override
    public void onBindViewHolder(CartViewHolder holder, int position) {
        CartItemModel cartItem = mCartItems.get(position);
        if(cartItem != null){
            holder.bind(mCurrencySymbol,cartItem, this);
        }
    }

    @Override
    public int getItemCount() {
        return mCartItems != null? mCartItems.size() : 0;
    }

    @Override
    public void onRemoveItem(CartItemModel removedCartItem) {

        /*int indexOfRemovedItem = mCartItems.indexOf(removedCartItem);

        if(indexOfRemovedItem != -1){
            mCartItems.remove(indexOfRemovedItem);
            notifyItemRemoved(indexOfRemovedItem);

            mListener.onRemoveItem(removedCartItem);
        }*/

        int indexOfRemovedItem = mCartItems.indexOf(removedCartItem);

        if(indexOfRemovedItem != -1){
            notifyItemRemoved(indexOfRemovedItem);

        }

        mListener.onRemoveItem(removedCartItem);
    }

    @Override
    public void onQuantityChange(CartItemModel cartItem, int quantity) {

        /*int indexOfAffectedItem = mCartItems.indexOf(cartItem);

        if(indexOfAffectedItem != -1){
            mCartItems.set(indexOfAffectedItem, cartItem);
            //notifyItemChanged(indexOfAffectedItem);

            mListener.onQuantityChange(cartItem, quantity);
        }*/

        mListener.onQuantityChange(cartItem, quantity);
    }
}
