package com.livefitter.artzibit.main.sign_up.presenter;

import android.support.annotation.Nullable;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.sign_up.SignUpContract;
import com.livefitter.artzibit.main.sign_up.model.request.SignUpRequestModel;
import com.livefitter.artzibit.main.sign_up.model.service.SignUpService;
import com.livefitter.artzibit.main.sign_up.model.service.SignUpServiceImpl;
import com.livefitter.artzibit.model.UserModel;

import java.lang.ref.WeakReference;

public class SignUpPresenterImpl
        implements SignUpContract.Presenter,
        SignUpService.SignUpCallback{

    private static final String TAG = SignUpPresenterImpl.class.getSimpleName();

    private WeakReference<SignUpContract.View> mView;
    private SignUpServiceImpl mModel;

    private boolean isSigningUpFromFacebook = false;
    private String mFbUid, mFbName, mFbEmail, mFbProfilePictureUrl;

    public SignUpPresenterImpl(SignUpContract.View view) {
        mView = new WeakReference<>(view);
        mModel = new SignUpServiceImpl();
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private SignUpContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void setFacebookFields(String uid, String name, String email, String profilePictureUrl) {
        isSigningUpFromFacebook = true;

        mFbUid = uid;
        mFbName = name;
        mFbEmail = email;
        mFbProfilePictureUrl = profilePictureUrl;

        getView().hideNonFacebookFields();
        getView().setFieldsFromFacebook(mFbName);
    }

    @Override
    public void doSignUp(@Nullable String name, String username, @Nullable String email, String country, String password){

        SignUpRequestModel requestModel = isSigningUpFromFacebook?
                new SignUpRequestModel(mFbName, username, mFbEmail, country, null, mFbProfilePictureUrl, AppConstants.PROVIDER_FACEBOOK, mFbUid) :
                new SignUpRequestModel(name, username, email, country, password);

        mModel.doSignUp(requestModel, this);
        getView().showProgressDialog();
    }

    @Override
    public void onSuccess(UserModel userModel) {
        getView().hideProgressDialog();
        getView().saveUserLogin(userModel);
        getView().proceedToHomeScreen(userModel);
    }

    @Override
    public void onAccountAlreadyTakenError() {
        getView().hideProgressDialog();
        if(!isSigningUpFromFacebook) {
            getView().clearEmailField();
        }
        getView().showDupeEmailErrorDialog();
    }

    @Override
    public void onError() {
        getView().hideProgressDialog();
        getView().clearPasswordFields();
        getView().showErrorDialog();
    }
}