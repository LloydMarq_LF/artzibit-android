package com.livefitter.artzibit.main.filter_artwork.presenter;

import android.app.Activity;
import android.content.Intent;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.filter_artwork.FilterContract;
import com.livefitter.artzibit.main.filter_artwork.model.RetrieveArtworkFiltersService;
import com.livefitter.artzibit.main.filter_artwork.model.RetrieveArtworkFiltersServiceImpl;
import com.livefitter.artzibit.model.ArtworkFilterModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by LloydM on 6/14/17
 * for Livefitter
 */

public class FilterPresenterImpl implements FilterContract.Presenter, RetrieveArtworkFiltersService.Callback {

    private static final String TAG = FilterPresenterImpl.class.getSimpleName();

    WeakReference<FilterContract.View> mView;
    RetrieveArtworkFiltersServiceImpl mService;

    ArrayList<ArtworkFilterModel> mFilterList;

    public FilterPresenterImpl(FilterContract.View view) {
        mView = new WeakReference<FilterContract.View>(view);
        mService = new RetrieveArtworkFiltersServiceImpl();
    }

    /**
     * Return the View reference.
     * Throw an exception if the View is unavailable.
     */
    private FilterContract.View getView() throws NullPointerException {
        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void retrieveFilterOptions() {
        if (mFilterList == null) {
            getView().showProgressDialog();
            mService.retrieveArtworkFilters(this);
        }
    }

    @Override
    public void onFilterOptionSelected(ArtworkFilterModel filterModel) {
        getView().proceedToFilterSubmenu(filterModel);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == AppConstants.REQUEST_CODE_FILTER_OPTIONS && resultCode == Activity.RESULT_OK) {
            ArtworkFilterModel chosenFilter = data.getExtras().getParcelable(AppConstants.BUNDLE_FILTER);

            if (chosenFilter != null) {
                int chosenFilterIndex = mFilterList.indexOf(chosenFilter);
                if (chosenFilterIndex != -1) {
                    mFilterList.set(chosenFilterIndex, chosenFilter);
                    getView().updateFilterList(mFilterList);

                    getView().saveFilterPreferences(mFilterList);
                }
            }
        }
    }

    @Override
    public void onSuccess(ArrayList<ArtworkFilterModel> filterList) {
        mFilterList = filterList;
        getView().hideProgressDialog();
        getView().updateFilterList(filterList);
    }

    @Override
    public void onError() {

    }
}
