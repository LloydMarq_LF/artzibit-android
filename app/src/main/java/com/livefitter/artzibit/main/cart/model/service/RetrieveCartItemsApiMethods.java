package com.livefitter.artzibit.main.cart.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.BaseResponseModel;
import com.livefitter.artzibit.model.CartModel;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;

/**
 * Created by LloydM on 7/5/17
 * for Livefitter
 */

public interface RetrieveCartItemsApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @GET(AppConstants.URL_CART)
    Call<BaseResponseModel<CartModel>> retrieveCartItems(@NonNull @Header("X-User-Email") String userEmail,
                                                         @NonNull @Header("X-User-Token") String authToken);
}
