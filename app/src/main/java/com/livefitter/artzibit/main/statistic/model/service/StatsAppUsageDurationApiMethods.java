package com.livefitter.artzibit.main.statistic.model.service;

import android.support.annotation.NonNull;

import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.main.statistic.model.request.AppUsageDurationRequestModel;
import com.livefitter.artzibit.model.BaseResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface StatsAppUsageDurationApiMethods {
    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_APP_TRACKING_LOGS)
    Call<BaseResponseModel<Object>> logAppUsageDuration(@NonNull @Header("X-User-Email") String userEmail,
                                                        @NonNull @Header("X-User-Token") String authToken,
                                                        @Body AppUsageDurationRequestModel param);
}