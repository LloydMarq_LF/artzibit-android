package com.livefitter.artzibit;

import android.content.Context;

/**
 * Created by LloydM on 5/11/17
 * for Livefitter
 */

public interface BaseContractView {

    BaseActivity getViewActivity();
    Context getAppContext();
}
