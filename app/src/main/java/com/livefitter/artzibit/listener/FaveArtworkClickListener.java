package com.livefitter.artzibit.listener;

import com.livefitter.artzibit.model.ArtworkModel;
import com.livefitter.artzibit.model.FaveArtworkModel;

/**
 * Created by LloydM on 6/21/17
 * for Livefitter
 */

public interface FaveArtworkClickListener {
    void onFaveArtworkClick(FaveArtworkModel faveArtworkModel);
}
