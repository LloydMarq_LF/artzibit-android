package com.livefitter.artzibit.listener;

import com.livefitter.artzibit.model.CartItemModel;

/**
 * Created by LloydM on 7/7/17
 * for Livefitter
 */

public interface CartItemListener{
    void onRemoveItem(CartItemModel removedCartItem);
    void onQuantityChange(CartItemModel cartItem, int quantity);
}
