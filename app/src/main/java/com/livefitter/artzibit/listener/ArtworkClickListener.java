package com.livefitter.artzibit.listener;

import com.livefitter.artzibit.model.ArtworkModel;

/**
 * Created by LloydM on 6/21/17
 * for Livefitter
 */

public interface ArtworkClickListener {
    void onArtworkClick(ArtworkModel artwork);
}
