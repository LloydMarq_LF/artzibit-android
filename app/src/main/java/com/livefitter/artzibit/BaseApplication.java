package com.livefitter.artzibit;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.livefitter.artzibit.main.browse_artwork.view.HomeActivity;
import com.livefitter.artzibit.main.statistic.ArtzibitLogger;
import com.livefitter.artzibit.model.UserModel;
import com.livefitter.artzibit.util.CalendarHelper;
import com.livefitter.artzibit.util.SharedPrefsUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Created by LloydM on 5/11/17
 * for Livefitter
 */

public class BaseApplication extends Application implements Application.ActivityLifecycleCallbacks{
    
    private static final String TAG = BaseApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated: "+ activity.getClass().getSimpleName());

        if(activity instanceof HomeActivity){
            Log.d(TAG, "onActivityCreated: Home");
            UserModel userModel = SharedPrefsUtil.getUserAuth(activity);
            ArtzibitLogger.getInstance().appUsageCount(userModel);
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.d(TAG, "onActivityStarted: "+ activity.getClass().getSimpleName());
        ArtzibitLogger.getInstance().appUsageDurationStart(CalendarHelper.getFormattedCurrentTime());
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.d(TAG, "onActivityResumed: "+ activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.d(TAG, "onActivityPaused: " + activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.d(TAG, "onActivityStopped: "+ activity.getClass().getSimpleName());

        if(activity instanceof HomeActivity){
            Log.d(TAG, "onActivityStopped: Home");
            UserModel userModel = SharedPrefsUtil.getUserAuth(activity);
            ArtzibitLogger.getInstance().appUsageDurationEnd(userModel, CalendarHelper.getFormattedCurrentTime());
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.d(TAG, "onActivitySaveInstanceState: "+ activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.d(TAG, "onActivityDestroyed: "+ activity.getClass().getSimpleName());
    }
}
