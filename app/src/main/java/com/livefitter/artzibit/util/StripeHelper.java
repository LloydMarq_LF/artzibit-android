package com.livefitter.artzibit.util;

import android.content.Context;

import com.livefitter.artzibit.R;
import com.stripe.android.Stripe;

/**
 * Created by LloydM on 7/17/17
 * for Livefitter
 */

public class StripeHelper {
    public static Stripe getInstance(Context context) {
        String stripeKey = context.getResources().getString(R.string.stripe_pub_key);
        return new Stripe(context, stripeKey);
    }
}
