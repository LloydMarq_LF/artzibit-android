package com.livefitter.artzibit.util;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.livefitter.artzibit.R;
import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * Created by LloydM on 6/20/17
 * for Livefitter
 */

public class BindingUtil {

    /**
     * Takes a url and loads the image asynchronously into a given imageView
     *
     * @param imageView
     * @param url
     */
    @BindingAdapter("app:imageSrc")
    public static void setImageUrl(ImageView imageView, String url) {
        if (url != null && !url.isEmpty()) {
            GlideApp.with(imageView.getContext())
                    .load(url)
                    .placeholder(R.drawable.img_logo)
                    .into(imageView);
        }
    }


    /**
     * Takes a url and loads the image asynchronously into a given imageView
     * and applies a blur transformation on it
     *
     * @param imageView
     * @param url
     */
    @BindingAdapter("app:imageSrcWithBlur")
    public static void setImageUrlWithBlur(ImageView imageView, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.with(imageView.getContext()).load(url)
                    .transform(new BlurTransformation(imageView.getContext()))
                    .placeholder(R.drawable.img_logo)
                    .into(imageView);
        }
    }
}
