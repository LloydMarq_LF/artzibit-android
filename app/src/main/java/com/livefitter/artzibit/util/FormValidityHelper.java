package com.livefitter.artzibit.util;

import android.text.Editable;
import android.util.Patterns;
import android.widget.EditText;

/**
 * Created by LloydM on 5/15/17
 * for Livefitter
 */

public class FormValidityHelper {
    public static boolean isFieldComplete(EditText editText){
        return isFieldComplete(editText.getText().toString());
    }

    public static boolean isFieldComplete(String text){
        return !text.trim().isEmpty();
    }

    public static boolean isEmailValid(EditText editText){
        return isEmailValid(editText.getText().toString().trim());
    }

    public static boolean isEmailValid(String email){
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /*public static boolean isPhoneNumberValid(EditText editText){
        return isEmailValid(editText.getText().toString().trim());
    }

    public static boolean isPhoneNumberValid(String phoneNumber){
        return Patterns.PHONE.matcher(phoneNumber).matches();
    }*/

    public static boolean doFieldsMatch(EditText left, EditText right){
        return doFieldsMatch(left.getText().toString(), right.getText().toString());
    }

    public static boolean doFieldsMatch(String left, String right) {
        return left.compareTo(right) == 0;
    }

    public static boolean doesFieldHaveEnoughCharacters(EditText editText, int minimumLength){
        return editText.getText().length() >= minimumLength;
    }

    public static boolean isFieldValueInRange(EditText editText, float min, float max){

        try{
            float value = Float.parseFloat(editText.getText().toString());

            return value >= min && value <= max;
        }catch (NumberFormatException e){
            e.printStackTrace();
        }

        return false;
    }

}
