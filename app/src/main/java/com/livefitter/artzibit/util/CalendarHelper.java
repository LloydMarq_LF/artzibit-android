package com.livefitter.artzibit.util;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by LloydM on 7/20/17
 * for Livefitter
 */

public class CalendarHelper {

    private static final String TAG = CalendarHelper.class.getSimpleName();


    public static String getFormattedCurrentTime(){

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        String formattedTime = format.format(calendar.getTime());
        Log.d(TAG, "getFormattedCurrentTime: formattedTime " + formattedTime);
        return formattedTime;
    }
}
