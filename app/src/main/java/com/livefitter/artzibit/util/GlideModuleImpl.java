package com.livefitter.artzibit.util;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;


/**
 * Created by LloydM on 6/8/17
 * for Livefitter
 */
@GlideModule
public class GlideModuleImpl extends AppGlideModule {
}
