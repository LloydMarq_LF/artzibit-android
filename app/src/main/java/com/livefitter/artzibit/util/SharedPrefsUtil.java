package com.livefitter.artzibit.util;

import android.content.ContentProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.artzibit.AppConstants;
import com.livefitter.artzibit.model.ArtworkFilterModel;
import com.livefitter.artzibit.model.UserModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by LloydM on 5/29/17
 * for Livefitter
 */

public class SharedPrefsUtil {
    private static final String TAG = SharedPrefsUtil.class.getSimpleName();

    public static int getChosenArtworkCategory(Context context){
        SharedPreferences sp = context.getSharedPreferences(AppConstants.PREFS, Context.MODE_PRIVATE);

        return sp.getInt(AppConstants.PREFS_CHOSEN_CATEGORY, AppConstants.CATEGORY_FIXED);
    }

    public static void setChosenArtworkCategory(Context context, int chosenCategory){
        SharedPreferences sp = context.getSharedPreferences(AppConstants.PREFS, Context.MODE_PRIVATE);

        sp.edit()
                .putInt(AppConstants.PREFS_CHOSEN_CATEGORY, chosenCategory)
                .apply();
    }

    public static void setUserAuth(Context context, UserModel userModel){
        // Encode the userModel's email and authToken to Base64
        String encodedUser = UserModel.encode(userModel);
        if(!encodedUser.isEmpty()) {
            SharedPreferences sp = context.getSharedPreferences(AppConstants.PREFS, Context.MODE_PRIVATE);
            sp.edit()
                    .putString(AppConstants.PREFS_AUTH, encodedUser)
                    .apply();
        }
    }

    public static UserModel getUserAuth(Context context){
        SharedPreferences sp = context.getSharedPreferences(AppConstants.PREFS, Context.MODE_PRIVATE);

        if(sp.contains(AppConstants.PREFS_AUTH)){
            String encodedUser = sp.getString(AppConstants.PREFS_AUTH, "");

            if(!encodedUser.isEmpty()){
                return UserModel.decode(encodedUser);
            }
        }

        return null;
    }

    public static boolean hasFilterPreferences(Context context){
        return !context.getSharedPreferences(AppConstants.PREFS_FILTERS, Context.MODE_PRIVATE).getAll().isEmpty();
    }

    public static void saveArtworkFilters(Context context, ArrayList<ArtworkFilterModel> filterList){
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_FILTERS, Context.MODE_PRIVATE).edit();

        // clear this preference first
        editor.clear();

        for(ArtworkFilterModel filter : filterList){
            if(filter.getSelectedOptionId() != ArtworkFilterModel.NO_SELECTED_OPTION){
                editor.putInt(filter.getKey(), filter.getSelectedOptionId());
            }
        }

        editor.apply();
    }

    /**
     * Filthy way to pass and retrieve the chosen filters for the artworks
     * given the weird structure of CrossContainer.
     * @param context
     * @return
     */
    public static HashMap<String,String> getFilterPreferences(Context context){
        Map<String,?> filterMap = context.getSharedPreferences(AppConstants.PREFS_FILTERS, Context.MODE_PRIVATE).getAll();

        HashMap<String,String> filterHashMap = new HashMap<>();

        for(Map.Entry entry : filterMap.entrySet()){
            String key = String.valueOf(entry.getKey());
            String value = String.valueOf(entry.getValue());
            filterHashMap.put(key, value);
        }

        return filterHashMap;
    }

    public static void setCartIdPreference(Context context, int cartId){
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS, Context.MODE_PRIVATE).edit();
        editor.putInt(AppConstants.PREFS_CART_ID, cartId);
        editor.apply();
    }

    public static int getCartIdPreference(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConstants.PREFS, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(AppConstants.PREFS_CART_ID, -1);
    }

    public static void clearFilterPreferences(Context context){

        SharedPreferences sp = context.getSharedPreferences(AppConstants.PREFS_FILTERS, Context.MODE_PRIVATE);
        sp.edit().clear().apply();
    }

    /**
     * Clears the user's authentication, their chosen category, their artwork filters
     * @param context
     */
    public static void clearUserPreferences(Context context){

        SharedPreferences sp = context.getSharedPreferences(AppConstants.PREFS, Context.MODE_PRIVATE);
        sp.edit()
                .remove(AppConstants.PREFS_AUTH)
                .remove(AppConstants.PREFS_CHOSEN_CATEGORY)
                .remove(AppConstants.PREFS_CART_ID)
                .apply();

        clearFilterPreferences(context);
    }
}
