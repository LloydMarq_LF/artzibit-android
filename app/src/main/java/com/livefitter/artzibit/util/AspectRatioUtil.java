package com.livefitter.artzibit.util;

import android.util.Log;

/**
 * Created by LloydM on 7/3/17
 * for Livefitter
 */

public class AspectRatioUtil {
    private static final String TAG = AspectRatioUtil.class.getSimpleName();
    public static float computeRatio(float width, float height){
        if(width > 0 && height > 0) {
            return height / width;
        }

        return 0;
    }
    public static float computeHeight(float width, float ratio){
        Log.d(TAG, "computeHeight: width: " + width + ", ratio: " + ratio);
        if(width > 0 && ratio > 0) {
            Log.d(TAG, "computeHeight: " + (ratio * width));
            return ratio * width;
        }

        return 0;
    }

    public static float computeWidth(float height, float ratio){
        Log.d(TAG, "computeWidth: height: " + height + ", ratio: " + ratio);
        if(height > 0 && height > 0) {
            Log.d(TAG, "computeWidth: " + (height / ratio));
            return height / ratio;
        }

        return 0;
    }
}
