package com.livefitter.artzibit.gson;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.livefitter.artzibit.model.CreditCardModel;

import java.lang.reflect.Type;
import java.util.ArrayList;


/**
 * Created by LloydM on 7/3/17
 * for Livefitter
 */

public class CheckOutCreditCardListSerializer implements JsonSerializer<ArrayList<CreditCardModel>> {

    private static final String TAG = CheckOutCreditCardListSerializer.class.getSimpleName();

    @Override
    public JsonElement serialize(ArrayList<CreditCardModel> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonCreditCardList = new JsonArray();

        for(CreditCardModel creditCardModel : src) {
            JsonObject jsonCreditCard = new JsonObject();
            jsonCreditCard.addProperty("is_selected", creditCardModel.isSelected());

            if (creditCardModel.isNew()) {
                jsonCreditCard.addProperty("type", CreditCardModel.TYPE_NEW_CARD);
                jsonCreditCard.addProperty("number", creditCardModel.getToken());
            } else {
                jsonCreditCard.addProperty("type", CreditCardModel.TYPE_EXISTING_CARD);
                jsonCreditCard.addProperty("number", creditCardModel.getId());
            }


            jsonCreditCardList.add(jsonCreditCard);
        }

        Log.d(TAG, "serialize: " + jsonCreditCardList.toString());

        return jsonCreditCardList;
    }
}
