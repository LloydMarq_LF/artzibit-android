package com.livefitter.artzibit.gson;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.livefitter.artzibit.model.ShippingDetailModel;

import java.lang.reflect.Type;
import java.util.ArrayList;


/**
 * Created by LloydM on 7/3/17
 * for Livefitter
 */

public class CheckOutShippingDetailListSerializer implements JsonSerializer<ArrayList<ShippingDetailModel>> {

    private static final String TAG = CheckOutShippingDetailListSerializer.class.getSimpleName();

    /*
    {
        first_name: "first",
        last_name: "last",
        phone_number: "111",
        detailed_addr: "111",
        city: "Valenzuela City",
        country: "country",
        is_default: true
        is_selected: true
    }
     */


    @Override
    public JsonElement serialize(ArrayList<ShippingDetailModel> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonShippingDetailList = new JsonArray();

        for(ShippingDetailModel shippingDetailModel : src){
            if(shippingDetailModel.isNew()) {
                JsonObject jsonShippingDetail = new JsonObject();

                jsonShippingDetail.addProperty("first_name", shippingDetailModel.getFirstName());
                jsonShippingDetail.addProperty("last_name", shippingDetailModel.getLastName());
                jsonShippingDetail.addProperty("phone_number", shippingDetailModel.getPhoneNumber());
                jsonShippingDetail.addProperty("detailed_addr", shippingDetailModel.getDetailedAddress());
                jsonShippingDetail.addProperty("city", shippingDetailModel.getCity());
                jsonShippingDetail.addProperty("country", shippingDetailModel.getCountry());
                jsonShippingDetail.addProperty("is_default", shippingDetailModel.isDefault());
                jsonShippingDetail.addProperty("is_selected", shippingDetailModel.isSelected());


                jsonShippingDetailList.add(jsonShippingDetail);
            }
        }

        Log.d(TAG, "serialize: " + jsonShippingDetailList.toString());

        return jsonShippingDetailList;
    }
}
