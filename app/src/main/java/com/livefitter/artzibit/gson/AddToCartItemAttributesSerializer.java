package com.livefitter.artzibit.gson;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.livefitter.artzibit.main.artwork_detail.model.request.AddToCartItemAttributes;

import java.lang.reflect.Type;


/**
 * Created by LloydM on 7/3/17
 * for Livefitter
 */

public class AddToCartItemAttributesSerializer implements JsonSerializer<AddToCartItemAttributes> {

    private static final String TAG = AddToCartItemAttributesSerializer.class.getSimpleName();

    @Override
    public JsonElement serialize(AddToCartItemAttributes src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonCartItemAttributes = new JsonObject();

        jsonCartItemAttributes.addProperty("artwork_id", src.getArtworkId());
        jsonCartItemAttributes.addProperty("installation", src.isInstallation());
        jsonCartItemAttributes.addProperty("quantity", src.getQuantity());

        if(src.getCartId() != -1){
            jsonCartItemAttributes.addProperty("cart_id", src.getCartId());
        }
        if(src.getFixedSizeId() != -1){
            jsonCartItemAttributes.addProperty("fixed_size_id", src.getFixedSizeId());
        }
        if(src.getFixedSizePriceId() != -1){
            jsonCartItemAttributes.addProperty("fixed_size_price_id", src.getFixedSizePriceId());
        }
        if(src.getHeight() != -1){
            jsonCartItemAttributes.addProperty("height", src.getHeight());
        }
        if(src.getWidth() != -1){
            jsonCartItemAttributes.addProperty("width", src.getWidth());
        }

        Log.d(TAG, "serialize: " + jsonCartItemAttributes.toString());


        return jsonCartItemAttributes;
    }
}
