package com.livefitter.artzibit.gson;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.livefitter.artzibit.main.checkout.model.request.CheckOutCartRequestModel;
import com.livefitter.artzibit.model.CreditCardModel;

import java.lang.reflect.Type;


/**
 * Created by LloydM on 7/3/17
 * for Livefitter
 */

public class CheckOutCartRequestSerializer implements JsonSerializer<CheckOutCartRequestModel> {

    private static final String TAG = CheckOutCartRequestSerializer.class.getSimpleName();

    @Override
    public JsonElement serialize(CheckOutCartRequestModel src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonCreditCard = new JsonObject();

        if(src.getStatus() != null && !src.getStatus().isEmpty()) {
            jsonCreditCard.addProperty("status", src.getStatus());
        }

        if(src.getSelectedShippingDetailId() != CheckOutCartRequestModel.SHIPPING_DETAIL_ID_NULL){
            jsonCreditCard.addProperty("user_shipping_detail_id", src.getSelectedShippingDetailId());
        }

        Log.d(TAG, "serialize: " + jsonCreditCard.toString());

        return jsonCreditCard;
    }
}
