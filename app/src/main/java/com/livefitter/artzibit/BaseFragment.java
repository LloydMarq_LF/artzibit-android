package com.livefitter.artzibit;

import android.support.v4.app.Fragment;


/**
 * Created by LloydM on 5/10/17
 * for Livefitter
 */

public abstract class BaseFragment extends Fragment {
    public abstract boolean onBackPressed();

    public BaseActivity getViewActivity(){
        if(getActivity() instanceof BaseActivity){
            return (BaseActivity)getActivity();
        }else{
            return null;
        }
    }
}
