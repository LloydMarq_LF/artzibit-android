package com.livefitter.artzibit.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.livefitter.artzibit.BaseFragment;


/**
 * Fragment that is designed for use in a viewpager or list.
 * Holds a condition flag that is set to determine if some arbitrary condition set in this class allows you
 * to navigate to the next fragment in sequence.
 * The flag in question is {@link #mCanProgress} and is set to false by default.
 * Uses {@link OnConditionChangeListener} to notify observers
 * if there's a change in the condition status.
 */

public abstract class ConditionalPagerFragment extends BaseFragment {

    protected static final String BUNDLE_CAN_PROGESS = "can_progress";

    protected boolean mCanProgress = false;

    private OnConditionChangeListener mListener;

    public interface OnConditionChangeListener{
        /**
         * Called as soon as the conditions for progression past this fragment change
         * @param fragment this fragment
         * @param canProgress determines if the next fragment in sequence is enabled for navigation/use
         */
        void onConditionChanged(ConditionalPagerFragment fragment, boolean canProgress);
    }

    public ConditionalPagerFragment() {
    }

    public ConditionalPagerFragment(OnConditionChangeListener mListener) {
        this.mListener = mListener;
    }

    protected OnConditionChangeListener getListener(){
        return mListener;
    }

    public void setConditionChangeListener(OnConditionChangeListener mListener) {
        this.mListener = mListener;
    }

    public boolean canProgress(){
        return mCanProgress;
    }

    public void setCanProgress(boolean canProgress){
        setCanProgress(canProgress, true);
    }

    public void setCanProgress(boolean canProgress, boolean callListener){
        this.mCanProgress = canProgress;

        if(mListener != null && callListener){
            mListener.onConditionChanged(this, canProgress);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState != null){
            mCanProgress = savedInstanceState.getBoolean(BUNDLE_CAN_PROGESS, false);

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(BUNDLE_CAN_PROGESS, mCanProgress);
    }
}
