package com.livefitter.artzibit.view;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.livefitter.artzibit.R;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LloydM on 7/16/17
 * for Livefitter
 */

public class CardExpirationPickerDialog extends AppCompatDialogFragment {

    private static final String TAG = CardExpirationPickerDialog.class.getSimpleName();
    private static final int MAX_YEAR_RANGE = 15;
    public static final int NO_VALUE = -1;
    private static final String ARG_MONTH = "month";
    private static final String ARG_YEAR = "year";
    private static final String BUNDLE_MONTH = "month";
    private static final String BUNDLE_YEAR = "year";
    private static final String BUNDLE_YEAR_LIST = "year_list";

    private OnExpirationDateSetListener mListener;
    private int mMonth;
    private int mYear;
    private int mBaseYear;
    private String[] mMonthArr;
    private String[] mYearArr;
    private ArrayAdapter<String> mMonthAdapter;
    private ArrayAdapter<String> mYearAdapter;

    @BindView(R.id.expiration_picker_spinner_month)
    AppCompatSpinner spnMonth;
    @BindView(R.id.expiration_picker_spinner_year)
    AppCompatSpinner spnYear;


    public interface OnExpirationDateSetListener{
        void onExpirationDateSet(int month, int year);
    }

    public static CardExpirationPickerDialog create(int month, int year, OnExpirationDateSetListener listener){
        CardExpirationPickerDialog fragment = new CardExpirationPickerDialog();
        fragment.setListener(listener);

        if(month != NO_VALUE && year != NO_VALUE) {
            Bundle args = new Bundle();
            args.putInt(ARG_MONTH, month);
            args.putInt(ARG_YEAR, year);
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeCalendar(savedInstanceState);
    }

    public void setListener(OnExpirationDateSetListener listener) {
        this.mListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_expiration_picker, null);

        ButterKnife.bind(this, view);

        mMonthAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, mMonthArr);
        spnMonth.setAdapter(mMonthAdapter);
        spnMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position >= 0 && position < mMonthArr.length) {
                    // since the position starts at 0, while the months correspond to 1 - 12
                    mMonth = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnMonth.setSelection(mMonth, false);

        mYearAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, mYearArr);
        spnYear.setAdapter(mYearAdapter);
        spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedYear = mYearAdapter.getItem(position);

                try {
                    mYear = Integer.decode(selectedYear);
                }catch(NumberFormatException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnYear.setSelection(mYearAdapter.getPosition(String.valueOf(mYear)));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.label_expiry_date)
                .setView(view)
                // Add action buttons
                .setPositiveButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if(mListener != null){
                            mListener.onExpirationDateSet(mMonth, mYear);
                        }
                    }
                })
                .setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }

    private void initializeCalendar(Bundle savedInstanceState){

        final Calendar c = Calendar.getInstance();
        mBaseYear = c.get(Calendar.YEAR);

        if(savedInstanceState != null
                && savedInstanceState.containsKey(BUNDLE_MONTH)
                && savedInstanceState.containsKey(BUNDLE_YEAR)
                && savedInstanceState.containsKey(BUNDLE_YEAR_LIST)){
            mMonth = savedInstanceState.getInt(BUNDLE_MONTH);
            mYear = savedInstanceState.getInt(BUNDLE_YEAR);
            mYearArr = savedInstanceState.getStringArray(BUNDLE_YEAR_LIST);
        }else if(getArguments() != null
                && getArguments().containsKey(ARG_MONTH)
                && getArguments().containsKey(ARG_YEAR)){

            mMonth = getArguments().getInt(ARG_MONTH);
            mYear = getArguments().getInt(ARG_YEAR);
        }else{
            // Use the current date as the default date in the picker
            mMonth = c.get(Calendar.MONTH);
            mYear = mBaseYear;
        }

        mMonthArr = getResources().getStringArray(R.array.months);

        // Generate a list of years starting from current year upto current year + MAX_YEAR_RANGE
        ArrayList<String> yearList = new ArrayList<>();
        for (int i = 0; i <= MAX_YEAR_RANGE; i++){
            yearList.add(String.valueOf(mBaseYear + i));
        }

        mYearArr = yearList.toArray(new String[0]);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(BUNDLE_MONTH, mMonth);
        outState.putInt(BUNDLE_YEAR, mYear);
        outState.putStringArray(BUNDLE_YEAR_LIST, mYearArr);
    }
}
