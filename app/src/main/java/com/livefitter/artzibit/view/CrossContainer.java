package com.livefitter.artzibit.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.livefitter.artzibit.adapter.CrossViewPagerAdapter;


/**
 * Created by LloydM on 5/17/17
 * for Livefitter
 */

public class CrossContainer extends RelativeLayout {

    private static final String TAG = CrossContainer.class.getSimpleName();

    // Direction which the scroll is going
    private static final int SCROLL_DIRECTION_DOWN = 1;
    private static final int SCROLL_DIRECTION_REST = 0;
    private static final int SCROLL_DIRECTION_UP = -1;

    // Active child index
    public static final int ACTIVE_CHILD_ABOVE = 15;
    public static final int ACTIVE_CHILD_NONE = 0;
    public static final int ACTIVE_CHILD_BELOW = -15;

    // State of active child
    private static final String CHILD_STATE_HIDDEN = "child_hidden";
    private static final String CHILD_STATE_SHOWING = "child_shown";
    private static final String CHILD_STATE_IS_SCROLLING = "child_is_scrolling";

    // Percentage of total height used as the threshold for determining if a scroll is enough to expand one of the vertical children
    private static final float THRESHOLD_PERCENTAGE = (float) 0.2;

    private float mStartX, mStartY, deltaY;

    private int mTouchSlop;

    // In pixel amount of threshold based on this container's height multiplied by the threshold percentage
    private int mVerticalThreshold = 0;

    private int mInitialScrollDirection = SCROLL_DIRECTION_REST;
    private int mLastScrollDirection = SCROLL_DIRECTION_REST;

    // The index of the active child and, if it exists currently, its state
    private int mActiveChildIndex = ACTIVE_CHILD_NONE;
    private String mActiveChildState = CHILD_STATE_HIDDEN;

    private boolean mIsScrolling;

    // Default starting positions for the vertical children both above and below this container
    private int mChildAboveStartPosition, mChildBelowStartPosition;

    // Position of vertical children when they are fully shown on screen
    private int mVisibleChildPosition = 0;

    private ViewPager mViewPager;

    private View mChildAbove, mChildBelow, mActiveChild;

    private OnVerticalChildChangeListener mChildChangeListener;

    public CrossContainer(Context context) {
        super(context);
    }

    public CrossContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CrossContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        final int layoutHeight = b - t;
        mVerticalThreshold = (int) (layoutHeight * THRESHOLD_PERCENTAGE);

        ViewConfiguration vc = ViewConfiguration.get(getContext());
        mTouchSlop = vc.getScaledTouchSlop();
    }

    /**
     * Sets the view of fragmentAbove as the child view that is vertically above this container
     */
    public void setChildAbove(Fragment fragmentAbove){
        setChildAbove(fragmentAbove.getView());
    }

    /**
     * Sets the view childAbove as the child view that is vertically above this container
     */
    public void setChildAbove(View childAbove) {
        this.mChildAbove = childAbove;

        if (mChildAbove != null) {
            mChildAbove.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    mChildAboveStartPosition = mChildAbove.getTop() - mChildAbove.getHeight();

                    mChildAbove.setTranslationY(mChildAboveStartPosition);

                    mChildAbove.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }

    /**
     * Sets the view of fragmentBelow as the child view that is vertically below this container
     */
    public void setChildBelow(Fragment fragmentBelow){
        setChildBelow(fragmentBelow.getView());
    }

    /**
     * Sets the view childBelow as the child view that is vertically below this container
     */
    public void setChildBelow(View childBelow) {
        this.mChildBelow = childBelow;

        if (mChildBelow != null) {
            mChildBelow.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    mChildBelowStartPosition = mChildBelow.getTop() + mChildBelow.getHeight();

                    mChildBelow.setTranslationY(mChildBelowStartPosition);

                    mChildBelow.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }

    /**
     * Sets the viewpager as the horizontal child of this container. It would be best if this viewpager starts showing its middle child though.
     */
    public void setViewPager(ViewPager viewPager) {
        this.mViewPager = viewPager;
    }

    public void setViewPagerPageChangeListener(ViewPager.OnPageChangeListener listener){
        if(mViewPager != null){
            mViewPager.addOnPageChangeListener(listener);
        }
    }


    public void setOnChildChangeListener(OnVerticalChildChangeListener mChildChangeListener) {
        this.mChildChangeListener = mChildChangeListener;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        final int action = MotionEventCompat.getActionMasked(event);

        // Always handle the case of the touch gesture being complete.
        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            // Release the scroll.
            mIsScrolling = false;
            return false; // Do not intercept touch event, let the child handle it
        }

        switch (action) {

            case MotionEvent.ACTION_DOWN:
                mStartX = event.getX();
                mStartY = event.getY();
                break;

            case MotionEvent.ACTION_MOVE: {

                boolean isCurrentChildCenter = ((CrossViewPagerAdapter) mViewPager.getAdapter()).isChildCenter(mViewPager.getCurrentItem());
                if(!isCurrentChildCenter){
                    // Only scroll if we are at the center child of the viewpager
                    return false;
                }

                if (mIsScrolling) {
                    // We're currently scrolling, so yes, intercept the
                    // touch event!
                    return true;
                }

                float x = event.getX();
                float y = event.getY();

                final float xDiff = x - mStartX;
                final float yDiff = y - mStartY;

                // Touch slop should be calculated using ViewConfiguration
                // constants.
                if (Math.abs(xDiff) > Math.abs(yDiff) && Math.abs(xDiff) > mTouchSlop) {
                    // Scrolling horizontally. Ignore touch event
                    if (mActiveChildIndex != ACTIVE_CHILD_NONE) {
                        return true;
                    }
                    return false;
                }

                if (Math.abs(yDiff) > mTouchSlop) {
                    // Start scrolling!
                    mIsScrolling = true;
                    return true;
                }
                break;
            }
        }

        // In general, we don't want to intercept touch events. They should be
        // handled by the child view.
        return false;

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (mViewPager == null) {
            throw new RuntimeException("Please provide a viewPager as a child of " + CrossContainer.class.getSimpleName());
        } else if (mChildAbove == null) {
            throw new RuntimeException("Please provide a view as the above child of " + CrossContainer.class.getSimpleName());
        } else if (mChildBelow == null) {
            throw new RuntimeException("Please provide a view as the below child of " + CrossContainer.class.getSimpleName());
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mStartX = event.getX();
                mStartY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                float x = event.getX();
                float y = event.getY();
                deltaY = y - mStartY;
                if (mIsScrolling) {

                    if (mInitialScrollDirection == SCROLL_DIRECTION_REST) {
                        // Determine the initial scroll direction
                        mInitialScrollDirection = deltaY > 0 ? SCROLL_DIRECTION_DOWN : SCROLL_DIRECTION_UP;
                    } else {
                        // Otherwise, track the most recent scroll direction
                        mLastScrollDirection = deltaY > 0 ? SCROLL_DIRECTION_DOWN : SCROLL_DIRECTION_UP;
                    }

                    if (mActiveChildIndex == ACTIVE_CHILD_NONE) {
                        // Determine the intended child to be affected based on the initial scroll
                        mActiveChildIndex = mInitialScrollDirection == SCROLL_DIRECTION_DOWN ? ACTIVE_CHILD_ABOVE : ACTIVE_CHILD_BELOW;
                    }

                    float scrollPercentage = deltaY / getHeight();

                    if (scrollPercentage > 1) {
                        scrollPercentage = 1;
                    } else if (scrollPercentage < -1) {
                        scrollPercentage = -1;
                    }

                    boolean isChildAboveGoingToOverScroll = (mActiveChild == mChildAbove && mActiveChildState.equals(CHILD_STATE_SHOWING)) && mInitialScrollDirection == SCROLL_DIRECTION_DOWN;
                    boolean isChildBelowGoingToOverScroll = (mActiveChild == mChildBelow && mActiveChildState.equals(CHILD_STATE_SHOWING)) && mInitialScrollDirection == SCROLL_DIRECTION_UP;

                    if(!isChildAboveGoingToOverScroll && !isChildBelowGoingToOverScroll){
                        scrollVerticalChild(mActiveChildIndex, scrollPercentage, mInitialScrollDirection);
                        return true;
                    }

                    return false;
                }
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:

                mIsScrolling = false;

                if (Math.abs(deltaY) >= mVerticalThreshold) {
                    if (mActiveChildIndex == ACTIVE_CHILD_NONE) {

                        boolean affectChildAbove = mLastScrollDirection == SCROLL_DIRECTION_DOWN;
                        mActiveChildIndex = affectChildAbove ? ACTIVE_CHILD_ABOVE : ACTIVE_CHILD_BELOW;
                        animateChildVisibilityChange(mActiveChildIndex, true);

                    } else if (mActiveChildIndex == ACTIVE_CHILD_ABOVE) {

                        boolean shouldShowChild = mLastScrollDirection == SCROLL_DIRECTION_DOWN;
                        animateChildVisibilityChange(mActiveChildIndex, shouldShowChild);
                    } else if (mActiveChildIndex == ACTIVE_CHILD_BELOW) {

                        boolean shouldShowChild = mLastScrollDirection == SCROLL_DIRECTION_UP;
                        animateChildVisibilityChange(mActiveChildIndex, shouldShowChild);
                    }

                } else {
                    boolean shouldShowChild = mActiveChild != null;

                    //mCallback.onScrollStop(mActiveChildIndex, shouldShowChild);
                    animateChildVisibilityChange(mActiveChildIndex, shouldShowChild);
                }

                return true;
        }
        return super.onTouchEvent(event);
    }

    /**
     * Called when this view is receiving a touch event that it considers as a vertical scroll
     *
     * @param activeChild:            determines the child view that should be affected by this scroll.
     *                                Either {@link CrossContainer#ACTIVE_CHILD_ABOVE} or {@link CrossContainer#ACTIVE_CHILD_BELOW}
     * @param scrollPercentage:       positive amount is an downwards scroll, while a negative amount is an upwards scroll
     * @param initialScrollDirection: determines the initial scrolling direction.
     *                                positive amount is an initial downwards scroll, while a negative amount is an initial upwards scroll
     */
    private void scrollVerticalChild(int activeChild, float scrollPercentage, int initialScrollDirection) {

        View targetedChild = activeChild == ACTIVE_CHILD_ABOVE? mChildAbove : mChildBelow;

        if (targetedChild != null) {

            int initialScrollPosition = 0;

            if ((targetedChild == mChildAbove && initialScrollDirection == CrossContainer.SCROLL_DIRECTION_DOWN)) {
                initialScrollPosition = mChildAboveStartPosition;
            } else if (targetedChild == mChildBelow && initialScrollDirection == CrossContainer.SCROLL_DIRECTION_UP) {
                initialScrollPosition = mChildBelowStartPosition;
            }

            float scrollAmount = initialScrollPosition + (targetedChild.getHeight() * scrollPercentage);

            targetedChild.setTranslationY(scrollAmount);

            mActiveChildState = CHILD_STATE_IS_SCROLLING;
        }
    }

    /**
     * Called when the scroll amount exceeds a threshold should show or hide one of its vertical children.
     *
     * @param activeChild:    determines the child view that should be affected by this change.
     *                        Either {@link CrossContainer#ACTIVE_CHILD_ABOVE} or {@link CrossContainer#ACTIVE_CHILD_BELOW}
     * @param shouldShowChild True if the child should be shown, hidden otherwise.
     */
    private void animateChildVisibilityChange(int activeChild, final boolean shouldShowChild) {

        setActiveChild(activeChild);

        if (mActiveChild != null) {

            int endingPosition = mVisibleChildPosition;
            Runnable endAction = new Runnable() {
                @Override
                public void run() {
                    mActiveChildState = shouldShowChild? CHILD_STATE_SHOWING : CHILD_STATE_HIDDEN;

                    if(mChildChangeListener != null){

                        mActiveChildIndex = shouldShowChild ? mActiveChildIndex : ACTIVE_CHILD_NONE;

                        mChildChangeListener.onVerticalChildChange(mActiveChildIndex);

                        mInitialScrollDirection = SCROLL_DIRECTION_REST;
                    }
                }
            };

            if(!shouldShowChild && mActiveChild == mChildAbove){
                endingPosition = mChildAboveStartPosition;
            }else if(!shouldShowChild && mActiveChild == mChildBelow){
                endingPosition = mChildBelowStartPosition;
            }

            animateChildToPosition(mActiveChild, endingPosition, endAction);
        }
    }

    private void animateChildToPosition(View childView, int endingPosition, @Nullable Runnable endAction){
        ViewPropertyAnimator animator = childView.animate().translationY(endingPosition);

        if(endAction != null){
            animator.withEndAction(endAction);
        }
    }

    /**
     * Determines the correct active child based on the parameter activeChild and sets the {@link CrossContainer#mActiveChild active child} accordingly
     * @param activeChild determines the child view that should be affected by this change.
     *                    Either {@link CrossContainer#ACTIVE_CHILD_ABOVE} or {@link CrossContainer#ACTIVE_CHILD_BELOW}.
     *                    If its {@link CrossContainer#ACTIVE_CHILD_NONE} or if either one of vertical children is null,
     *                    this method sets the {@link CrossContainer#mActiveChild active child} to null
     */
    private void setActiveChild(int activeChild) {
        if (mChildAbove != null && mChildBelow != null) {
            if (activeChild == ACTIVE_CHILD_ABOVE) {
                mActiveChild = mChildAbove;
                return;
            } else if (activeChild == ACTIVE_CHILD_BELOW) {
                mActiveChild = mChildBelow;
                return;
            }
        }
        mActiveChild = null;
    }

    public int getActiveChildIndex(){
        return mActiveChildIndex;
    }

    public void toggleVerticalChildVisibility(int activeChild, final boolean shouldShowChild){
        animateChildVisibilityChange(activeChild, shouldShowChild);
    }

    public interface OnVerticalChildChangeListener{
        /**
         * Called whenever a {@link CrossContainer} has one of its vertical children active or inactive
         * @param childIndex Either the {@link CrossContainer#ACTIVE_CHILD_ABOVE}, {@link CrossContainer#ACTIVE_CHILD_BELOW}
         *                   or {@link CrossContainer#ACTIVE_CHILD_NONE}
         */
        void onVerticalChildChange(int childIndex);
    }
}
