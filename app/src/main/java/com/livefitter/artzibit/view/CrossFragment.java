package com.livefitter.artzibit.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.livefitter.artzibit.BaseFragment;
import com.livefitter.artzibit.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CrossFragment extends BaseFragment {

    private String title;

    /*@BindView(R.id.toolbar)
    Toolbar toolbar;*/

    @Nullable
    @BindView(R.id.test_title)
    TextView tvTitleTest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            title = getArguments().getString("title", "default title");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_test, container, false);

        ButterKnife.bind(this, rootView);

        if(title != null && !title.isEmpty()){
            tvTitleTest.setText(title);
        }

        return rootView;
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    /**
     * Tell this fragment to set its toolbar to the parent activity as the actionbar
     */
    public void setOwnToolbar(){

    };

    public void onVisible(){

    };
    public void onHidden(){

    };
}
