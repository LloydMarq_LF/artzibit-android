package com.livefitter.artzibit.adapter;

import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.livefitter.artzibit.R;


/**
 * Created by LloydM on 5/17/17
 * for Livefitter
 */

public abstract class CrossViewPagerAdapter extends FragmentPagerAdapter {

    public static final int NO_CENTER = -1;

    public CrossViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    /*@Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_home_test, null);
        //new LinearLayout(container.getContext());
        TextView textView = (TextView) relativeLayout.findViewById(R.id.title);
        textView.setText("Inner:" + position);
        switch (position) {
            case 0:
                relativeLayout.setBackgroundColor(Color.parseColor("#2196F3"));
                break;
            case 1:
                relativeLayout.setBackgroundColor(Color.parseColor("#673AB7"));
                break;
            case 2:
                relativeLayout.setBackgroundColor(Color.parseColor("#009688"));
                break;
        }
        container.addView(relativeLayout);
        return relativeLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        RelativeLayout view = (RelativeLayout) object;
        container.removeView(view);
    }*/

    public int getCenterIndex(){
        if (getCount() % 2 == 0 ){
            return NO_CENTER;
        }else{
            return (int) Math.floor(getCount() / 2);
        }
    }

    public boolean isChildCenter(int position){

        int centerIndex = getCenterIndex();
        if(centerIndex != NO_CENTER){
            return centerIndex == position;
        }

        return false;
    }
}
